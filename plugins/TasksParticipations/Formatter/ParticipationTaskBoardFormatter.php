<?php

namespace Kanboard\Plugin\TasksParticipations\Formatter;

use Kanboard\Model\SwimlaneModel;
use Kanboard\Model\TaskModel;
use Kanboard\Plugin\Group_assign\Model\MultiselectMemberModel;
use Kanboard\Plugin\VectorTasks\Formatter\VectorBoardFormatter;

class ParticipationTaskBoardformatter extends VectorBoardFormatter {
    protected function getTasks() {
        return $this->query
            ->join(MultiselectMemberModel::TABLE, 'group_id', 'owner_ms')
            ->eq(TaskModel::TABLE.'.project_id', $this->projectId)
            ->eq(MultiselectMemberModel::TABLE.'.user_id', $this->userId)
            ->asc(TaskModel::TABLE.'.position')
            ->findAll();
    }
}