<?php

namespace Kanboard\Plugin\TasksParticipations;

use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Translator;

class Plugin extends Base
{
    public function initialize()
    {
        $this->template->setTemplateOverride('board/view_private', 'TasksParticipations:board/view_private');
        $this->template->setTemplateOverride('board/table_container', 'TasksParticipations:board/table_container');
    }

    public function onStartup()
    {
        Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getClasses()
    {
        return [
            'Plugin\TasksParticipations\Formatter' => [
                'ParticipationTaskBoardFormatter',
            ],
        ];
    }

    public function getPluginName()
    {
        return 'TasksParticipations';
    }

    public function getPluginDescription()
    {
        return t('Showing tasks in which the user participates');
    }

    public function getPluginAuthor()
    {
        return 'Rustam Urazov';
    }

    public function getPluginVersion()
    {
        return '1.0.0';
    }

    public function getPluginHomepage()
    {
        return 'https://github.com/kanboard/plugin-myplugin';
    }
}

