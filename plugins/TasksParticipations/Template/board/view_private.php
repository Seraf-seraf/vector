<section id="main">

    <?= $this->projectHeader->render($project, 'BoardViewController', 'show', true) ?>

    <?php $tasks_array = array(
        'project' => $project,
        'swimlanes' => $swimlanes,
        'board_private_refresh_interval' => $board_private_refresh_interval,
        'board_highlight_period' => $board_highlight_period,
    ) ?>

    <?php switch ($my_tasks):
        case 'owner': ?>
            <?= $this->render('VectorTasks:board/table_container', $tasks_array) ?>
        <?php break; ?>
        <?php case 'participant' ?>
            <?php $tasks_array['not_editable'] = true ?>
            <?= $this->render('TasksParticipations:board/table_container', $tasks_array) ?>
        <?php break; ?>
        <?php case 'creator' ?>
            <?= $this->render('ControlledTasks:board/table_container', $tasks_array) ?>
        <?php break; ?>
    <?php endswitch; ?>

</section>