<?php

namespace Kanboard\Plugin\TasksParticipations\Controller;

use Kanboard\Controller\BaseController;

class ParticipationTaskBoardController extends BaseController {
    public function show() {
        $project = $this->getProject();

        $this->response->html($this->helper->layout->app('board/view_private', array(
            'project' => $project,
            'title' => $project['name'],
            'my_tasks' => $this->request->getStringParam('task'),
            'description' => $this->helper->projectHeader->getDescription($project),
            'board_private_refresh_interval' => $this->configModel->get('board_private_refresh_interval'),
            'board_highlight_period' => $this->configModel->get('board_highlight_period'),
            'swimlanes' => $this->taskLexer
                ->build('')
                ->format($this->participationTaskBoardFormatter->withProjectId($project['id']),$this->participationTaskBoardFormatter->withUserId($this->getUser()['id']))
        )));
    }
}