<?php

namespace Kanboard\Plugin\NotificationSuperiors;
use Kanboard\Core\Plugin\Base as Bbase;
use Kanboard\Core\Base;
use Kanboard\Core\Translator;
use Kanboard\Model\TaskFinderModel;
use Kanboard\Plugin\VectorTasks\Model\RecurrenceTaskModel;

use Kanboard\Plugin\NotificationSuperiors\Action\NotificationSuperiors;
use Kanboard\Plugin\NotificationSuperiors\Action\NotificationAdmins;
use Kanboard\Plugin\NotificationSuperiors\Helper\NotiHelper;
use Kanboard\Plugin\NotificationSuperiors\Console\Command;
use DateTimeImmutable;
use Pheanstalk\Pheanstalk;
use \PhpAmqpLib\Connection\AMQPStreamConnection;
use \PhpAmqpLib\Message\AMQPMessage;


class Plugin extends Bbase
{
    public function initialize()
    {
        //date_default_timezone_set($this->timezoneModel->getCurrentTimezone());
        // $datt = date_default_timezone_get();
        //$h = date('H');
        //$i = date('i');
        // var_dump($datt);
        //var_dump($h); var_dump($i);

        // $hours = '05';
        // $minutes = '16';
        // if (date('H') == $hours && date('i') == $minutes) {
        //     echo 'This time';
        // }

        //$this->logger->debug(__METHOD__.' Plugin started... ');
        $this->template->setTemplateOverride('notification/tasks_dueng_superiors','NotificationSuperiors:notification/tasks_dueng');
        $this->actionManager->register(new NotificationSuperiors($this->container));
        //$this->actionManager->register(new NotificationAdmins($this->container));
    }
    public function onStartup()
    {
        //Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
        //$this->eventManager->register(NotiHelper::EVENT_NOTISUP, t('Overdued a tasks'));
    }

    public function getPluginName()
    {
        return 'NotificationSuperiors';
    }

    public function getPluginDescription()
    {
        return t('Addition for Related Groups and SchemeDraw to notify of overdue tasks superiors');
    }

    public function getPluginAuthor()
    {
        return 'Rzer_1';
    }

    public function getPluginVersion()
    {
        return '1.0.0';
    }

    public function getPluginHomepage()
    {
        return 'https://github.com/kanboard/plugin-myplugin';
    }
    public function getClasses()
    {
        return [
            'Plugin\VectorTasks\Model' => [
                'TaskCreationModel',
                'VectorTaskFileModel',
                'RecurrenceTaskModel',
            ],
            'Plugin\RelatedGroups\Model' => [
                'GroupMemberModel','GroupModel','GroupRelatedModel',
            ],
        ];
    }
}

