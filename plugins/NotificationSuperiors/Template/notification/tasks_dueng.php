<html>
<body>
<h2><?= $this->text->e($task['title']) ?> (#<?= $task['id'] ?>)</h2>
<? //var_dump($messagedata); ?>

<ul>
    <li>
        <?= t('Created:').' '.$messagedata['datecreate'] ?>
    </li>
    <?php if ($messagedata['datedue']): ?>
    <li>
        <strong><?= t('Due date:').' '.$messagedata['datedue'] ?></strong>
    </li>
    <?php endif ?>
    <?php if (! empty($messagedata['user'])): ?>
    <li>
        <?= t('Исполнитель: %s', $messagedata['user']) ?>
    </li>
    <?php endif ?>
    <li>
        <strong>
        <?php if (! empty($task['assignee_username'])): ?>
            <?= t('Assigned to %s', $task['assignee_name'] ?: $task['assignee_username']) ?>
        <?php else: ?>
            <?= t('There is nobody assigned') ?>
        <?php endif ?>
        </strong>
    </li>
    <li>
        <?= t('Column on the board:') ?>
        <strong><?= $this->text->e($messagedata['column']) ?></strong>
    </li>
    <li><?= t('Task position:').' '.$this->text->e($task['position']) ?></li>
    <?php if (! empty($task['category_id'])): ?>
    <li>
        <?= t('Category:') ?> <strong><?= $this->text->e($task['category_id']) ?></strong>
    </li>
    <?php endif ?>
</ul>

<?php if (! empty($task['description'])): ?>
    <h2><?= t('Description') ?></h2>
    <?= $this->text->markdown($task['description'], true) ?>
<?php endif ?>

<?= $this->render('notification/footer', array('task' => $task)) ?>
</body>
</html>