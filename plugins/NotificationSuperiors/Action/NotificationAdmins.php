<?php

namespace Kanboard\Plugin\NotificationSuperiors\Action;

use Kanboard\Model\TaskModel;
use Kanboard\Action\Base;

use DateTimeImmutable;

/**
 * Send notification to superiors when due date is expired
 *
 * @package Kanboard\Plugin\NotificationSuperiors\Action
 * @author  Rzer_1
 */
class NotificationAdmins extends Base
{
    /**
     * Get action description
     *
     * @access public
     * @return string
     */
    public function getDescription()
    {
        return t('Send a task by email to admins');
    }

    /**
     * Get the list of compatible events
     *
     * @access public
     * @return array
     */
    public function getCompatibleEvents()
    {
        return array(
            TaskModel::EVENT_MOVE_COLUMN,
            TaskModel::EVENT_DAILY_CRONJOB,
        );
    }

    /**
     * Get the required parameter for the action
     *
     * @access public
     * @return array
     */
    public function getActionRequiredParameters()
    {
        return array(
            'subject' => t('Email subject'),
            //'hours' => array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23),
            //'minutes' => array(00,10,20,30,40,50),
            //'day_to_create_task' => array('Mon' => t('Monday'), 'Tue' => t('Tuesday'),etc...)`
            //'days_to_send' => t('Days to send notifications'),
            //'user_id' => t('User that will receive the email'),
        );
    }

    /**
     * Get all tasks
     *
     * @access public
     * @return array
     */

    public function getEventRequiredParameters()
    {
        return array();
    }

    /**
     * Check if the event data meet the action condition
     *
     * @access public
     * @param  array   $data   Event data dictionary
     * @return bool
     */
    public function hasRequiredCondition(array $data)
    {
        return true;
        //return date('D') == $this->getParam('day_to_create_task');
        //return $data['task']['column_id'] == $this->getParam('column_id');
        //return ! empty($data['task_id']);
    }
        

    public function doAction(array $data)
    {
        $results = array();
        //$hours = $this->getParam('hours');
        //$minutes = $this->getParam('minutes');
        //$daystosend = $this->getParam('days_to_send');

        if (date('i') == 10 || date('i') == 20 || date('i') == 30 || date('i') == 40 || date('i') == 50 || date('i') == 00) {
            
        }

        $duetasks = $this->taskFinderModel->getOverdueTasksQuery()->findAll();
        $daystosend = 3;
        $date = new DateTimeImmutable(date('Y-m-d'));
        foreach ($duetasks as $key => $value) {
            //Все просроченные задачи
            $duetask = $this->taskFinderModel->getById($value['id']);
            //время просроченной задачи и отправки уведомления
            $datedue = new DateTimeImmutable(date('Y-m-d', $duetask['date_due']));
            $datesend = $datedue->modify('+'.$daystosend.' day')->format('Y-m-d');
            $daysgone = $date->diff($datedue)->d;
            //echo $key. '<br>'; var_dump($daysgone); echo '<br><hr><br>';
            if ($datesend <= $date->format('Y-m-d')) {
                //группы в которых состоит исполнитель просроченной задачи
                $groups = $this->groupMemberModel->getGroups($duetask['owner_id']);
                //echo $key. '<br>'; var_dump($groups); echo '<br><hr><br>';
                $admins = array();
                
                foreach ($groups as $key => $elem) {
                    //$members = $this->groupMemberModel->getMembers($elem['id']);
                    $admins[] = $this->db->table('group_has_owners')->eq('group_id', $elem['id'])->findOne();
                }
                $daysgone = 3;
                $daystosend = 3;
                if ($daysgone >= $daystosend) {
                    $result = $daysgone / $daystosend;
                }else {
                    return;
                }
                //echo($result. '<br>');
                switch ($result) {
                    case 1:
                        //echo '<br>'; var_dump($admins); echo '<br><hr><br>';
                        $results[] = $this->sendEmail($duetask, $admins);
                        break;
                    case 2:
                        foreach ($admins as $key => $prop) {
                            if (isset($prop['user_id'])) {
                                $superiorsgroup = $this->groupRelatedModel->checkOwnerGroup($prop['group_id']);
                                if (! empty($superiorsgroup['group_owner_id'])) {
                                    $admins[] = $this->db->table('group_has_owners')->eq('group_id', $superiorsgroup['group_owner_id'])->findOne();
                                }else { break; }
                            }
                        }
                        //echo '<br>'; var_dump($admins); echo '<br><hr><br>';
                        $results[] = $this->sendEmail($duetask, $admins);
                        break;
                    case 3:
                        foreach ($admins as $key => $prop) {
                            if (isset($prop['user_id'])) {
                                $superiorsgroup = $this->groupRelatedModel->checkOwnerGroup($prop['group_id']);
                                if (! empty($superiorsgroup['group_owner_id'])) {
                                    $admins[] = $this->db->table('group_has_owners')->eq('group_id', $superiorsgroup['group_owner_id'])->findOne();
                                }else { break; }
                                $superiorsgroup2 = $this->groupRelatedModel->checkOwnerGroup($superiorsgroup['group_owner_id']);
                                if (! empty($superiorsgroup2['group_owner_id'])) {
                                    $admins[] = $this->db->table('group_has_owners')->eq('group_id', $superiorsgroup2['group_owner_id'])->findOne();
                                }else { break; }
                            }
                        }
                        //echo '<br>'; var_dump($admins); echo '<br><hr><br>';
                        $results[] = $this->sendEmail($duetask, $admins);
                        break;
                    case 4:
                        foreach ($admins as $key => $prop) {
                            if (isset($prop['user_id'])) {
                                $superiorsgroup = $this->groupRelatedModel->checkOwnerGroup($prop['group_id']);
                                if (! empty($superiorsgroup['group_owner_id'])) {
                                    $admins[] = $this->db->table('group_has_owners')->eq('group_id', $superiorsgroup['group_owner_id'])->findOne();
                                }else { break; }
                                $superiorsgroup2 = $this->groupRelatedModel->checkOwnerGroup($superiorsgroup['group_owner_id']);
                                if (! empty($superiorsgroup2['group_owner_id'])) {
                                    $admins[] = $this->db->table('group_has_owners')->eq('group_id', $superiorsgroup2['group_owner_id'])->findOne();
                                }else { break; }
                                $superiorsgroup3 = $this->groupRelatedModel->checkOwnerGroup($superiorsgroup2['group_owner_id']);
                                if (! empty($superiorsgroup3['group_owner_id'])) {
                                    $admins[] = $this->db->table('group_has_owners')->eq('group_id', $superiorsgroup3['group_owner_id'])->findOne();
                                }else { break; }
                            }
                        }
                        //echo '<br>'; var_dump($admins); echo '<br><hr><br>';
                        $results[] = $this->sendEmail($duetask, $admins);
                        break;
                    case 5:
                        foreach ($admins as $key => $prop) {
                            if (isset($prop['user_id'])) {
                                $superiorsgroup = $this->groupRelatedModel->checkOwnerGroup($prop['group_id']);
                                if (! empty($superiorsgroup['group_owner_id'])) {
                                    $admins[] = $this->db->table('group_has_owners')->eq('group_id', $superiorsgroup['group_owner_id'])->findOne();
                                }else { break; }
                                $superiorsgroup2 = $this->groupRelatedModel->checkOwnerGroup($superiorsgroup['group_owner_id']);
                                if (! empty($superiorsgroup2['group_owner_id'])) {
                                    $admins[] = $this->db->table('group_has_owners')->eq('group_id', $superiorsgroup2['group_owner_id'])->findOne();
                                }else { break; }
                                $superiorsgroup3 = $this->groupRelatedModel->checkOwnerGroup($superiorsgroup2['group_owner_id']);
                                if (! empty($superiorsgroup3['group_owner_id'])) {
                                    $admins[] = $this->db->table('group_has_owners')->eq('group_id', $superiorsgroup3['group_owner_id'])->findOne();
                                }else { break; }
                                $superiorsgroup4 = $this->groupRelatedModel->checkOwnerGroup($superiorsgroup3['group_owner_id']);
                                if (! empty($superiorsgroup4['group_owner_id'])) {
                                    $admins[] = $this->db->table('group_has_owners')->eq('group_id', $superiorsgroup4['group_owner_id'])->findOne();
                                }else { break; }
                            }
                        }
                        //echo '<br>'; var_dump($admins); echo '<br><hr><br>';
                        $results[] = $this->sendEmail($duetask, $admins);
                        break;
                        
                    default:
                        $results[] = $this->sendEmail($duetask, $admins);
                        break;
                }
            }
        }

        return in_array(true, $results, true);
        //return true;
    }
    
    private function sendEmail($duetask, $admins){

        foreach ($admins as $key => $value) {
            if (! empty($value['user_id'])) {
            $user = $this->userModel->getById($value['user_id']);
                $this->emailClient->send(
                    $user['email'],
                    $user['name'] ?: $user['username'],
                    $subject = $this->getParam('subject'),
                    $this->template->render('notification/task_create', array('task' => $duetask,))
                );
            }
        }
        return true;
    }
}