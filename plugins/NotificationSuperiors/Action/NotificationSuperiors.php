<?php

namespace Kanboard\Plugin\NotificationSuperiors\Action;

use Kanboard\Model\TaskModel;
use Kanboard\Action\Base;
use Kanboard\Core\Translator;

use DateTimeImmutable;

use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram as TelegramClass;
use Longman\TelegramBot\Exception\TelegramException;

/**
 * Send notification to superiors when due date is expired
 *
 * @package Kanboard\Plugin\NotificationSuperiors\Action
 * @author  Rzer_1
 */
class NotificationSuperiors extends Base
{
    /**
     * Get action description
     *
     * @access public
     * @return string
     */
    public function getDescription()
    {
        date_default_timezone_set($this->timezoneModel->getCurrentTimezone());
        $h = date('H');
        $i = date('i');
        $timezone = date_default_timezone_get();
        return t('Send notifications to superiors when due date is expired. Server Time: ' . $h . ':' . $i . ' - '. $timezone);
    }

    /**
     * Get the list of compatible events
     *
     * @access public
     * @return array
     */
    public function getCompatibleEvents()
    {
        return array(
            TaskModel::EVENT_UPDATE,
            TaskModel::EVENT_DAILY_CRONJOB,
            TaskModel::EVENT_MOVE_COLUMN,
        );
    }

    /**
     * Get the required parameter for the action
     *
     * @access public
     * @return array
     */
    public function getActionRequiredParameters()
    {
        return array(
            //'subject' => t('Email subject'),
            'hours' => array('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23'),
            'minutes' => array(
                '00',
                '10',
                '20',
                '30',
                '40',
                '50'
            ),
            //'hours' => t('Mailing hour(0-23)'),
            //'minutes' => t('Mailing minute(0,10,20 and so on)'),
            //'day_to_create_task' => array('Mon' => t('Monday'), 'Tue' => t('Tuesday'),etc...)`
            'days_to_send' => t('Days to send notifications'),
            //'user_id' => t('User that will receive the email'),
        );
    }

    /**
     * Get all tasks
     *
     * @access public
     * @return array
     */

    public function getEventRequiredParameters()
    {
        return array();
    }

    /**
     * Check if the event data meet the action condition
     *
     * @access public
     * @param  array   $data   Event data dictionary
     * @return bool
     */
    public function hasRequiredCondition(array $data)
    {
        return true;
        //return date('D') == $this->getParam('day_to_create_task');
        //return $data['task']['column_id'] == $this->getParam('column_id');
        //return ! empty($data['task_id']);
        //return count($data['task']) > 0;
        //return;
    }

    public function doAction(array $data)
    {
        date_default_timezone_set($this->timezoneModel->getCurrentTimezone());
        $results = array();
        $hours = $this->getParam('hours');
        $minutes = $this->getParam('minutes');
        //$hours = '09';
        //$minutes = '48';
        // $hours = date('H');
        // $minutes = date('i');

        if (date('H') == $hours && date('i') == $minutes) {

            $duetasks = $this->taskFinderModel->getOverdueTasksQuery()->findAll();
            $daystosend = $this->getParam('days_to_send');
            $date = new DateTimeImmutable(date('Y-m-d'));
            foreach ($duetasks as $key => $value) {
                //Все просроченные задачи
                $duetask = $this->taskFinderModel->getById($value['id']);
                if ($duetask['column_id'] == 4 || $duetask['column_id'] == 5) {
                    continue;
                }
                //время просроченной задачи и отправки уведомления
                $datedue = new DateTimeImmutable(date('Y-m-d', $duetask['date_due']));
                $datesend = $datedue->modify('+'.$daystosend.' day')->format('Y-m-d');
                $daysgone = $date->diff($datedue)->d;
                //echo $key. '<br>'; var_dump($daysgone); echo '<br><hr><br>';
                if ($datesend <= $date->format('Y-m-d')) {
                    //группы в которых состоит исполнитель просроченной задачи
                    $groups = $this->groupMemberModel->getGroups($duetask['owner_id']);
                    //echo $key. '<br>'; var_dump($groups); echo '<br><hr><br>';
                    $admins = array();
                    
                    foreach ($groups as $key => $elem) {
                        //$members = $this->groupMemberModel->getMembers($elem['id']);
                        $admins[] = $this->db->table('group_has_owners')->eq('group_id', $elem['id'])->findOne();
                    }
                    // $daysgone = 3; //тестовые данные
                    // $daystosend = 3; //тестовые данные
                    if ($daysgone >= $daystosend) { 
                        $result = $daysgone / $daystosend;
                    }else {
                        return;
                    }

                    //запись данных для передачи в сообщения уведомлений
                    $proj_name = $this->projectModel->getById(1);
                    $task_link = 'https://vk.acidhead.ru/?controller=ActivityController&action=task&task_id='.$duetask['id'];
                    $owner = $this->userModel->getById($duetask['owner_id']);
                    $creator = $this->userModel->getById($duetask['creator_id']);
                    if ($creator['name'] == null){
                        $creator_name = $creator['username'];
                    }else {
                        $creator_name = $creator['name'];
                    }
                    $datecreate = new DateTimeImmutable(date('Y-m-d', $duetask['date_creation']));
                    $column = $this->columnModel->getById($duetask['column_id']);
                    $messagedata = array(
                        'proj_name' => $proj_name['name'],
                        'task' => $duetask,
                        'task_link' => $task_link,
                        'user' => $owner['name'],
                        'creator_name' => $creator_name,
                        'subject' => 'Просрочена задача: ' . $duetask['title'] . '. Дней прошло: ' . $daysgone,
                        'datecreate' => $datecreate->format('Y-m-d'),
                        'datedue' => $datedue->format('Y-m-d'),
                        'daysgone' => $daysgone,
                        'column' => $column['title'],
                    );
                    //echo($result. '<br>');
                    switch ($result) {
                        case 1:
                            //echo '<br>'; var_dump($admins); echo '<br><hr><br>';
                            $results[] = $this->sendNotification($messagedata, $admins);
                            break;
                        case 2:
                            foreach ($admins as $key => $prop) {
                                if (isset($prop['user_id'])) {
                                    $superiorsgroup = $this->groupRelatedModel->checkOwnerGroup($prop['group_id']);
                                    if (! empty($superiorsgroup['group_owner_id'])) {
                                        $admins[] = $this->db->table('group_has_owners')->eq('group_id', $superiorsgroup['group_owner_id'])->findOne();
                                    }else { break; }
                                }
                            }
                            //echo '<br>'; var_dump($admins); echo '<br><hr><br>';
                            $results[] = $this->sendNotification($messagedata, $admins);
                            break;
                        case 3:
                            foreach ($admins as $key => $prop) {
                                if (isset($prop['user_id'])) {
                                    $superiorsgroup = $this->groupRelatedModel->checkOwnerGroup($prop['group_id']);
                                    if (! empty($superiorsgroup['group_owner_id'])) {
                                        $admins[] = $this->db->table('group_has_owners')->eq('group_id', $superiorsgroup['group_owner_id'])->findOne();
                                    }else { break; }
                                    $superiorsgroup2 = $this->groupRelatedModel->checkOwnerGroup($superiorsgroup['group_owner_id']);
                                    if (! empty($superiorsgroup2['group_owner_id'])) {
                                        $admins[] = $this->db->table('group_has_owners')->eq('group_id', $superiorsgroup2['group_owner_id'])->findOne();
                                    }else { break; }
                                }
                            }
                            //echo '<br>'; var_dump($admins); echo '<br><hr><br>';
                            $results[] = $this->sendNotification($messagedata, $admins);
                            break;
                        case 4:
                            foreach ($admins as $key => $prop) {
                                if (isset($prop['user_id'])) {
                                    $superiorsgroup = $this->groupRelatedModel->checkOwnerGroup($prop['group_id']);
                                    if (! empty($superiorsgroup['group_owner_id'])) {
                                        $admins[] = $this->db->table('group_has_owners')->eq('group_id', $superiorsgroup['group_owner_id'])->findOne();
                                    }else { break; }
                                    $superiorsgroup2 = $this->groupRelatedModel->checkOwnerGroup($superiorsgroup['group_owner_id']);
                                    if (! empty($superiorsgroup2['group_owner_id'])) {
                                        $admins[] = $this->db->table('group_has_owners')->eq('group_id', $superiorsgroup2['group_owner_id'])->findOne();
                                    }else { break; }
                                    $superiorsgroup3 = $this->groupRelatedModel->checkOwnerGroup($superiorsgroup2['group_owner_id']);
                                    if (! empty($superiorsgroup3['group_owner_id'])) {
                                        $admins[] = $this->db->table('group_has_owners')->eq('group_id', $superiorsgroup3['group_owner_id'])->findOne();
                                    }else { break; }
                                }
                            }
                            //echo '<br>'; var_dump($admins); echo '<br><hr><br>';
                            $results[] = $this->sendNotification($messagedata, $admins);
                            break;
                        case 5:
                            foreach ($admins as $key => $prop) {
                                if (isset($prop['user_id'])) {
                                    $superiorsgroup = $this->groupRelatedModel->checkOwnerGroup($prop['group_id']);
                                    if (! empty($superiorsgroup['group_owner_id'])) {
                                        $admins[] = $this->db->table('group_has_owners')->eq('group_id', $superiorsgroup['group_owner_id'])->findOne();
                                    }else { break; }
                                    $superiorsgroup2 = $this->groupRelatedModel->checkOwnerGroup($superiorsgroup['group_owner_id']);
                                    if (! empty($superiorsgroup2['group_owner_id'])) {
                                        $admins[] = $this->db->table('group_has_owners')->eq('group_id', $superiorsgroup2['group_owner_id'])->findOne();
                                    }else { break; }
                                    $superiorsgroup3 = $this->groupRelatedModel->checkOwnerGroup($superiorsgroup2['group_owner_id']);
                                    if (! empty($superiorsgroup3['group_owner_id'])) {
                                        $admins[] = $this->db->table('group_has_owners')->eq('group_id', $superiorsgroup3['group_owner_id'])->findOne();
                                    }else { break; }
                                    $superiorsgroup4 = $this->groupRelatedModel->checkOwnerGroup($superiorsgroup3['group_owner_id']);
                                    if (! empty($superiorsgroup4['group_owner_id'])) {
                                        $admins[] = $this->db->table('group_has_owners')->eq('group_id', $superiorsgroup4['group_owner_id'])->findOne();
                                    }else { break; }
                                }
                            }
                            //echo '<br>'; var_dump($admins); echo '<br><hr><br>';
                            $results[] = $this->sendNotification($messagedata, $admins);
                            break;
                            
                        default:
                            $results[] = $this->sendNotification($messagedata, $admins);
                            break;
                    }
                }
            }

            return in_array(true, $results, true);
            //return true;
        }else {
            return false;
        }
    }
    private function sendNotification($messagedata, $admins){
        foreach ($admins as $key => $value) {
            if (! empty($value['user_id'])) {
                $this->sendEmail($messagedata, $value);
                $this->sendTelegram($messagedata, $value);
            }
            break;
        }
        return true;
    }
    
    private function sendEmail($messagedata, $value){

        $user = $this->userModel->getById($value['user_id']);
        //$user = $this->userModel->getById(8);
        $this->emailClient->send(
            $user['email'],
            $user['name'] ?: $user['username'],
            $messagedata['subject'],
            $this->template->render('notification/tasks_dueng_superiors', array('task' => $messagedata['task'], 'messagedata' => $messagedata))
        );   

    }
    private function sendTelegram($messagedata, $value)
    {
        $user = $this->userModel->getById($value['user_id']);
        //$user = $this->userModel->getById(8);
        $apikey = $this->userMetadataModel->get($user['id'], 'telegram_apikey', $this->configModel->get('telegram_apikey'));
        $bot_username = $this->userMetadataModel->get($user['id'], 'telegram_username', $this->configModel->get('telegram_username'));
        $chat_id = $this->userMetadataModel->get($user['id'], 'telegram_user_cid');
        $forward_attachments = $this->userMetadataModel->get($user['id'], 'forward_attachments', $this->configModel->get('forward_attachments'));
        $telegram_proxy = $this->userMetadataModel->get($user['id'], 'telegram_proxy', $this->configModel->get('telegram_proxy'));

        // Get required data
        
        $proj_name = $messagedata['proj_name'];
        $title = $messagedata['subject'];
        $date_created = 'Создана: '.$messagedata['datecreate'];
        $date_due = 'Завершается: '.$messagedata['datedue'];
        $performer = 'Исполнитель: '.$messagedata['user'];
        $column = 'Колонка на доске: '.$messagedata['column'];
        $position = 'Позиция задачи: '.$messagedata['task']['position'];
        if ($messagedata['task']['creator_id'] == $value) {
            $creator = 'Создатель: '.$messagedata['creator_name'].' (Вы)';
        }else{
            $creator = 'Создатель: '.$messagedata['creator_name'];
        }
        if ($messagedata['creator_name'] == null) {
            $creator = 'Создатель: Система';
        }

        // Build message
        
        $message = "[".htmlspecialchars($proj_name, ENT_NOQUOTES | ENT_IGNORE)."]\n";
        //$message .= htmlspecialchars($title, ENT_NOQUOTES | ENT_IGNORE)."\n";
        $message .= 'Просрочена задача: <a href="'.$messagedata['task_link'].'">' . $messagedata['task']['title'] . '</a>. Дней прошло: ' . $messagedata['daysgone']."\n";
        $message .= htmlspecialchars($date_created, ENT_NOQUOTES | ENT_IGNORE)."\n";
        $message .= htmlspecialchars($date_due, ENT_NOQUOTES | ENT_IGNORE)."\n";
        $message .= htmlspecialchars($performer, ENT_NOQUOTES | ENT_IGNORE)."\n";
        $message .= htmlspecialchars($column, ENT_NOQUOTES | ENT_IGNORE)."\n";
        $message .= htmlspecialchars($position, ENT_NOQUOTES | ENT_IGNORE)."\n";
        $message .= htmlspecialchars($creator, ENT_NOQUOTES | ENT_IGNORE)."\n";
        
        // Send Message
        try
        {   

            // Create Telegram API object
            $telegram = new TelegramClass($apikey, $bot_username);
            // Setup proxy details if set in kanboard configuration
            if ($telegram_proxy == '')
	        {
                Request::setClient(new \GuzzleHttp\Client([
	                    'base_uri' => 'https://api.telegram.org',
                        'timeout' => 20,
                        'verify' => false,
                        'proxy'   => $telegram_proxy,
                ]));
                
            }

            // Message pay load
            $data = array('chat_id' => $chat_id, 'text' => $message, 'parse_mode' => 'HTML');
            
            // Send message
            $result = Request::sendMessage($data);
            
        }
        catch (TelegramException $e)
        {
            // log telegram errors
            error_log($e->getMessage());
        }
    }
}