<?php

namespace Kanboard\Plugin\TaskClosing;

use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Translator;

class Plugin extends Base
{
    public function initialize()
    {
        $this->template->setTemplateOverride('task_status/close', 'TaskClosing:task_status/close');
        $this->template->setTemplateOverride('task/dropdown', 'TaskClosing:task/dropdown');
        $this->template->setTemplateOverride('task/sidebar', 'TaskClosing:task/sidebar');
    }

    public function onStartup()
    {
        Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getClasses() {
        return [
            'Plugin\TaskClosing\Model' => [
                'TaskClosingModel',
            ],
            'Plugin\CheckListPlugin\Model' => [
                'CheckListModel'
            ]
        ];
    }

    public function getPluginName()
    {
        return 'TaskClosing';
    }

    public function getPluginDescription()
    {
        return t('moving to done when task closing');
    }

    public function getPluginAuthor()
    {
        return 'Rustam Urazov';
    }

    public function getPluginVersion()
    {
        return '1.0.0';
    }

    public function getPluginHomepage()
    {
        return 'https://github.com/kanboard/plugin-myplugin';
    }
}

