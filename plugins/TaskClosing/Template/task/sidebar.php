<?php use Kanboard\Plugin\VectorTasks\Model\ProofModel;?>

<div class="sidebar sidebar-icons">
    <div class="sidebar-title">
        <h2><?=t('Task #%d', $task['id'])?></h2>
    </div>
    <ul>
        <li <?=$this->app->checkMenuSelection('TaskViewController', 'show')?>>
            <?=$this->url->icon('newspaper-o', t('Summary'), 'VectorTaskViewController', 'show', array('plugin' => 'VectorTasks', 'task_id' => $task['id']))?>
        </li>
        <li <?=$this->app->checkMenuSelection('ActivityController', 'task')?>>
            <?=$this->url->icon('dashboard', t('Activity stream'), 'ActivityController', 'task', array('task_id' => $task['id']))?>
        </li>
        <?php if ($task['time_estimated'] > 0 || $task['time_spent'] > 0): ?>
        <li <?=$this->app->checkMenuSelection('TaskViewController', 'timetracking')?>>
            <?=$this->url->icon('clock-o', t('Time tracking'), 'VectorTaskViewController', 'timetracking', array('plugin' => 'VectorTasks', 'task_id' => $task['id']))?>
        </li>
        <?php endif?>

        <?=$this->hook->render('template:task:sidebar:information', array('task' => $task))?>
    </ul>

    <?php if ($this->user->hasProjectAccess('TaskModificationController', 'edit', $task['project_id'])): ?>
    <div class="sidebar-title">
        <h2><?=t('Actions')?></h2>
    </div>
    <ul>
        <?=$this->hook->render('template:task:sidebar:before-actions', array('task' => $task))?>

        <?php if ($this->projectRole->canUpdateTask($task) && ($this->user->getId() == $task['controller_id'] || $this->user->getId() == $task['creator_id'])): ?>
        <li>
            <?=$this->modal->large('edit', t('Edit the task'), 'TaskModificationController', 'edit', array('task_id' => $task['id']))?>
        </li>
        <?php endif?>
        <?=$this->hook->render('template:task:sidebar:after-basic-actions', array('task' => $task))?>
        <?=$this->hook->render('template:task:sidebar:after-add-links', array('task' => $task))?>

        <li>
            <?=$this->modal->small('comment-o', t('Add a comment'), 'CommentController', 'create', array('task_id' => $task['id']))?>
        </li>
        <?=$this->hook->render('template:task:sidebar:after-add-comment', array('task' => $task))?>

        <li>
            <?=$this->modal->medium('file', t('Attach a document'), 'TaskFileController', 'create', array('task_id' => $task['id']))?>
        </li>
        <li>
            <?=$this->modal->medium('camera', t('Add a screenshot'), 'TaskFileController', 'screenshot', array('task_id' => $task['id']))?>
        </li>
        <?=$this->hook->render('template:task:sidebar:after-add-attachments', array('task' => $task))?>

        <li>
            <?=$this->modal->small('files-o', t('Duplicate'), 'VectorTaskDuplicationController', 'duplicate', array('plugin' => 'VectorTasks', 'task_id' => $task['id']))?>
        </li>
        <?=$this->hook->render('template:task:sidebar:after-duplicate-task', array('task' => $task))?>
        <li>
            <?=$this->modal->small('paper-plane', t('Send by email'), 'TaskMailController', 'create', array('task_id' => $task['id']))?>
        </li>
        <?=$this->hook->render('template:task:sidebar:after-send-mail', array('task' => $task))?>
        <?php if ($this->proofHelper->proofExist($task['id'])): ?>
        <?=$this->modal->small('plus', 'Добавить доказательство', 'ProofTaskController', 'show', array('task_id' => $task['id'], 'plugin' => 'VectorTasks'))?>
        <?php endif;?>
        <?php if ($task['is_active'] == 1 && $this->projectRole->isSortableColumn($task['project_id'], $task['column_id'])): ?>
            <li>
                <?=$this->modal->small('arrows', t('Move position'), 'TaskMovePositionController', 'show', array('task_id' => $task['id']))?>
            </li>
        <?php endif?>
        <?php if ($task['column_id'] == 1 && $task['owner_id'] == $this->user->getId()): ?>
            <li>
                <?=$this->modal->small('accept', 'Принять', 'TaskReplyingController', 'accept', array('task_id' => $task['id'], 'project_id' => $task['project_id'], 'plugin' => 'RespondToTask'))?>
            </li>
            <li>
                <?=$this->modal->small('reject', 'Отклонить', 'TaskReplyingController', 'reject', array('task_id' => $task['id'], 'project_id' => $task['project_id'], 'plugin' => 'RespondToTask'))?>
            </li>
        <?php endif?>

        <?php if ($task['column_id'] == 3 && $task['owner_id'] == $this->user->getId()): ?>
            <li>
                <?=$this->modal->small('send_for_verification', 'Отправить на проверку', 'TaskReplyingController', 'sendForVerification', array('task_id' => $task['id'], 'project_id' => $task['project_id'], 'plugin' => 'RespondToTask'))?>
            </li>
        <?php endif?>

        <?php if ($task['column_id'] == 5 && $task['creator_id'] == $this->user->getId()): ?>
            <li>
                <?=$this->modal->small('send_for_revision', 'Отправить на доработку', 'TaskReplyingController', 'sendForRevision', array('task_id' => $task['id'], 'project_id' => $task['project_id'], 'plugin' => 'RespondToTask'))?>
            </li>
        <?php endif?>

        <?php if ($this->user->getId() == $task['owner_id']): ?>
            <?php if (isset($task['is_active']) && $this->projectRole->canChangeTaskStatusInColumn($task['project_id'], $task['column_id'])): ?>
                <li>
                    <?php if ($task['is_active'] == 1): ?>
                        <?= $this->modal->confirm('times', t('Close this task'), 'TaskClosingController', 'close', array('task_id' => $task['id'], 'plugin' => 'TaskClosing')) ?>
                    <?php else: ?>
                        <?= $this->modal->confirm('check-square-o', t('Open this task'), 'TaskStatusController', 'open', array('task_id' => $task['id'])) ?>
                    <?php endif ?>
                </li>
            <?php endif ?>
        <?php endif ?>

        <?php if ($this->user->getId() == $task['controller_id'] || $this->user->getId() == $task['creator_id']): ?>
            <?php if ($this->projectRole->canRemoveTask($task)): ?>
                <li>
                    <?= $this->modal->confirm('trash-o', t('Remove'), 'TaskSuppressionController', 'confirm', array('task_id' => $task['id'], 'redirect' => 'board')) ?>
                </li>
            <?php endif ?>
        <?php endif ?>

        <?=$this->hook->render('template:task:sidebar:actions', array('task' => $task))?>
    </ul>
    <?php endif?>
</div>
