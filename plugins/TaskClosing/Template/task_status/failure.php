<div class="page-header">
    <h2><?= t('Close a task') ?></h2>
</div>

<div class="confirm">
    <p class="alert alert-info">
        Чтобы закрыть задачу, нужно заполнить чек-лист!
    </p>
</div>
