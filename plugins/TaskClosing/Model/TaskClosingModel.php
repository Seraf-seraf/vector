<?php

namespace Kanboard\Plugin\TaskClosing\Model;

use Kanboard\Model\TaskModel;
use Kanboard\Model\TaskStatusModel;

class TaskClosingModel extends TaskStatusModel {
    public function close($task_id)
    {
        $this->subtaskStatusModel->closeAll($task_id);
        return $this->changeStatus($task_id, TaskModel::STATUS_CLOSED, time(), TaskModel::EVENT_CLOSE);
    }

    private function changeStatus($task_id, $status, $date_completed, $event_name)
    {
        if (! $this->taskFinderModel->exists($task_id)) {
            return false;
        }

        $result = $this->db
                        ->table(TaskModel::TABLE)
                        ->eq('id', $task_id)
                        ->update(array(
                            'column_id' => 4,
                            'is_active' => $status,
                            'date_completed' => $date_completed,
                            'date_modification' => time(),
                        ));

        if ($result) {
            $this->queueManager->push($this->taskEventJob->withParams($task_id, array($event_name)));
        }

        return $result;
    }
}