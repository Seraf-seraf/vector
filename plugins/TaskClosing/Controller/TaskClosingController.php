<?php

namespace Kanboard\Plugin\TaskClosing\Controller;

use Kanboard\Controller\TaskStatusController;

class TaskClosingController extends TaskStatusController {
    public function close()
    {   
        $task = $this->getTask();
        $completedCheckList = $this->checkListModel->checkCompletingChecklist($task['id']);

        if ($completedCheckList) {
            $this->changeStatus('close', 'task_status/close', t('Task closed successfully.'), t('Unable to close this task.'));
        } else {
            $this->response->html($this->template->render('TaskClosing:task_status/failure', array('completedCheckList' => $completedCheckList)));
        }

    }

    private function changeStatus($method, $template, $success_message, $failure_message)
    {
        $task = $this->getTask();
        if ($this->request->getStringParam('confirmation') === 'yes') {
            $this->checkCSRFParam();

            if ($this->taskClosingModel->$method($task['id'])) {
                $this->flash->success($success_message);
            } else {
                $this->flash->failure($failure_message);
            }

            $this->response->redirect($this->helper->url->to('TaskViewController', 'show', array('task_id' => $task['id'])), true);
        } else {
            $this->response->html($this->template->render($template, array(
                'task' => $task,
            )));
        }
    }
}