<?php

namespace Kanboard\Plugin\StyleFixerMobile\Controller;

use Kanboard\Controller\BaseController;

use Kanboard\Filter\TaskProjectFilter;
use Kanboard\Model\TaskModel;

use Kanboard\Plugin\VectorTasks\Formatter\VectorBoardFormatter;
use Kanboard\Plugin\TasksParticipations\Formatter\ParticipationTaskBoardformatter;
use Kanboard\Plugin\ControlledTasks\Formatter\ControlledTaskBoardFormatter;

class FixerMobileController extends BaseController {
    
    /**
     * Show list view for projects
     *
     * @access public
     */
    public function show()
    {
        $project = $this->getProject();
        $search = $this->helper->projectHeader->getSearchQuery($project);

        if ($this->request->getIntegerParam('show_subtasks') !== 0 ||
            $this->request->getIntegerParam('hide_subtasks') !== 0 ||
            $this->request->getStringParam('direction') !== '' ||
            $this->request->getStringParam('order') !== '') {
            $this->checkReusableGETCSRFParam();
        }

        if ($this->request->getIntegerParam('show_subtasks')) {
            session_set('subtaskListToggle', true);
        } elseif ($this->request->getIntegerParam('hide_subtasks')) {
            session_set('subtaskListToggle', false);
        }

        if ($this->userSession->hasSubtaskListActivated()) {
            $formatter = $this->taskListSubtaskFormatter;
        } else {
            $formatter = $this->taskListFormatter;
        }

        list($order, $direction) = $this->userSession->getListOrder($project['id']);
        $direction = $this->request->getStringParam('direction', $direction);
        $order = $this->request->getStringParam('order', $order);
        $this->userSession->setListOrder($project['id'], $order, $direction);

        // $projectID = $project['id'];
        // $userID = $this->getUser()['id'];
        // $tasks = $this->fixerMobileModel->getMyTasks($projectID, $userID);

        $paginator = $this->paginator
            ->setUrl('TaskListController', 'show', array('project_id' => $project['id'], 'csrf_token' => $this->token->getReusableCSRFToken()))
            ->setMax(30)
            ->setOrder($order)
            ->setDirection($direction)
            ->setFormatter($formatter)
            ->setQuery($this->taskLexer
                ->build($search)
                ->withFilter(new TaskProjectFilter($project['id']))
                ->getQuery()
            )
            ->calculate();
        
        $task = $this->request->getStringParam('task');
        switch ($task) {
            case 'owner':
                $swimlanes = $this->taskLexer
                ->build('')
                ->format($this->vectorBoardFormatter->withProjectId($project['id']),$this->vectorBoardFormatter->withUserId($this->getUser()['id']));
                break;
            case 'participant':
                $swimlanes = $this->taskLexer
                ->build('')
                ->format($this->participationTaskBoardFormatter->withProjectId($project['id']),$this->participationTaskBoardFormatter->withUserId($this->getUser()['id']));
                break;
            case 'creator':
                $swimlanes = $this->taskLexer
                ->build('')
                ->format($this->controlledTaskBoardFormatter->withProjectId($project['id'])->withUserId($this->getUser()['id']));
                break;
            default:
                $swimlanes = null;
                break;
        }
        

        $this->response->html($this->helper->layout->app('task_list/listing', array(
            'project'     => $project,
            'title'       => $project['name'],
            'task'        => $this->request->getStringParam('task'),
            'description' => $this->helper->projectHeader->getDescription($project),
            'paginator'   => $paginator,
            'swimlanes'   => $swimlanes
        )));
    }
}