<?php

namespace Kanboard\Plugin\StyleFixerMobile;

use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Translator;

class Plugin extends Base
{
    public function initialize()
    {
        // CSS - Asset Hook
        //  - Keep filename lowercase
        $this->hook->on('template:layout:css', array('template' => 'plugins/StyleFixerMobile/Assets/css/fixer.css'));
        $this->template->setTemplateOverride('task_list/listing', 'StyleFixerMobile:task_list/listing');
    }

    public function onStartup()
    {
        //Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getPluginName()
    {
        return 'StyleFixerMobile';
    }

    public function getPluginDescription()
    {
        return t('CSS correction plugin for the mobile version of the site.');
    }

    public function getPluginAuthor()
    {
        return 'Rzer_1';
    }

    public function getPluginVersion()
    {
        return '1.0.0';
    }

    public function getCompatibleVersion()
    {
        return '>=1.0.0';
    }

    public function getPluginHomepage()
    {
        return 'https://github.com/';
    }
    public function getClasses()
    {
        return [
            'Plugin\StyleFixerMobile\Model' => [
                'FixerMobileModel',
            ],
            'Plugin\VectorTasks\Model' => [
                'TaskCreationModel',
                'VectorTaskFileModel',
                'RecurrenceTaskModel',
                'VectorTaskDuplicationModel',
                'VectorTaskRecurrenceModel',
            ],
            'Plugin\VectorTasks\Formatter' =>[
                'VectorBoardFormatter',
                'VectorBoardSwimlaneFormatter',
                'VectorBoardTaskFormatter',
                'VectorBoardColumnFormatter',
            ],
        ];
    }
}
