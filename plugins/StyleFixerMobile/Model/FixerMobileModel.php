<?php

namespace Kanboard\Plugin\StyleFixerMobile\Model;
use Kanboard\Core\Base;
use Kanboard\Model\TaskModel;

class FixerMobileModel extends Base
{
    const TABLE = 'tasks';

    public function getMyTasks($projectID, $userID) {
        return $this->db->table(self::TABLE)
            ->eq('project_id', $projectID)
            ->eq('owner_id',$userID)
            ->asc('position')
            ->findAll();
    }

}