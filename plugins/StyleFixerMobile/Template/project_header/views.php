<ul class="views">
    <li <?= $this->app->checkMenuSelection('ProjectOverviewController') ?>>
        <?= $this->url->icon('eye', t('Overview'), 'ProjectOverviewController', 'show', array('project_id' => $project['id'], 'search' => $filters['search']), false, 'view-overview', t('Keyboard shortcut: "%s"', 'v o')) ?>
    </li>

    <?= $this->hook->render('template:project-header:view-switcher-before-board-view', array('project' => $project, 'filters' => $filters)) ?>

    <div class="desktop-ver">
        <li <?= $this->app->checkMenuSelection('VectorBoardController') ?>>
            <?= $this->url->icon('th', t('Мои'), 'VectorBoardController', 'show_my_tasks', array('plugin' => 'VectorTasks','project_id' => $project['id'], 'search' => $filters['search'], 'task' => 'owner'), false, 'view-board', t('Keyboard shortcut: "%s"', 'v b')) ?>
        </li>
        <li <?= $this->app->checkMenuSelection('ParticipationTaskBoardController') ?>>
            <?= $this->url->icon('th', t('Участвую'), 'ParticipationTaskBoardController', 'show', array('plugin' => 'TasksParticipations','project_id' => $project['id'], 'search' => $filters['search'], 'task' => 'participant'), false, 'view-board', t('Keyboard shortcut: "%s"', 'v b')) ?>
        </li>
        <li <?= $this->app->checkMenuSelection('ControlledTaskBoardController') ?>>
            <?= $this->url->icon('th', t('Контролирую'), 'ControlledTaskBoardController', 'show', array('plugin' => 'ControlledTasks','project_id' => $project['id'], 'search' => $filters['search'], 'task' => 'creator'), false, 'view-board', t('Keyboard shortcut: "%s"', 'v b')) ?>
        </li>
    </div>
    
    <div class="mobile-ver">
        <li <?= $this->app->checkMenuSelection('VectorBoardController') ?>>
            <?= $this->url->icon('list', t('Мои'), 'VectorBoardController', 'show_my_tasks', array('plugin' => 'VectorTasks','project_id' => $project['id'], 'search' => $filters['search'], 'task' => 'owner'), false, 'view-board', t('Keyboard shortcut: "%s"', 'v b')) ?>
        </li>
        <li <?= $this->app->checkMenuSelection('ParticipationTaskBoardController') ?>>
            <?= $this->url->icon('list', t('Участвую'), 'ParticipationTaskBoardController', 'show', array('plugin' => 'TasksParticipations','project_id' => $project['id'], 'search' => $filters['search'], 'task' => 'participant'), false, 'view-board', t('Keyboard shortcut: "%s"', 'v b')) ?>
        </li>
        <li <?= $this->app->checkMenuSelection('ControlledTaskBoardController') ?>>
            <?= $this->url->icon('list', t('Контролирую'), 'ControlledTaskBoardController', 'show', array('plugin' => 'ControlledTasks','project_id' => $project['id'], 'search' => $filters['search'], 'task' => 'creator'), false, 'view-board', t('Keyboard shortcut: "%s"', 'v b')) ?>
        </li>
    </div>

    <?= $this->hook->render('template:project-header:view-switcher-before-task-list', array('project' => $project, 'filters' => $filters)) ?>

    <li <?= $this->app->checkMenuSelection('TaskListController') ?>>
        <?= $this->url->icon('list', t('List'), 'TaskListController', 'show', array('project_id' => $project['id'], 'search' => $filters['search']), false, 'view-listing', t('Keyboard shortcut: "%s"', 'v l')) ?>
    </li>

    <?= $this->hook->render('template:project-header:view-switcher', array('project' => $project, 'filters' => $filters)) ?>
</ul>
