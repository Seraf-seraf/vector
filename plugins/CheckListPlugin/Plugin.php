<?php

namespace Kanboard\Plugin\CheckListPlugin;

use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Translator;

class Plugin extends Base
{
    public function initialize()
    {        
        $this->hook->on('template:layout:js', array('template' => 'plugins/CheckListPlugin/Assets/checkList.js'));
        $this->hook->on('template:layout:css', array('template' => 'plugins/CheckListPlugin/Assets/main.css'));
        $this->helper->register('checkListHelper', 'Kanboard\Plugin\CheckListPlugin\Helper\CheckListHelper');
        $this->helper->register('recurrenceCheckListHelper', 'Kanboard\Plugin\CheckListPlugin\Helper\RecurrenceCheckListHelper');
        $this->template->hook->attachCallable('template:task:form:first-column', 'CheckListPlugin:checklist/checklist', function () {
            $taskId = $this->request->getStringParam('task_id');
            if (empty($taskId)) {
                $taskId = 0;
            }
            return array(
                'checklist' => $this->checkListModel->getCheckboxes($taskId),
                'task' => $this->taskFinderModel->getById($taskId)
            );
        });
    }

    public function getClasses() {
        return [
            'Plugin\CheckListPlugin\Model' => [
                'CheckListModel',
                'RecurrenceTaskCheckListModel',
            ],
            'Plugin\CheckListPlugin\Controller' => [
                'CheckListController',
            ],
        ];
    }

    public function onStartup()
    {
        Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getPluginName()
    {
        return 'CheckListPlugin';
    }

    public function getPluginDescription()
    {
        return t('create checklist for task');
    }

    public function getPluginAuthor()
    {
        return 'seraf-seraf';
    }

    public function getPluginVersion()
    {
        return '1.0.0';
    }

    public function getPluginHomepage()
    {
        return 'https://github.com/kanboard/plugin-myplugin';
    }
}

