<?php

namespace Kanboard\Plugin\CheckListPlugin\Class;

class CheckListItem
{
    private $title;
    private $userId;
    private $isCheckbox;

    public function __construct($userId, $title = 'Цель')
    {
        $this->title = $title;
        $this->userId = $userId;
        $this->isCheckbox = 1;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getRole()
    {
        return $this->isCheckbox;
    }
}