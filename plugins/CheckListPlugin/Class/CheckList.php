<?php

namespace Kanboard\Plugin\CheckListPlugin\Class;

use Kanboard\Plugin\CheckListPlugin\Class\CheckListItem;

class CheckList
{
    private $items;

    public function __construct($userId, ?array $data)
    {
        $this->createItems($data, (int) $userId);
    }

    public function createItems(?array $data, $userId)
    {
        foreach ($data as $title) {
            if (self::checkTitle($title)) {
                continue;
            }
            $this->items[] = new CheckListItem($userId, $title);
        }
    }

    public static function prepareDataForEdit(?array $data) {
        $items = array();

        foreach ($data as $name => $value) {
            if (strpos($name, 'checklist-') === 0) {
                $id = explode('-', $name)[1];

                if (self::checkTitle($value)) {
                    continue;
                }

                $items[] = ['id' => $id, 'title' => $value];
            }
        }

        return $items;
    }

    public function getItems()
    {
        return $this->items;
    }

    private static function checkTitle($title)
    {
        return empty($title);
    }
}