<?php

namespace Kanboard\Plugin\CheckListPlugin\Schema;

use PDO;

const VERSION = 8;

function version_8(PDO $pdo)
{
    $pdo->exec(
        "ALTER TABLE subtasks 
     ADD COLUMN recurrence_id INT DEFAULT NULL,
     ADD CONSTRAINT fk_parent_checkbox 
     FOREIGN KEY (recurrence_id) REFERENCES recurrence_tasks_has_checklist(id);"
    );
}

function version_7(PDO $pdo) {
    $pdo->exec(
        "ALTER TABLE recurrence_tasks_has_checklist DROP COLUMN status;"
    );
}


function version_6(PDO $pdo) {
    $pdo->exec(
        "ALTER TABLE recurrence_tasks_has_checklist
        ALTER COLUMN status SET DEFAULT 0;"
    );
}

function version_5(PDO $pdo) {
    $pdo->exec(
        "CREATE TABLE recurrence_tasks_has_checklist (
        id serial PRIMARY KEY,
        recurrence_id INTEGER NOT NULL, 
        title TEXT NOT NULL,
        status SMALLINT NOT NULL,
        FOREIGN KEY (recurrence_id) REFERENCES recurrence_tasks(id) ON UPDATE NO ACTION ON DELETE CASCADE
        );"
    );
}

function version_1(PDO $pdo)
{
    $pdo->exec("ALTER TABLE subtasks ADD COLUMN isCheckbox INTEGER DEFAULT 0");
}
