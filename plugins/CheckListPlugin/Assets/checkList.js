KB.on("modal.afterRender", function () {
    const container = document.querySelector('.add-checklist');
    const item = container.querySelector('.new-checklist-item');
    const button = document.querySelector('.button.add-checklist-button.fa.fa-plus');
    const closeButton = container.querySelector('.add-checklist li i');

    function createCheckListItem() {
        const newItem = item.cloneNode(true);
        const newInput = newItem.querySelector('input#new-checklist-text');
        
        newInput.setAttribute('value', 'Цель');
        newInput.setAttribute('name', 'new-checklist-text[]');
        newInput.setAttribute('placeholder', 'Цель');

        container.appendChild(newItem);
    }

    button.addEventListener('click', (event) => {
        event.preventDefault();

        createCheckListItem();
    });

    // Удаление строки

    function removeParent(event) {
        const item = event.target.closest('.add-checklist li i');

        if (item) {
            item.parentElement.remove();
        }
    }

    container.addEventListener('click', removeParent)
});

