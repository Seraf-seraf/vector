<?php

namespace Kanboard\Plugin\CheckListPlugin\Controller;

class RecurrenceCheckListController extends CheckListController
{
    public function change()
    {
        $this->checkReusableGETCSRFParam();
        $task = array();
        $subtask = $this->recurrenceTaskCheckListModel->getCheckListItem($this->request->getIntegerParam('subtask_id'));

        $status = $this->recurrenceTaskCheckListModel->toggleStatus($subtask['id']);
        $subtask['status'] = $status;

        $html = $this->helper->recurrenceCheckListHelper->renderToggleStatus($task, $subtask);

        $this->response->html($html);
    }

    public function confirm()
    {
        $task_id = $this->request->getIntegerParam('task_id');
        $checklistItem_id = $this->request->getIntegerParam('subtask_id');

        $this->db->table('recurrence_tasks_has_checklist')
            ->eq('recurrence_id', $task_id)
            ->eq('id', $checklistItem_id)
            ->remove();

        $checklist = $this->db->table('recurrence_tasks_has_checklist')
            ->eq('recurrence_id', $task_id)
            ->findAll();

//        $this->response->html(
//            $this->template->render(
//                'CheckListPlugin:recurrence_checklist/checklist', [
//                    'checklist' => $checklist
//                ]
//            )
//        );

        return
            $this->template->render(
                'CheckListPlugin:recurrence_checklist/checklist', [
                    'checklist' => $checklist
                ]
            );
    }
}