<?php

namespace Kanboard\Plugin\CheckListPlugin\Controller;

use Kanboard\Controller\SubtaskStatusController;

class CheckListController extends SubtaskStatusController
{
    public function change()
    {
        $this->checkReusableGETCSRFParam();
        $task = $this->getTask();
        $subtask = $this->getSubtask($task);
        
        $status = $this->checkListModel->toggleStatus($subtask['id']);

        $subtask['status'] = $status;

        $html = $this->helper->checkListHelper->renderToggleStatus($task, $subtask);

        $this->response->html($html);
    }

    /**
     * Render table
     *
     * @access protected
     * @param  array  $task
     * @return string
     */
    protected function renderTable(array $task)
    {
        return $this->template->render('CheckListPlugin:checklist/table', array(
            'task'     => $task,
            'checklist' => $this->checkListModel->getCheckboxes($task['id']),
            'editable' => true,
        ));
    }

    public function confirm()
    {
        $task = $this->getTask();
        $target = $this->getSubtask($task);

        $this->response->html($this->template->render('CheckListPlugin:checklist/remove', array(
            'target' => $target,
            'task' => $task,
        )));
    }
}