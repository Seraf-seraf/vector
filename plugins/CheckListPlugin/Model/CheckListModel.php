<?php
namespace Kanboard\Plugin\CheckListPlugin\Model;

use PicoDb\Database;
use Kanboard\Model\SubtaskModel;
use Kanboard\Model\UserModel;
use Kanboard\Plugin\CheckListPlugin\Class\CheckList;

class CheckListModel extends SubtaskModel
{
    public function getCheckboxes($taskId)
    {
        $taskId = empty($taskId) ? 0 : $taskId;
        return $this->db->table('subtasks')
                ->eq('task_id', $taskId)
                ->eq('ischeckbox', '1')
                ->findAll();
    }

    public function checkCompletingChecklist($taskId)
    {
        $allChecklist = $this->getCheckboxes($taskId);

        if (count($allChecklist) == 0) {
            return true;
        }
        
        $completedCheckbox = $this->getQuery()
            ->eq('status', 2)
            ->eq('task_id', $taskId)
            ->eq('ischeckbox', 1)
            ->findAll();

        return count($allChecklist) == count($completedCheckbox);
    }

    public function toggleStatus($checkboxId)
    {
       $this->db->execute(
        'UPDATE subtasks '.
        'SET status = '.
        'CASE '.
            'WHEN status = 0 THEN 2 '.
            'WHEN status = 2 THEN 0 '.
        'END '.
        'WHERE id = ?',
        array($checkboxId)
       )->fetchAll();

       return $this->db->table('subtasks')->eq('id', $checkboxId)->findOneColumn('status');
    }

    public function updateCheckList(?array $checkList)
    {
        if(!empty($checkList)) {
            foreach($checkList as $target) {
               $this->db->execute(
                'UPDATE subtasks SET title = ? WHERE id = ?', array($target['title'],  $target['id'])
               )->fetchAll();
            }
        }
    }

    public function saveNewCheckList($taskId, ?CheckList $checkList)
    {
        $checkList = $checkList->getItems() ?? [];
        if(!empty($checkList)) {
            foreach($checkList as $target) {
                $this->db->table('subtasks')->save(
                    [
                        'task_id' => $taskId,
                        'title' => $target->getTitle(), 
                        'user_id' => $target->getUserId(), 
                        'ischeckbox' => $target->getRole()
                    ]
                );
            }
        }
    }
}