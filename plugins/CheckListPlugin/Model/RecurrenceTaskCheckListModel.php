<?php

namespace Kanboard\Plugin\CheckListPlugin\Model;

use Kanboard\Core\Base;
use Kanboard\Plugin\CheckListPlugin\Class\CheckList;

class RecurrenceTaskCheckListModel extends Base
{
    public function getCheckListItem($id)
    {
        return $this->db->table('recurrence_tasks_has_checklist')
            ->eq('id', $id)
            ->findOne();
    }

    public function update(array $checkList)
    {
        $this->db->transaction(function ($db) use ($checkList) {
            foreach ($checkList as $target) {
                $db->table('recurrence_tasks_has_checklist')
                    ->eq('id', $target['id'])
                    ->save(
                        [
                            'title' => $target['title'],
                        ]
                    );

                $this->updateSubtasks($target['id'], $target['title']);
            }
        });
    }

    protected function updateSubtasks($recurrence_id, $newTitle)
    {
        $this->db->table('subtasks')
            ->eq('recurrence_id', $recurrence_id)
            ->save(
                [
                    'title' => $newTitle,
                ]
            );
    }


    public function save($taskId, ?CheckList $checkList)
    {
        $checkList = $checkList->getItems() ?? [];
        if (!empty($checkList)) {
            $this->db->transaction(function ($db) use ($checkList, $taskId) {
                foreach ($checkList as $target) {
                    $db->table('recurrence_tasks_has_checklist')->save(
                        [
                            'recurrence_id' => $taskId,
                            'title' => $target->getTitle(),
                        ]
                    );
                }
            });
        }
    }

    public function firstCreateRecurrenceCheckList(int $recurrence_task_id, int $task_id, int $user_id): void
    {
        $checklist = $this->prepareToSave($recurrence_task_id, $task_id, $user_id);

        $this->db->transaction(function ($db) use ($checklist) {
            foreach ($checklist as $checklist_item) {
                $db->table('subtasks')->insert($checklist_item);
            }
        });
    }

    protected function prepareToSave(int $recurrence_task_id, int $task_id, int $user_id): array
    {
        $data = $this->getCheckList($recurrence_task_id);
        $checklist = array();
        foreach ($data as $value) {
            $checklist[] = [
                'task_id' => $task_id,
                'user_id' => $user_id,
                'title' => $value['title'],
                'recurrence_id' => $value['id'],
                'ischeckbox' => 1
            ];
        }

        return $checklist;
    }

    public function getCheckList($taskId)
    {
        return $this->db->table('recurrence_tasks_has_checklist')
            ->eq('recurrence_id', $taskId)
            ->findAll();
    }
}