<div class="dropdown">
    <a href="#" class="dropdown-menu dropdown-menu-link-icon"><div class="subtask-submenu"><i class="fa fa-cog"></i><i class="fa fa-caret-down"></i></div></a>
    <ul>
        <li>
            <?= $this->modal->confirm('trash-o', t('Remove'), 'RecurrenceCheckListController', 'confirm', array('plugin' => 'CheckListPlugin', 'task_id' => $task['task_id'], 'subtask_id' => $target['id'])) ?>
        </li>
    </ul>
</div>
