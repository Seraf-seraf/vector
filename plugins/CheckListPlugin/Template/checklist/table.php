<?php if (! empty($checklist)): ?>
    <table
        class="checklist table-striped table-scrolling"
        data-save-position-url="<?= $this->url->href('CheckListController', 'movePosition', array('task_id' => $task['id'])) ?>"
    >
    <tbody>
        <?php foreach ($checklist as $target): ?>
        <tr data-subtask-id="<?= $target['id'] ?>">
            <td>
              <div class="checklist-table-td">
                  <i class="fa" title="<?= t('Изменить статус чеклиста') ?>" role="button" aria-label="<?= t('Изменить статус чеклиста') ?>"></i>&nbsp;
                  <?= $this->render('CheckListPlugin:checklist/menu', array(
                      'task' => $task,
                      'target' => $target,
                  )) ?>
                <?php if ($editable): ?>
                    <?= $this->checkListHelper->renderToggleStatus($task, $target, 'table') ?>
                <?php else: ?>
                    <?= $this->checkListHelper->renderTitle($target) ?>
                <?php endif ?>
              </div>
            </td>
        </tr>
        <?php endforeach ?>
    </tbody>
    </table>
<?php endif ?>
