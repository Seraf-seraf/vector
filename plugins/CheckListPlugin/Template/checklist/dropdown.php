<details class="accordion-section" <?= empty($checklist) ? '' : 'open' ?>>
    <summary class="accordion-title"><?= t('Чек-лист') ?></summary>
    <div class="accordion-content">
        <?= $this->render('CheckListPlugin:checklist/table', array(
            'checklist' => $checklist,
            'task' => $task,
            'editable' => $editable
        )) ?>
    </div>
</details>
