<div class="page-header">
    <h2>Удаление элемента чек-листа</h2>
</div>

<div class="confirm">
    <div class="alert alert-info">
        Вы хотите удалить цель?
        <ul>
            <li>
                <strong><?= $this->text->e($target['title']) ?></strong>
            </li>
        </ul>
    </div>

    <?= $this->modal->confirmButtons(
        'SubtaskController',
        'remove',
        array('task_id' => $task['id'], 'subtask_id' => $target['id'])
    ) ?>
</div>