<ul class="add-checklist">
    <?php foreach ($checklist as $target): ?>
        <li class="checklist-item">
            <label for="checklist-text" class="visually-hidden">Цель</label>
            <input id="checklist-text" class="text-editor-write-mode fa fa-times" type="text" name='checklist-<?= $target['id'] ?>' value="<?= $target['title'] ?>" placeholder='Цель'>
            <?php if ($this->user->hasProjectAccess('SubtaskController', 'edit', $task['project_id'])): ?>
                <a class="checklist-item--delete js-modal-confirm"
                    href="<?= $this->url->href(
                            'CheckListController',
                            'confirm',
                            array(
                                'plugin' => 'CheckListPlugin',
                                'task_id' => $task['id'],
                                'subtask_id' => $target['id']
                            )
                    ); ?>"
                   aria-hidden="true">
                </a>
            <?php endif ?>
        </li>
    <?php endforeach; ?>
    <li class="new-checklist-item">
        <label for="new-checklist-text" class="visually-hidden">Цель</label>
        <input id="new-checklist-text" class="text-editor-write-mode" type="text" name='new-checklist-text[]' value="" placeholder='Цель'>
        <i class="checklist-item--delete" aria-hidden="true"></i>
    </li>
</ul>
<button class="button add-checklist-button fa fa-plus">Добавить чеклист</button>
