CheckListPlugin
==============================

create checklist for task

Author
------

- seraf-seraf
- License MIT

Requirements
------------

- Kanboard >= 1.0.35

Installation
------------

You have the choice between 3 methods:

1. Install the plugin from the Kanboard plugin manager in one click
2. Download the zip file and decompress everything under the directory `plugins/Kanboard\Plugin\CheckListPlugin`
3. Clone this repository into the folder `plugins/Kanboard\Plugin\CheckListPlugin`

Note: Plugin folder is case-sensitive.

Documentation
-------------

TODO.
