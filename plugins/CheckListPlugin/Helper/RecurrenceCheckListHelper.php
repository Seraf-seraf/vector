<?php

namespace Kanboard\Plugin\CheckListPlugin\Helper;

class RecurrenceCheckListHelper extends CheckListHelper
{
    public function renderToggleStatus(array $task, array $target, $userId = 0)
    {
        $title = $this->renderTitle($target);
        $params = array(
            'plugin' => 'CheckListPlugin',
            'task_id'    => $target['recurrence_id'],
            'subtask_id' => $target['id'],
        );

        if ($target['status'] == 0) {
            $icon = '<i class="fa fa-square-o fa-fw js-modal-confirm></i>';
        } else {
            $icon = '<i class="fa fa-check-square-o fa-fw"></i>';
        }

        $html = $this->helper->url->link($title, 'RecurrenceCheckListController', 'change', $params, false, 'subtask-title js-subtask-toggle-status', $this->getSubtaskTooltip($target));

        return '<span class="subtask-title">'.$html.'</span>';
    }
}