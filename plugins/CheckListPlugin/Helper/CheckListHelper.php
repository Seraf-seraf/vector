<?php
namespace Kanboard\Plugin\CheckListPlugin\Helper;

use Kanboard\Core\Base;
use Kanboard\Model\SubtaskModel;

class CheckListHelper extends Base
{
    public function hasSubtaskInProgress()
    {
        return session_is_true('hasSubtaskInProgress');
    }

    public function renderToggleStatus(array $task, array $target, $userId = 0)
    {
        if (! $this->helper->user->hasProjectAccess('SubtaskController', 'edit', $task['project_id'])) {
            $html = $this->renderTitle($target);
        } else {
            $title = $this->renderTitle($target);
            $params = array(
                'plugin' => 'CheckListPlugin',
                'task_id'    => $target['task_id'],
                'subtask_id' => $target['id'],
                'user_id'    => $userId,
                'csrf_token' => $this->token->getReusableCSRFToken(),
            );
            if ($target['status'] == 0) {
                $html = '<i class="fa fa-square-o fa-fw ' . ($this->hasSubtaskInProgress() ? 'js-modal-confirm' : '') . '"></i>';
            } else {
                $html = '<i class="fa fa-check-square-o fa-fw"></i>';
            }

            return $this->helper->url->link($title, 'CheckListController', 'change', $params, false, 'subtask-title js-subtask-toggle-status', $this->getSubtaskTooltip($target));
        }

        return '<span class="subtask-title">'.$html.'</span>';
    }

    public function renderTitle(array $target)
    {
        if ($target['status'] == 0) {
            $html = '<i class="fa fa-square-o fa-fw ' . ($this->hasSubtaskInProgress() ? 'js-modal-confirm' : '') . '"></i>';
        } else {
            $html = '<i class="fa fa-check-square-o fa-fw"></i>';
        }

        return $html.$this->helper->text->e($target['title']);
    }

    public function getSubtaskTooltip(array $target)
    {
        switch ($target['status']) {
            case SubtaskModel::STATUS_TODO:
                return t('Не выполнено');
            case SubtaskModel::STATUS_DONE:
                return t('Готово');
        }

        return '';
    }
}