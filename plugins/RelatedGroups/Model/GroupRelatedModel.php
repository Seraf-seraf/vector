<?php

namespace Kanboard\Plugin\RelatedGroups\Model;

use Kanboard\Core\Base;

/**
 * Group Model
 *
 * @package  Kanboard\Plugin\RelatedGroups\
 * @author   Michael Acidhead
 */
class GroupRelatedModel extends Base
{
    /**
     * SQL table name
     *
     * @var string
     */
    const TABLE = 'group_has_owners';
    const TABLE2 = 'group_has_owners_groups';

    /**
     * Get query to fetch all groups
     *
     * @access public
     * @return \PicoDb\Table
     */
    public function checkAdmin($group_id, $user_id = 0)
    {
        return $this->db->table(self::TABLE)
            ->eq('user_id', $user_id)
            ->eq('group_id', $group_id)
            ->exists();
    }
    public function checkOwnerGroup($group_id)
    {
        return $this->db->table(self::TABLE2)->eq('group_id', $group_id)->findOne();
    }
    public function changeAdmin($group_id, $user_id, $is_admin)
    {
        if ($is_admin) {
            return $this->db->table(self::TABLE)->insert(array(
                'group_id' => $group_id,
                'user_id' => $user_id,
            ));
        } else {
            return $this->db->table(self::TABLE)
                ->eq('user_id', $user_id)
                ->eq('group_id', $group_id)
                ->remove();
        }
    }
    public function addRelatedGroup($name, $owner_id)
    {
        $group_id = $this->search($name);
        return $this->db->table(self::TABLE2)->persist(array(
            'group_id' => $group_id['id'],
            'group_owner_id' => $owner_id,
        ));
    }
    public function updateRelatedGroup($id, $owner_id)
    {
        return $this->db->table(self::TABLE2)->eq('group_id', $id)->update(array('group_id' => $id, 'group_owner_id' => $owner_id));
    }

    /**
     * Get a specific group by id
     *
     * @access public
     * @param  integer $group_id
     * @return array
     */
    public function getById($group_id)
    {
        return $this->db->table('groups')->eq('id', $group_id)->findOne();
    }

    /**
     * Get a specific group by externalID
     *
     * @access public
     * @param  string $external_id
     * @return array
     */
    public function getByExternalId($external_id)
    {
        return $this->db->table(self::TABLE)->eq('external_id', $external_id)->findOne();
    }
    public function getExecType($id)
    {
        return $this->db->table('group_has_exec_type')
            ->columns('exec_type')
            ->eq('group_id', $id)
            ->findAll();
    }
    public function getExecTypes()
    {
        return $this->db->table('group_has_exec_type')
            ->columns('exec_type')
            ->groupBy('exec_type')
            ->findAll();
    }
    public function getAdminLevels()
    {
        return $this->db->table('groups')
            ->columns('admin_level')
            ->neq('admin_level', 'NULL')
            ->groupBy('admin_level')
            ->findAll();
    }
    public function getGroupsByAdminLevel(array $admin_lvls)
    {
        $query = $this->db->table('groups')
            ->columns('id')
            ->in('admin_level', $admin_lvls);
        return $query->findAll();

    }
    public function getGroupsByExecType(array $exec_types)
    {
        $query = $this->db->table('group_has_exec_type')
            ->columns('group_id')
            ->in('exec_type', $exec_types);
        return $query->findAll();

    }
    public function getAdminsByLevel($lvl)
    {
        return $this->db->table(self::TABLE)
            ->columns('users.id')
            ->eq('groups.admin_level', $lvl)
            ->join('users', 'id', 'user_id', self::TABLE)
            ->join('groups', 'id', 'group_id', self::TABLE)
            ->findAll();
    }
    public function getExecutorsByType($exec_type)
    {
        $query = $this->db->table('group_has_users')
            ->columns('users.id')
            ->eq('group_has_exec_type.exec_type', $exec_type)
            ->addCondition('NOT EXISTS (
            SELECT *
            FROM group_has_owners
            WHERE group_has_owners.group_id = group_has_users.group_id
              AND group_has_owners.user_id = group_has_users.user_id
        )')
            ->join('users', 'id', 'user_id', 'group_has_users')
            ->join('group_has_exec_type', 'group_id', 'group_id', 'group_has_users');
        $result = $query->findAll();
        return $result;
    }
    public function saveExecType($data)
    {
        $exec_type_before = $this->getExecType($data['group_id']);
        $before_type = array();
        foreach ($exec_type_before as $before) {
            $before_type[] = $before['exec_type'];
        }
        // // var_dump($before_type);
        $diff = array_diff($before_type, $data['exec_type']);
        foreach ($diff as $ex_el) {
            $this->db->table('group_has_exec_type')
                ->eq('group_id', $data['group_id'])
                ->remove(array(
                    'group_id' => $data['group_id'],
                    'exec_type' => $ex_el,
                ));
        }
        // $data['exec_type']=array_diff($data['exec_type'],$before_type);
        foreach ($data['exec_type'] as $exec_type) {
            $exist = false;
            foreach ($exec_type_before as $before_type) {
                if ($exec_type == $before_type) {
                    $exist = true;
                    break;
                }
            }
            if (!$exist) {
                $this->db->table('group_has_exec_type')
                    ->eq('group_id', $data['group_id'])
                    ->insert(array(
                        'group_id' => $data['group_id'],
                        'exec_type' => $exec_type,
                    ));
            }
        }

        return;
    }

    /**
     * Get specific groups by externalIDs
     *
     * @access public
     * @param  string[] $external_ids
     * @return array
     */
    public function getGroupsById(array $ids)
    {
        return $this->db->table('groups')->in('id', $ids)->findAll();
    }

    /**
     * Get all groups
     *
     * @access public
     * @return array
     */
    public function getAll()
    {
        return $this->getQuery()->asc('name')->findAll();
    }
    public function getUnrelatedGroups($owner_id)
    {
        $subquery = $this->db->table(self::TABLE2)
            ->columns('group_id')
            ->eq('group_owner_id', $owner_id)
            ->findAll();
        $subqueryColumn = array_column($subquery, 'group_id');

        return $this->db->table('groups')
            ->columns('id', 'name', 'external_id')
            ->notIn('id', $subqueryColumn)
            ->findAll();
    }
    public function getRelatedGroups()
    {
        return $this->db->table(self::TABLE2)
            ->columns('group_has_owners_groups.group_id', 'group_has_owners_groups.group_owner_id', 'groups.name', 'groups.result')
            ->join('groups', 'id', 'group_id', self::TABLE2)
            ->findAll();
    }
    public function getAdminsUnique()
    {
        return $this->db->table(self::TABLE)
            ->columns('groups.name', 'group_id', 'users.id')
            ->join('users', 'id', 'user_id', self::TABLE)
            ->join('groups', 'id', 'group_id', self::TABLE)
            ->groupBy('users.id','groups.name','group_id')
            ->findAll();
    }
    public function groupContainUser(array $groups,$user_id)
    {
        return $this->db->table('group_has_users')->columns('group_id')->in('group_id',$groups)->eq('user_id',$user_id)->findOne();
    }
    public function getAdmins()
    {
        return $this->db->table(self::TABLE)
            ->columns('groups.name', 'group_id', 'users.id')
            ->join('users', 'id', 'user_id', self::TABLE)
            ->join('groups', 'id', 'group_id', self::TABLE)
            ->findAll();
    }
    public function getExecutorsByGroup($group_ids)
    {
        $query = $this->db->table('group_has_users')
            ->columns('users.id')
        // ->eq('group_has_users.group_id',$group_id)
            ->in('group_has_users.group_id', $group_ids)
            ->addCondition('NOT EXISTS (
            SELECT *
            FROM group_has_owners
            WHERE group_has_owners.group_id = group_has_users.group_id
              AND group_has_owners.user_id = group_has_users.user_id
        )')
            ->join('users', 'id', 'user_id', 'group_has_users');
        // ->join('groups','group_id','group_id','group_has_users');

        $result = $query->groupBy('users.id')->findAll();
        return $result;
    }
    public function getAdminsByGroup($group_id)
    {
        return $this->db->table(self::TABLE)
            ->columns('users.id')
            ->eq('group_id', $group_id)
            ->join('users', 'id', 'user_id', self::TABLE)
            ->join('groups', 'id', 'group_id', self::TABLE)
            ->findAll();
    }
    public function getGroupsWithAdmin()
    {
        return $this->db->table(self::TABLE)
            ->columns('groups.name', 'group_id')
            ->join('groups', 'id', 'group_id', self::TABLE)
            ->groupBy('group_id', 'groups.name')
            ->findAll();
    }
    public function getExecutors()
    {
        $query = $this->db->table('group_has_users')
            ->columns('group_id', 'users.id')
            ->addCondition('NOT EXISTS (
            SELECT *
            FROM group_has_owners
            WHERE group_has_owners.group_id = group_has_users.group_id
              AND group_has_owners.user_id = group_has_users.user_id
        )')
            ->join('users', 'id', 'user_id', 'group_has_users');

        $result = $query->findAll();
        return $result;
    }
    public function getUsersInfo()
    {
        return $this->db->table('users')
            ->columns('id', 'name', 'username', 'email', 'avatar_path')
            ->eq('is_active', 1)
            ->findAll();
    }

    public function getQuery()
    {
        return $this->db->table('groups')
            ->columns('id', 'name', 'external_id')
            ->subquery('SELECT COUNT(*) FROM ' . GroupMemberModel::TABLE . ' WHERE group_id=' . 'groups' . '.id', 'nb_users');
    }
    /**
     * Search groups by name
     *
     * @access public
     * @param  string  $input
     * @return array
     */
    public function search($input)
    {
        return $this->db->table('groups')->ilike('name', '%' . $input . '%')->asc('name')->findOne();
    }

    /**
     * Remove a group
     *
     * @access public
     * @param  integer $group_id
     * @return boolean
     */
    public function remove($group_id)
    {
        return $this->db->table(self::TABLE)->eq('id', $group_id)->remove();
    }

    /**
     * Create a new group
     *
     * @access public
     * @param  string  $name
     * @param  string  $external_id
     * @return integer|boolean
     */
    public function create($name, $external_id = '')
    {
        return $this->db->table(self::TABLE)->persist(array(
            'name' => $name,
            'external_id' => $external_id,
        ));
    }

    /**
     * Update existing group
     *
     * @access public
     * @param  array $values
     * @return boolean
     */
    public function update(array $values)
    {
        $updates = $values;
        unset($updates['id']);
        return $this->db->table(self::TABLE)->eq('id', $values['id'])->update($updates);
    }

    /**
     * Get groupId from externalGroupId and create the group if not found
     *
     * @access public
     * @param  string $name
     * @param  string $external_id
     * @return bool|integer
     */
    public function getOrCreateExternalGroupId($name, $external_id)
    {
        $group_id = $this->db->table(self::TABLE)->eq('external_id', $external_id)->findOneColumn('id');

        if (empty($group_id)) {
            $group_id = $this->create($name, $external_id);
        }

        return $group_id;
    }
}
