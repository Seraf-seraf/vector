<?php

namespace Kanboard\Plugin\RelatedGroups\Model;

use Kanboard\Core\Base;
use Kanboard\Model\TaskModel;
use Kanboard\Model\UserModel;

class GroupHasRecurrenceTasksModel extends Base {

    public function getGroupAdminLevel($groupId) {
        return $this->db->table('groups')
            ->eq('id', $groupId)
            ->findOne();
    }

    public function getAdminLevelRecurrenceTasks($adminLevel) {
        return $this->db->table('recurrence_tasks_select')
            ->eq('type', 'admin_level')
            ->eq('value', $adminLevel)
            ->findAll();
    }

    public function getAdminFunctionRecurrenceTasks($groupId) {
        return $this->db->table('recurrence_tasks_select')
            ->eq('type', 'admin_function')
            ->eq('value', $groupId)
            ->findAll();
    }

    public function getAllAdminRecurrenceTasks() {
        return $this->db->table('recurrence_tasks_select')
            ->eq('type', 'admins')
            ->findAll();
    }

    public function getGroupExecType($groupId) {
        return $this->db->table('group_has_exec_type')
            ->eq('group_id', $groupId)
            ->findOne();
    }

    public function getExecTypeRecurrenceTasks($execType) {
        return $this->db->table('recurrence_tasks_select')
            ->eq('type', 'exec_type')
            ->eq('value', $execType)
            ->findAll();
    }

    public function getExecFunctionRecurrenceTasks($groupId) {
        return $this->db->table('recurrence_tasks_select')
            ->eq('type', 'exec_function')
            ->eq('value', $groupId)
            ->findAll();
    }

    public function getAllTasks() {
        return $this->db->table('recurrence_tasks_select')
            ->eq('type', 'all')
            ->findAll();
    }

    public function getRecurrenceTask($recurrenceTaskId) {
        return $this->db->table('recurrence_tasks')
            ->eq('id', $recurrenceTaskId)
            ->findOne();
    }

    public function getRecurrenceMeta($recurrenceTaskId) {
        return $this->db->table('recurrence_tasks_meta')
            ->eq('recurrence_id', $recurrenceTaskId)
            ->findOne();
    }

    public function getTask($recurrenceTaskId, $ownerId) {
        return $this->db->table('tasks')
            ->eq('owner_id', $ownerId)
            ->eq('recurrence_id', $recurrenceTaskId)
            ->findAll();
    }

    public function getUserRecurrenceTasks($userId) {
        return $this->db->table('tasks')
            ->eq('owner_id', $userId)
            ->findAll();
    }

    public function getRecurrenceTaskSelect($recurrenceTaskId) {
        return $this->db->table('recurrence_tasks_select')
            ->eq('recurrence_id', $recurrenceTaskId)
            ->findOne();
    }

    public function getOwnedGroups($userId) {
        return $this->db->table('group_has_owners')
            ->eq('user_id', $userId)
            ->findAll();
    }

    public function getGroupOwner($groupId) {
        return $this->db->table('group_has_owners')
            ->eq('group_id', $groupId)
            ->findOne();
    }

    public function getConsistedGroups($userId) {
        return $this->db->table('group_has_users')
            ->eq('user_id', $userId)
            ->findAll();
    }

    public function getRecurrenceTasks($groupId, $recurrenceTaskId) {
        return $this->db->table('group_has_recurrence_tasks')
            ->eq('group_id', $groupId)
            ->eq('recurrence_id', $recurrenceTaskId)
            ->findAll();
    }

    public function removeRecurrenceTask($ownerId, $recurrenceTaskId) {
        return $this->db->table('tasks')
            ->eq('owner_id', $ownerId)
            ->eq('recurrence_id', $recurrenceTaskId)
            ->remove();
    }

    public function getUserGroups($userId) {
        return $this->db->table('group_has_users')
            ->eq('user_id', $userId)
            ->findAll();
    }

    public function removeAllUserRecurrenceTasks($ownerId) {
        return $this->db->table('tasks')
            ->eq('owner_id', $ownerId)
            ->remove();
    }
}