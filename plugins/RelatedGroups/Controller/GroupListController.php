<?php

namespace Kanboard\Plugin\RelatedGroups\Controller;

use Kanboard\Plugin\RelatedGroups\Model\GroupModel;
use Kanboard\Plugin\RelatedGroups\Model\GroupMemberModel;
use Kanboard\Plugin\RelatedGroups\Model\GroupRelatedModel;
use Kanboard\Model\UserModel;
use Kanboard\Controller\BaseController;

/**
 * Group Controller
 *
 * @package Kanboard\Controller
 * @author  Michael
 */
class GroupListController extends BaseController
{
    /**
     * List all groups
     *
     * @access public
     */
    public function index()
    {
        $search = $this->request->getStringParam('search');
        $query = $this->groupModel->getQuery();

        if ($search !== '') {
            $query->ilike('groups.name', '%'.$search.'%');
        }

        $paginator = $this->paginator
            ->setUrl('GroupListController', 'index')
            ->setMax(30)
            ->setOrder(GroupModel::TABLE.'.name')
            ->setQuery($query)
            ->calculate();

        $this->response->html($this->helper->layout->app('group/index', array(
            'title' => t('Groups').' ('.$paginator->getTotal().')',
            'paginator' => $paginator,
            'values' => array(
                'search' => $search
            ),
        )));
    }

    /**
     * List all users
     *
     * @access public
     */
    public function users()
    {   
        $group_id = $this->request->getIntegerParam('group_id');
        $group = $this->groupModel->getById($group_id);
        $haveChild = $this->groupHasOwnersGroupsModel->getChilds($group_id);
        $isRoot = $this->groupHasOwnersGroupsModel->getRoot($group_id);
        $rootOrHaveChild = $haveChild || $isRoot;

        $paginator = $this->paginator
            ->setUrl('GroupListController', 'users', array('group_id' => $group_id, 'plugin' => 'RelatedGroups'))
            ->setMax(30)
            ->setOrder(UserModel::TABLE.'.username')
            ->setQuery($this->groupMemberModel->getQuery($group_id))
            ->calculate();

        $this->response->html($this->helper->layout->app('group/users', array(
            'title' => t('Members of %s', $group['name']).' ('.$paginator->getTotal().')',
            'paginator' => $paginator,
            'group' => $group,
            'is_parent' => $rootOrHaveChild,
            'plugin' => 'UserProfileEdit',
        )));
    }

    /**
     * Form to associate a user to a group
     *
     * @access public
     * @param array $values
     * @param array $errors
     */
    public function associate(array $values = array(), array $errors = array())
    {
        $group_id = $this->request->getIntegerParam('group_id');
        $group = $this->groupModel->getById($group_id);

        if (empty($values)) {
            $values['group_id'] = $group_id;
        }
        
        $haveChild = $this->groupHasOwnersGroupsModel->getChilds($values['group_id']);
        $isRoot = $this->groupHasOwnersGroupsModel->getRoot($values['group_id']);
        $rootOrHaveChild = $haveChild || $isRoot;

        $this->response->html($this->template->render('group/associate', array(
            'users' => $this->userModel->prepareList($this->groupMemberModel->getNotMembers($group_id)),
            'group' => $group,
            'errors' => $errors,
            'values' => $values,
            'is_parent' => $rootOrHaveChild,
        )));
    }

    /**
     * Add user to a group
     *
     * @access public
     */
    public function addUser()
    {
        $values = $this->request->getValues();
        echo "<script>console.log(".json_encode($values).")</script>";
        if (isset($values['group_id']) && isset($values['user_id'])) {
            if (!isset($values["is_admin"])) {
                $values["is_admin"] = false;
            }
            if ($this->groupMemberModel->addUser($values['group_id'], $values['user_id'],$values['is_admin'])) {
            
                $this->addRecurrenceTasksForNewUser($values['group_id'], $values['user_id'],$values['is_admin']);

                $this->flash->success(t('Group member added successfully.'));
                return $this->response->redirect($this->helper->url->to('GroupListController', 'users', array('group_id' => $values['group_id'], 'plugin' => 'RelatedGroups')), true);
            } else {
                $this->flash->failure(t('Unable to add group member.'));
            }
        }
        return $this->associate($values);
    }

    private function addRecurrenceTasksForNewUser($groupId, $userId, $isAdmin) {
        if ($isAdmin) {
            $adminLevel = $this->groupHasRecurrenceTasksModel->getGroupAdminLevel($groupId);
            $adminLevelTasks = $this->groupHasRecurrenceTasksModel->getAdminLevelRecurrenceTasks($adminLevel['admin_level']);
            
            $adminFunctionTasks = $this->groupHasRecurrenceTasksModel->getAdminFunctionRecurrenceTasks($groupId);

            $adminTasks = $this->groupHasRecurrenceTasksModel->getAllAdminRecurrenceTasks();

            $this->addRecurrenceTasksFromCollection($userId, $adminFunctionTasks);
            $this->addRecurrenceTasksFromCollection($userId, $adminLevelTasks);
            $this->addRecurrenceTasksFromCollection($userId, $adminTasks);
        } else {
            $execType = $this->groupHasRecurrenceTasksModel->getGroupExecType($groupId);
            $execTypeTasks = $this->groupHasRecurrenceTasksModel->getExecTypeRecurrenceTasks($execType['exec_type']);

            $execFunctionTasks = $this->groupHasRecurrenceTasksModel->getExecFunctionRecurrenceTasks($groupId);

            $this->addRecurrenceTasksFromCollection($userId, $execTypeTasks);
            $this->addRecurrenceTasksFromCollection($userId, $execFunctionTasks);
        }

        $allTasks = $this->groupHasRecurrenceTasksModel->getAllTasks();

        $this->addRecurrenceTasksFromCollection($userId, $allTasks);
    }

    private function addRecurrenceTasksFromCollection($userId, $collection) {
        foreach ($collection as $task) {
            $this->addRecurrenceTask($userId, $task);
        }
    }

    private function addRecurrenceTask($userId, $recurrenceTask) {
        $task = $this->groupHasRecurrenceTasksModel->getRecurrenceTask($recurrenceTask['recurrence_id']);
        $meta = $this->groupHasRecurrenceTasksModel->getRecurrenceMeta($recurrenceTask['recurrence_id']);
        
        //exclude duplicate
        $thisTask = $this->groupHasRecurrenceTasksModel->getTask($recurrenceTask['recurrence_id'], $userId);
        
        $isDuplicate = count($thisTask) > 0 ? true : false;
        if (!$isDuplicate) {
            $newTask = array();
            $newTask['title'] = $meta['title'];
            $newTask['description'] = $meta['description'];
            $newTask['color_id'] = 'red';
            $newTask['project_id'] = 1;
            $newTask['owner_id'] = $userId;
            $newTask['is_active'] = true;
            $newTask['date_due'] = $task['time_due'];
            $newTask['creator_id'] = $task['creator_id'];
            $newTask['swimlane_id'] = 1;
            $newTask['recurrence_status'] = 1;
            $newTask['recurrence_trigger'] = 4;
            $newTask['recurrence_factor'] = $task['recurrence_factor'];
            $newTask['controller_id'] = 1;
            $newTask['recurrence_id'] = $task['id'];
            $newTask['recurring_time'] = $task['recurring_time'];
            
            $this->taskCreationModel->create($newTask);
        }
    }

    private function removeRecurrenceTasks($userId, $groupId) {
        $userRecurrenceTasks = $this->groupHasRecurrenceTasksModel->getUserRecurrenceTasks($userId);

        foreach ($userRecurrenceTasks as $recurrenceTask) {
            $task = $this->groupHasRecurrenceTasksModel->getRecurrenceTaskSelect($recurrenceTask['recurrence_id']);

            if ($task != null) {
                switch ($task['type']) {
                    case 'all':
                        break;
                    case 'admin_level':
                        $mustBeDeleted = true;
                        $ownedGroups = $this->groupHasRecurrenceTasksModel->getOwnedGroups($userId);
                        foreach ($ownedGroups as $ownedGroup) {
                            $group = $this->groupHasRecurrenceTasksModel->getGroupAdminLevel($ownedGroup['group_id']);
                            if ($group['admin_level'] == $task['value']) $mustBeDeleted = false;
                        }
                        if ($mustBeDeleted) {
                            $this->groupHasRecurrenceTasksModel->removeRecurrenceTask($userId, $recurrenceTask['recurrence_id']);
                        }
                        break;
                    case 'admin_function':
                        $mustBeDeleted = true;
                        $ownedGroups = $this->groupHasRecurrenceTasksModel->getOwnedGroups($userId);
                        foreach ($ownedGroups as $ownedGroup) {
                            if (strval($ownedGroup['group_id']) == $task['value']) $mustBeDeleted = false;
                        }
                        if ($mustBeDeleted) {
                            $this->groupHasRecurrenceTasksModel->removeRecurrenceTask($userId, $recurrenceTask['recurrence_id']);
                        }
                        break;
                    case 'admins':
                        $ownedGroups = $this->groupHasRecurrenceTasksModel->getOwnedGroups($userId);
                        if (count($ownedGroups) < 1) $this->groupHasRecurrenceTasksModel->removeRecurrenceTask($userId, $recurrenceTask['recurrence_id']);
                        break;
                    case 'exec_type':
                        $mustBeDeleted = true;
                        $consistedGroups = $this->groupHasRecurrenceTasksModel->getConsistedGroups($userId);
                        foreach ($consistedGroups as $consistedGroup) {
                            $groupOwner = $this->groupHasRecurrenceTasksModel->getGroupOwner($consistedGroup['group_id']);
                            $isAdmin = $groupOwner['user_id'] == $userId ? true : false;
                            $group = $this->groupHasRecurrenceTasksModel->getGroupExecType($consistedGroup['group_id']);
                            if ($group['exec_type'] == $task['value'] && !$isAdmin) $mustBeDeleted = false;
                        }
                        if ($mustBeDeleted) {
                            $this->groupHasRecurrenceTasksModel->removeRecurrenceTask($userId, $recurrenceTask['recurrence_id']);
                        }
                        break;
                    case 'exec_function':
                        $mustBeDeleted = true;
                        $consistedGroups = $this->groupHasRecurrenceTasksModel->getConsistedGroups($userId);
                        foreach ($consistedGroups as $consistedGroup) {
                            $groupOwner = $this->groupHasRecurrenceTasksModel->getGroupOwner($consistedGroup['group_id']);
                            $isAdmin = $groupOwner['user_id'] == $userId ? true : false;
                            if (strval($consistedGroup['group_id']) == $task['value'] && !$isAdmin) $mustBeDeleted = false;
                        }
                        if ($mustBeDeleted) {
                            $this->groupHasRecurrenceTasksModel->removeRecurrenceTask($userId, $recurrenceTask['recurrence_id']);
                        }
                        break;
                }
            }
            
        }
    }

    /**
     * Confirmation dialog to remove a user from a group
     *
     * @access public
     */
    public function dissociate()
    {
        $group_id = $this->request->getIntegerParam('group_id');
        $user_id = $this->request->getIntegerParam('user_id');
        $group = $this->groupModel->getById($group_id);
        $user = $this->userModel->getById($user_id);

        $this->removeRecurrenceTasks($user_id, $group_id);
        $this->response->html($this->template->render('group/dissociate', array(
            'group' => $group,
            'user' => $user,
        )));
    }
    public function userSetup(){
        $group_id = $this->request->getIntegerParam('group_id');
        $user_id = $this->request->getIntegerParam('user_id');
        $group = $this->groupModel->getById($group_id);
        $user = $this->userModel->getById($user_id);
        $haveChild = $this->groupHasOwnersGroupsModel->getChilds($this->request->getIntegerParam('group_id'));
        $isRoot = $this->groupHasOwnersGroupsModel->getRoot($this->request->getIntegerParam('group_id'));
        $rootOrHaveChild = $haveChild || $isRoot;
        $is_admin=$this->groupRelatedModel->checkAdmin($group_id, $user_id);    
        $this->response->html($this->template->render('RelatedGroups:group/user_admin', array(
            'group' => $group,
            'user' => $user,
            'is_admin' => $is_admin,
            'is_parent' => $rootOrHaveChild,
        )));
    }
    public function changeAdmin(){
        $values['is_admin_before']=false;
        $values['is_admin']=false;
        $values = array_merge($values,$this->request->getValues());
        if (!isset($values['is_admin_before'])){
            $values['is_admin_before'] = false;
        }
        if (!isset($values['is_admin'])){
            $values['is_admin'] = false;
        }
        if (isset($values["is_admin_before"]) && isset($values["is_admin"]) && $values["is_admin_before"] == $values["is_admin"]) {
            return;
        }
        if (isset($values['group_id']) && isset($values['user_id'])) {
            if ($this->groupRelatedModel->changeAdmin($values['group_id'], $values['user_id'],$values['is_admin'])) {
                $this->removeRecurrenceTasks($values['user_id'], $values['group_id']);
                $this->addRecurrenceTasksForNewUser($values['group_id'], $values['user_id'],$values['is_admin']);
                $this->flash->success(t('Group member added successfully.'));
                return $this->response->redirect($this->helper->url->to('GroupListController', 'users', array('group_id' => $values['group_id'], 'plugin' => 'RelatedGroups')), true);
            } else {
                $this->flash->failure(t('Unable to add group member.'));
            }
        }
    }
        /**
     * Validate and save a new group
     *
     * @access public
     */
    public function save()
    {
        $values = $this->request->getValues();
        list($valid, $errors) = $this->groupValidator->validateCreation($values);
        $values['group_id'] = ($values['group_id'] == '') ? NULL : $values['group_id'];
        $exec_type=$values['exec_type'];
        if($exec_type[0]=='')
        unset($exec_type[0]);
        $exec_data['exec_type']=$exec_type;
        
        if ($valid) {
            $id=$this->groupModel->create($values['name'],$values['result'],$values['admin_level']);
            $this->projectGroupRoleModel->addGroup(1, $id, 'project-member');
            if ( $id !== false) {
                $exec_data['group_id']=$id;
                $this->groupRelatedModel->saveExecType($exec_data);
                $this->groupRelatedModel->addRelatedGroup($values['name'],$values['group_id']);
                $this->flash->success(t('Group created successfully.'));
                return $this->response->redirect($this->helper->url->to('GroupListController', 'index'), true);
            } else {
                $this->flash->failure(t('Unable to create your group.'));
            }
        }

        return $this->show($values, $errors);
    }
    public function show(array $values = array(), array $errors = array())
    {
        $exec_type_all_data=$this->groupRelatedModel->getExecTypes();
        $exec_type_all = array();
        foreach($exec_type_all_data as $el){
            $exec_type_all[]=$el['exec_type'];
        }
        $admin_levels_all_data=$this->groupRelatedModel->getAdminLevels();
        $admin_levels_all = array();
        foreach($admin_levels_all_data as $el){
            $admin_levels_all[]=$el['admin_level'];
        }
        $this->response->html($this->template->render('group_creation/show', array(
            'groups' => $this->groupModel->prepareList($this->groupModel->getAll()),
            'errors' => $errors,
            'values' => $values,
            'exec_type_all' =>$exec_type_all,
            'admin_levels_all' =>$admin_levels_all,
        )));
    }
    public function show_edit_group(array $values = array(), array $errors = array())
    {
        if (empty($values)) {
            $values = $this->groupModel->getById($this->request->getIntegerParam('group_id'));
        }
        $groups=$this->groupModel->prepareList($this->groupRelatedModel->getUnrelatedGroups($values['id']));
        $owner_group=$this->groupRelatedModel->checkOwnerGroup($values['id']);
        if ($owner_group && isset($owner_group['group_owner_id']) && $owner_group['group_owner_id'] === null) {
            $owner_group['group_owner_id'] = '';
        }        
        unset($groups[$values['id']]);
        $exec_type=$this->groupRelatedModel->getExecType($values['id']);
        $exec_type_data=array();
        foreach($exec_type as $type)
        {
            $exec_type_data[]=$type["exec_type"];
        }
        $exec_type_all_data=$this->groupRelatedModel->getExecTypes();
        foreach($exec_type_all_data as $el){
            $exec_type_all[]=$el['exec_type'];
        }
        $admin_levels_all_data=$this->groupRelatedModel->getAdminLevels();
        foreach($admin_levels_all_data as $el){
            $admin_levels_all[]=$el['admin_level'];
        }
        $this->response->html($this->template->render('group_modification/show', array(
            'errors' => $errors,
            'values' => $values,
            'groups' => $groups,
            'owner_group' => ($owner_group && isset($owner_group['group_owner_id'])) ? $owner_group['group_owner_id'] : '',
            'exec_type' => $exec_type_data,
            'exec_type_all' => $exec_type_all,
            'admin_levels_all'=>$admin_levels_all,
            
        )));
    }
    /**
     * Remove a user from a group
     *
     * @access public
     */
    public function removeUser()
    {
        $this->checkCSRFParam();
        $group_id = $this->request->getIntegerParam('group_id');
        $user_id = $this->request->getIntegerParam('user_id');

        if ($this->groupMemberModel->removeUser($group_id, $user_id)) {
            $this->removeRecurrenceTasks($user_id, $group_id);
            $this->flash->success(t('User removed successfully from this group.'));
        } else {
            $this->flash->failure(t('Unable to remove this user from the group.'));
        }

        $this->response->redirect($this->helper->url->to('GroupListController', 'users', array('group_id' => $group_id, 'plugin' => 'RelatedGroups')), true);
    }

    /**
     * Confirmation dialog to remove a group
     *
     * @access public
     */
    public function confirm()
    {
        $group_id = $this->request->getIntegerParam('group_id');
        $group = $this->groupModel->getById($group_id);

        $this->response->html($this->template->render('group/remove', array(
            'group' => $group,
        )));
    }
    public function save_modification(){
        $values = $this->request->getValues();
        $result=$values['result'];
        $owner_id=$values['group_id'];
        $exec_type=$values['exec_type'];
        if($exec_type[0]=='')
        unset($exec_type[0]);
        $exec_data['group_id']=$values['id'];
        $exec_data['exec_type']=$exec_type;
        // var_dump($exec_data);
        // return;
        if (!$values['admin_level']){
            $values['admin_level']=NULL;
        }
        if (!$owner_id)
        $owner_id=NULL;
        unset($values['group_id']);
        unset($values['exec_type']);
        // unset($values['admin_level']);
        // list($valid, $errors) = $this->groupValidator->validateModification($values);
        $this->groupRelatedModel->saveExecType($exec_data);
        // if ($valid) {
            if ($this->groupModel->update($values) !== false) {
                $this->groupRelatedModel->updateRelatedGroup($values['id'],$owner_id);
                $this->flash->success(t('Group updated successfully.'));
                return $this->response->redirect($this->helper->url->to('GroupListController', 'index'), true);
            } else {
                $this->flash->failure(t('Unable to update your group.'));
            }
        // }

        return $this->show_edit_group($values, $errors);
    }
    /**
     * Remove a group
     *
     * @access public
     */
    public function remove()
    {
        $this->checkCSRFParam();
        $group_id = $this->request->getIntegerParam('group_id');

        if ($this->groupModel->remove($group_id)) {
            $this->flash->success(t('Group removed successfully.'));
        } else {
            $this->flash->failure(t('Unable to remove this group.'));
        }

        $this->response->redirect($this->helper->url->to('GroupListController', 'index'), true);
    }
}
