Related groups
==============================

The plugin allows you to establish relationships between groups and build a hierarchy of subordination

Author
------

- Michael
- License MIT

Requirements
------------

- Kanboard >= 1.0.35

Installation
------------

You have the choice between 3 methods:

1. Install the plugin from the Kanboard plugin manager in one click
2. Download the zip file and decompress everything under the directory `plugins/RelatedGroups`
3. Clone this repository into the folder `plugins/RelatedGroups`

Note: Plugin folder is case-sensitive.

Documentation
-------------

TODO.
