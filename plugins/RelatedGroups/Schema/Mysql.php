<?php

namespace Kanboard\Plugin\RelatedGroups\Schema;

use PDO;

const VERSION = 4;
function version_4(PDO $pdo)
{
    $pdo->exec("ALTER TABLE `groups` ADD COLUMN `admin_level` TEXT");
}
function version_3(PDO $pdo)
{
    $pdo->exec("
    CREATE TABLE IF NOT EXISTS `group_has_exec_type` (
        group_id INT NOT NULL,
        exec_type TEXT(200) ,
        FOREIGN KEY(group_id) REFERENCES `groups`(id) ON DELETE CASCADE,
        UNIQUE(group_id, exec_type(200))
    ) ENGINE=InnoDB CHARSET=utf8
");
}
function version_2(PDO $pdo)
{
    $pdo->exec("
        CREATE TABLE IF NOT EXISTS `group_has_owners`(
            group_id INT NOT NULL AUTO_INCREMENT,
            user_id INT NOT NULL,
            FOREIGN KEY(group_id) REFERENCES `groups`(id) ON DELETE CASCADE,
            FOREIGN KEY(user_id) REFERENCES `users`(id) ON DELETE CASCADE
        ) ENGINE=InnoDB CHARSET=utf8
    ");

    $pdo->exec("
    CREATE TABLE IF NOT EXISTS `group_has_owners_groups` (
        group_id INT NOT NULL,
        group_owner_id INT,
        FOREIGN KEY(group_id) REFERENCES `groups`(id) ON DELETE CASCADE,
        FOREIGN KEY(group_owner_id) REFERENCES `groups`(id) ON DELETE CASCADE,
        UNIQUE(group_id, group_owner_id)
    ) ENGINE=InnoDB CHARSET=utf8
");
}
function version_1(PDO $pdo)
{
    $pdo->exec("ALTER TABLE `groups` ADD COLUMN `result` TEXT");
}
