<div class="page-header">
    <h2><?='Управление администраторами группы'?></h2>
</div>
<div class="confirm">
    <p class="alert alert-info"><?= sprintf('Изменить права пользователя "%s" для группы "%s"?', $user['name'] ?: $user['username'], $group['name']) ?></p>
    <form method="post" action="<?= $this->url->href('GroupListController', 'changeAdmin', array('plugin'=> 'RelatedGroups','group_id' => $group['id'],'user_id'=>$user['id'],'is_admin_before'=>$is_admin)) ?>" autocomplete="off">
        <?= $this->form->csrf() ?>
        <?= $this->form->hidden('group_id', array('group_id'=>$group['id'])) ?>
        <?= $this->form->hidden('user_id', array('user_id'=>$user['id'])) ?>
        <?= $this->form->hidden('is_admin_before', array('is_admin_before'=>$is_admin)) ?>
        <?php if ($is_parent): ?>
            <label for="is_admin"><?= t('Администратор группы') ?></label>
            <?php if ($is_admin): ?>
            <input type="checkbox" name="is_admin" id="is_admin" value="1" checked>
            <?php else: ?>
                <input type="checkbox" name="is_admin" id="is_admin" value="1">
            <?php endif; ?>
            <?php endif; ?>
    </div>
        <?= $this->modal->submitButtons() ?>
    </form>
</div>
