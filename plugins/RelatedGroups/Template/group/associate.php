<div class="page-header">
    <h2><?= t('Add group member to "%s"', $group['name']) ?></h2>
</div>
<?php if (empty($users)): ?>
    <p class="alert"><?= t('There is no user available.') ?></p>
    <div class="form-actions">
        <?= $this->url->link(t('Close this window'), 'GroupListController', 'index', array(), false, 'btn js-modal-close') ?>
    </div>
<?php else: ?>
    <form method="post" action="<?= $this->url->href('GroupListController', 'addUser', array('plugin'=> 'RelatedGroups','group_id' => $group['id'])) ?>" autocomplete="off">
        <?= $this->form->csrf() ?>
        <?= $this->form->hidden('group_id', $values) ?>

        <?php if ($is_parent): ?>
            <?= $this->form->label(t('Administrator'), 'user_id') ?>
        <?php else: ?>
            <?= $this->form->label(t('User'), 'user_id') ?>
        <?php endif; ?>
        <?= $this->app->component('select-dropdown-autocomplete', array(
            'name' => 'user_id',
            'items' => $users,
            'defaultValue' => isset($values['user_id']) ? $values['user_id'] : key($users),
        )) ?>
            <div class="form-group">
        <?php if (!$is_parent): ?> 
            <label for="is_admin"><?= t('Администратор группы?') ?></label>
            <input type="checkbox" name="is_admin" id="is_admin" value="1">
        <?php else: ?>
            <input type="checkbox" name="is_admin" id="is_admin" value="1" checked hidden>
        <?php endif; ?>
        
    </div>

        <?= $this->modal->submitButtons() ?>
    </form>
<?php endif ?>
