<div class="page-header">
    <h2><?= t('Edit group') ?></h2>
</div>
<form method="post" action="<?= $this->url->href('GroupListController', 'save_modification',array('plugin'=> 'RelatedGroups')) ?>" autocomplete="off">
    <?= $this->form->csrf() ?>
    <?= $this->form->hidden('id', $values) ?>
    <?= $this->form->hidden('external_id', $values) ?>
    <?= $this->form->label(t('Name'), 'name') ?>
    <?= $this->form->text('name', $values, $errors, array('autofocus', 'required', 'maxlength="191"')) ?>
    <label for="result"><?= t('Ожидаемый результат') ?></label>
    <input type="text" name="result" id="result" value='<?=$values['result'];?>'>
    <?= $this->form->label('Подчинена группе', 'owner_id') ?>
        <?= $this->app->component('select-dropdown-autocomplete', array(
            'name' => 'group_id',
            'items' => $groups,
            'defaultValue' => $owner_group,
        )) ?>
    <?= $this->form->label(t('Уровень администратора'), 'admin_level') ?>
    <input type="hidden" name="admin_level" value="">
    <select name="admin_level" id="admin_level" class="tag-autocomplete" tabindex="3">
        <?php                 
                $admin_level[]=$values['admin_level'];
                array_unshift($admin_levels_all,'');
                foreach ($admin_levels_all as $option) {
                    echo ($option);
                    $html = sprintf(
                        '<option value="%s" %s>%s</option>',
                        $this->helper->text->e($option),
                        in_array($option, $admin_level) ? 'selected="selected"' : '',
                        $this->helper->text->e($option)
                    );
                    echo ($html);
                }
        
        ?>
    </select>
     <!-- $this->form->text('admin_level', $values, $errors, array('autofocus', 'maxlength="191"')) -->
    <?= $this->form->label(t('Тип исполнителя'), 'exec_type') ?>
    <input type="hidden" name="exec_type[]" value="">
    <select name="exec_type[]" id="exec_type" class="tag-autocomplete" multiple tabindex="3">
        <?php 
                $tags=$exec_type;
                foreach ($exec_type_all as $option) {
                    echo ($option);
                    $html = sprintf(
                        '<option value="%s" %s>%s</option>',
                        $this->helper->text->e($option),
                        in_array($option, $tags) ? 'selected="selected"' : '',
                        $this->helper->text->e($option)
                    );
                    echo ($html);
                }
        
        ?>
    </select>
    <?= $this->modal->submitButtons() ?>
</form>
