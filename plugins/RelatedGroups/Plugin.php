<?php

namespace Kanboard\Plugin\RelatedGroups;
use Kanboard\Plugin\RelatedGroups\Model\GroupMemberModel;
use Kanboard\Plugin\RelatedGroups\Model\GroupModel;
use Kanboard\Plugin\RelatedGroups\Model\GroupRelatedModel;
use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Translator;
use Kanboard\Model\UserModel;


class Plugin extends Base
{
    public function initialize()
    {
        $this->template->setTemplateOverride('group/associate', 'RelatedGroups:group/associate');
        $this->template->setTemplateOverride('group/user_dropdown', 'RelatedGroups:group/user_dropdown');
        $this->template->setTemplateOverride('group_creation/show', 'RelatedGroups:group_creation/show');
        $this->template->setTemplateOverride('group/index', 'RelatedGroups:group/index');
        $this->template->setTemplateOverride('group/remove', 'RelatedGroups:group/remove');
        $this->template->setTemplateOverride('group_modification/show', 'RelatedGroups:group_modification/show');
        $this->template->setTemplateOverride('group/dropdown', 'RelatedGroups:group/dropdown');
    }
    public function onStartup()
    {
        Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getPluginName()
    {
        return 'Related groups';
    }

    public function getPluginDescription()
    {
        return t('The plugin allows you to establish relationships between groups and build a hierarchy of subordination');
    }

    public function getPluginAuthor()
    {
        return 'Michael';
    }

    public function getPluginVersion()
    {
        return '1.0.0';
    }

    public function getPluginHomepage()
    {
        return 'https://github.com/kanboard/plugin-myplugin';
    }
    public function getClasses()
    {
        return [
            'Plugin\RelatedGroups\Model' => [
                'GroupMemberModel','GroupModel','GroupRelatedModel','GroupHasRecurrenceTasksModel',
            ],
        ];
    }
}

