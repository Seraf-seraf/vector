<?php

namespace Kanboard\Plugin\VectorTasks\Action;

use Kanboard\Model\TaskModel;
use Kanboard\Action\Base;
use Kanboard\Plugin\VectorTasks\Model\VectorTaskRecurrenceModel;
use Kanboard\Plugin\VectorTasks\Model\RecurrenceTaskModel;
use Kanboard\Model\UserModel;
use Kanboard\Plugin\VectorTasks\Model\TaskCreationModel;

class CreateDuplicateTask extends Base
{
    /**
     * Get automatic action description
     *
     * @access public
     * @return string
     */
    public function getDescription()
    {
        return t('Создает новые задачи согласно графику создания задач');
    }
    /**
     * Get the list of compatible events
     *
     * @access public
     * @return array
     */
    public function getCompatibleEvents()
    {
        return array(
            TaskModel::EVENT_DAILY_CRONJOB,
        );
    }
    /**
     * Get the required parameter for the action (defined by the user)
     *
     * @access public
     * @return array
     */
    public function getActionRequiredParameters()
    {
        return array();
    }
    /**
     * Get the required parameter for the event
     *
     * @access public
     * @return string[]
     */
    public function getEventRequiredParameters()
    {
        return array('tasks');
    }
    /**
     * Check if the event data meet the action condition
     *
     * @access public
     * @param  array   $data   Event data dictionary
     * @return bool
     */
    public function hasRequiredCondition(array $data)
    {
        return true;
    }

    public function doAction(array $data)
    {
        $results = array();
        $this->firstCreate();
        foreach ($data['tasks'] as $task) {
            if ($task['recurrence_status'] && $task['recurring_time']){
                if($task['recurring_time'] <= time()){
                    $this->vectorTaskRecurrenceModel->duplicateRecurringTask($task['id']);
                }
            }
        }

        return in_array(true, $results, true);
    }
    private function firstCreate(){
        $tasks=$this->recurrenceTaskModel->get_recurrence_user_tasks();
        foreach($tasks as $task){
            if ($task['recurrence_id']){
                $params=$this->recurrenceTaskModel->get_recurring_time_params($task['recurrence_id']);
                if($params['recurring_time']<=time()) {
                    $task_data = array();
                    $meta=$this->recurrenceTaskModel->get_meta_data($task['recurrence_id'])[0];
                    $task_data['title'] = $meta['title'];
                    $task_data['description'] = $meta['description'];
                    $task_data['result'] = $meta['result'];
                    $task_data['recurrence_trigger'] = 4;
                    $task_data['recurrence_factor'] = $params['recurrence_factor'];
                    $task_data['recurrence_timeframe'] = 0;
                    $task_data['recurrence_basedate'] = 0;
                    $task_data['date_due']=$params['time_due'];
                    $task_data['recurring_time']=$params['recurring_time']+24*3600*$params['recurrence_factor'];
                    $task_data['project_id']=1;
                    $task_data['owner_id']=$task['user_id'];
                    $task_data['controller_id']=$task['creator_id'];
                    $task_data['recurrence_status']=1;
                    $task_data['recurrence_id']=$task['recurrence_id'];
                    $task_id = $this->taskCreationModel->create($task_data);

                    $this->recurrenceTaskCheckListModel->firstCreateRecurrenceCheckList($task['recurrence_id'], $task_id, $task['user_id']);

                    $this->recurrenceTaskModel->remove_users_recurrence_task($task['recurrence_id']);
                }
            }
        }
    }

    /**
     * Send email
     *
     * @access private
     * @param  integer $task_id
     * @param  array   $user
     * @return boolean
     */
    private function sendEmail($task_id, array $user)
    {
        $task = $this->taskFinderModel->getDetails($task_id);
        $this->emailClient->send(
            $user['email'],
            $user['name'] ?: $user['username'],
            $this->getParam('subject'),
            $this->template->render('notification/task_create', array('task' => $task))
        );
        return true;
    }
}
