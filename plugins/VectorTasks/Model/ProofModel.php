<?php

namespace Kanboard\Plugin\VectorTasks\Model;

use Kanboard\Core\Base;

/**
 * Task Creation
 *
 * @package  Kanboard\Model
 * @author   Frederic Guillot
 */
class ProofModel extends Base
{
    public function get_proof_params_by_rec_id($recurrence_id)
    {
        return $this->db->table('recurrence_task_has_proof')->eq('recurrence_id', $recurrence_id)->findOne();
    }
    public function get_proof_params_by_task_id($task_id)
    {
        return $this->db->table('recurrence_task_has_proof')->eq('task_id', $task_id)->findOne();
    }
    public function set_proof($values)
    {
        return $this->db->table('task_has_proof')->persist($values);
    }
    public function get_proofs($task_id)
    {
        return $this->db->table('task_has_proof')->eq('task_id', $task_id)->findAll();
    }
    public function proof_exist($task_id)
    {
        $exist = $this->db->table('recurrence_task_has_proof')->eq('task_id', $task_id)->exists();
        if (!$exist) {
            $r_id = $this->recurrenceTaskModel->get_recurrence_id_by_task_id($task_id);
            if ($r_id) {
                $exist = $this->db->table('recurrence_task_has_proof')->eq('recurrence_id', $r_id['recurrence_id'])->exists();
            }

        }
        return $exist;
    }
}
