<?php

namespace Kanboard\Plugin\VectorTasks\Model;

use Kanboard\Model\TaskFileModel;

class ProofFileModel extends TaskFileModel
{
    public function uploadProofFiles($id, array $files, $proof = 'false', $type = 'file')
    {
        try {
            if (empty($files)) {
                return false;
            }

            foreach (array_keys($files['error']) as $key) {
                $file = array(
                    'name' => $files['name'][$key],
                    'tmp_name' => $files['tmp_name'][$key],
                    'size' => $files['size'][$key],
                    'error' => $files['error'][$key],
                );

                $this->uploadProofFile($id, $file, $proof, $type);
            }

            return true;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
    public function getAllImages($id)
    {
        return $this->getQuery()->eq($this->getForeignKey(), $id)->eq('is_image', 1)->findAll();
    }
    public function getAllDocumentsProof($id)
    {
        return $this->getQuery()->eq('task_id', $id)->eq('proof', 'true')->findAll();
    }
    public function getAllDocuments($id)
    {
        return $this->getQuery()->eq('task_id', $id)->eq('is_image', 0)->findAll();
    }
    public function proof_count($task_id)
    {
        return $this->db->table('task_has_files')->eq('task_id', $task_id)->eq('proof', 'true')->count();
    }
    /**
     * Upload a file
     *
     * @access public
     * @param  integer $id
     * @param  array   $file
     * @throws Exception
     */
    public function uploadProofFile($id, array $file, $proof, $type)
    {
        if ($file['error'] == UPLOAD_ERR_OK && $file['size'] > 0) {
            $destination_filename = $this->generatePath($id[0], $file['name']);

            if ($this->isImage($file['name'])) {
                if ($type == 'file') {
                    return false;
                }

                $this->generateThumbnailFromFile($file['tmp_name'], $destination_filename);
            } elseif ($type == 'image') {
                return false;
            }

            $this->objectStorage->moveUploadedFile($file['tmp_name'], $destination_filename);
            $this->ProofCreate($id, $file['name'], $destination_filename, $file['size'], $proof);
        } else {
            throw new Exception('File not uploaded: ' . var_export($file['error'], true));
        }
    }
    public function duplicateFileTask($task_id, $new_task_id)
    {
        $files = $this->db->table('task_has_files')
            ->eq('task_id', $task_id)
            ->findAll();
        foreach ($files as &$file) {
            unset($file['id']);
            $file['task_id'] = $new_task_id;
            $file['is_image'] = $file['is_image'] ? 'true' : 'false';
            $this->db->table('task_has_files')->persist($file);
        }

    }
    public function ProofCreate($foreign_key_id, $name, $path, $size, $proof = 'false')
    {
        $values = array(
            $this->getForeignKey() => $foreign_key_id,
            'name' => substr($name, 0, 255),
            'path' => $path,
            'is_image' => $this->isImage($name) ? 1 : 0,
            'size' => $size,
            'user_id' => $this->userSession->getId() ?: 0,
            'date' => time(),
            'proof' => $proof,
        );

        $result = $this->db->table($this->getTable())->insert($values);

        if ($result) {
            $file_id = (int) $this->db->getLastId();
            $this->fireCreationEvent($file_id);
            return $file_id;
        }

        return false;
    }
}
