<?php

namespace Kanboard\Plugin\VectorTasks\Model;
use Kanboard\Model\TaskFileModel;
use Kanboard\Plugin\VectorTasks\Model\RecurrenceTaskModel;
class VectorTaskFileModel extends TaskFileModel{
    public function uploadFiles($id, array $files,$recurrence_id='')
    {
        try {
            if (empty($files)) {
                return false;
            }

            foreach (array_keys($files['error']) as $key) {
                $file = array(
                    'name' => $files['name'][$key],
                    'tmp_name' => $files['tmp_name'][$key],
                    'size' => $files['size'][$key],
                    'error' => $files['error'][$key],
                );

                $this->uploadFile($id, $file,$recurrence_id);
            }

            return true;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
    public function removeRecurrenceFile($file_id){
        $file=$this->getRecurrenceFile($file_id);
        try {
            // $this->fireDestructionEvent($file_id);
            $this->objectStorage->remove($file['path']);
                if ($file['is_image'] == 1) {
                    $this->objectStorage->remove($this->getThumbnailPath($file['path']));
                }
                return $this->db->table('recurrence_task_has_files')->eq('id',$file_id)->remove();
            }
            catch (ObjectStorageException $e) {
            $this->logger->error($e->getMessage());
            var_dump($e->getMessage());
            return false;
        }
    }
    
    public function getRecurrenceFile($file_id){
        return $this->db->table('recurrence_task_has_files')->eq('id',$file_id)->findOne();
    }
    public function getRecurrenceFiles($recurrence_id){
        return $this->db->table('recurrence_task_has_files')
        ->columns('recurrence_task_has_files'.'.*','users.username AS user_name')
        ->eq('recurrence_id',$recurrence_id)
        ->join('users','id','user_id')
        ->findAll();
    }
    public function uploadFilesRecurrenceTask(array $files,$recurrence_id='')
    {
        try {
            if (empty($files)) {
                return false;
            }
            $task_ids=array_column($this->recurrenceTaskModel->get_tasks_by_recurrence_id($recurrence_id),'id');
            foreach (array_keys($files['error']) as $key) {
                $file = array(
                    'name' => $files['name'][$key],
                    'tmp_name' => $files['tmp_name'][$key],
                    'size' => $files['size'][$key],
                    'error' => $files['error'][$key],
                );

                $this->uploadFileRecurrenceTask($file,$recurrence_id,$task_ids);
            }

        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            return false;
        }
        // $this->afterRecurrenceFilesUpdate($recurrence_id);
    }
    public function afterRecurrenceFilesUpdate($recurrence_id){
        $task_ids=array_column(get_tasks_by_recurrence_id($recurrence_id),'id');
        $files=$this->getRecurrenceFiles($recurrence_id);
        unset($files['user_name']);
        $files['recurrence_file_id']=$files['id'];
        unset($files['id']);
        foreach($task_ids as $id){
        foreach ($files as $file){
        $file['task_id']=$id;
        $this->db->table('task_has_files')
        ->persist($file);
        }
        }
    }
    public function uploadFileRecurrenceTask(array $file,$recurrence_id='',array $task_ids=array())
    {
        if ($file['error'] == UPLOAD_ERR_OK && $file['size'] > 0) {
            $destination_filename = $this->generatePath($recurrence_id, $file['name']);

            if ($this->isImage($file['name'])) {
                $this->generateThumbnailFromFile($file['tmp_name'], $destination_filename);
            }
            $this->objectStorage->moveUploadedFile($file['tmp_name'], $destination_filename);
            $this->createRecurrenceTaskFile($recurrence_id, $file['name'], $destination_filename, $file['size'],$task_ids);
        } else {
            throw new Exception('File not uploaded: '.var_export($file['error'], true));
        }
    }

    /**
     * Upload a file
     *
     * @access public
     * @param  integer $id
     * @param  array   $file
     * @throws Exception
     */
    public function uploadFile($id, array $file,$recurrence_id='')
    {
        if ($file['error'] == UPLOAD_ERR_OK && $file['size'] > 0) {
            $destination_filename = $this->generatePath($id[0], $file['name']);

            if ($this->isImage($file['name'])) {
                $this->generateThumbnailFromFile($file['tmp_name'], $destination_filename);
            }
            $this->objectStorage->moveUploadedFile($file['tmp_name'], $destination_filename);
            foreach($id as $el){
            $this->create($el, $file['name'], $destination_filename, $file['size'],$recurrence_id);
            }
        } else {
            throw new Exception('File not uploaded: '.var_export($file['error'], true));
        }
    }
    public function createRecurrenceTaskFile($recurrence_id, $name, $path, $size,array $task_ids=array())
    {
        $values = array(
            'name' => substr($name, 0, 255),
            'path' => $path,
            'is_image' => $this->isImage($name) ? 1 : 0,
            'size' => $size,
            'user_id' => $this->userSession->getId() ?: 0,
            'date' => time(),
            'recurrence_id'=>$recurrence_id,
        );

        $result = $this->db->table('recurrence_task_has_files')->insert($values);

        if ($result) {
            $file_id = (int) $this->db->getLastId();
            $this->fireCreationEvent($file_id);
            unset($values['recurrence_id']);
            $values['recurrence_file_id']=$file_id;
            foreach($task_ids as $id){
            $values['task_id']=$id;
                $this->db->table('task_has_files')
                ->persist($values);
            }

            return $file_id;
        }

        return false;
    }
    public function duplicateFileTask($task_id,$new_task_id){
        $files=$this->db->table('task_has_files')
        ->eq('task_id',$task_id)
        ->findAll();
        foreach($files as &$file){
            unset($file['id']);
            $file['task_id']=$new_task_id;
            $file['is_image']= $file['is_image'] ? 'true' : 'false';
            $this->db->table('task_has_files')->persist($file);
        }

    }
    public function create($foreign_key_id, $name, $path, $size,$recurrence_id='')
    {
        $values = array(
            $this->getForeignKey() => $foreign_key_id,
            'name' => substr($name, 0, 255),
            'path' => $path,
            'is_image' => $this->isImage($name) ? 1 : 0,
            'size' => $size,
            'user_id' => $this->userSession->getId() ?: 0,
            'date' => time(),
            'recurrence_id' =>$recurrence_id,
        );

        $result = $this->db->table($this->getTable())->insert($values);

        if ($result) {
            $file_id = (int) $this->db->getLastId();
            $this->fireCreationEvent($file_id);
            return $file_id;
        }

        return false;
    }
}