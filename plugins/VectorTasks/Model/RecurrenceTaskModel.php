<?php

namespace Kanboard\Plugin\VectorTasks\Model;
use Kanboard\Core\Base;
use Kanboard\Model\GroupMemberModel;
use Kanboard\Plugin\RelatedGroups\Model\GroupRelatedModel;

/**
 * Task Creation
 *
 * @package  Kanboard\Model
 * @author   Frederic Guillot
 */
class RecurrenceTaskModel extends Base
{
    const TABLE = 'recurrence_tasks';
    public function get_tasks_by_recurrence_id($recurrence_id){
        return $this->db->table('tasks')->columns('id')->eq('recurrence_id',$recurrence_id)->findAll();
    }
    public function create(array $values=array()){
        if($this->userSession->isLogged()){
        $values['creator_id'] = $this->userSession->getId();
        $values['date_creation']=time();
        return $this->db->table(self::TABLE)->persist($values);
        }
    }
    public function set_recurrence_user_task($user_id,$recurrence_id,$creator_id){
        return $this->db->table('user_has_recurrence_tasks')->persist(array('user_id'=>$user_id,'recurrence_id'=>$recurrence_id,'creator_id'=>$creator_id));
    }
    public function get_recurrence_user_tasks(){
        return $this->db->table('user_has_recurrence_tasks')->findAll();
    }
    public function update_recurrence_task(array $values){
        return $this->db->table(self::TABLE)->eq('id',$values['id'])->update($values);
    }
    public function add_meta_info(array $values){
        return $this->db->table('recurrence_tasks_meta')->persist($values);
    }
    public function update_meta_info(array $values){
        return $this->db->table('recurrence_tasks_meta')->eq('recurrence_id',$values['recurrence_id'])->update($values);
    }
    public function remove($recurrence_id){
        return $this->db->table(self::TABLE)->eq('id', $recurrence_id)->remove();
    }
    public function get_params($recurrence_id){
        return $this->db->table(self::TABLE)->eq('id', $recurrence_id)
        ->columns('recurrence_factor','recurring_time','time_due')
        ->findOne();

    }
    public function add_select_options(array $values){
        // var_dump($values);
      
        if($values['add']){
            foreach($values['add'] as $key=>$el){              
                foreach($el[0] as $param){
                    $this->db->table('recurrence_tasks_select')->persist(array('recurrence_id'=>$values['recurrence_id'],'type'=>$key,'value'=>$param,'ignore'=>0));
                }
            }
        }
        if($values['ignore']){
            foreach($values['ignore'] as $key=>$el){
                foreach($el[0] as $param){
                    $this->db->table('recurrence_tasks_select')->persist(array('recurrence_id'=>$values['recurrence_id'],'type'=>$key,'value'=>$param,'ignore'=>1));
                }
            }
           
        }
    }
    public function update_select_options(array $values){
        $this->db->table('recurrence_tasks_select')
        ->eq('recurrence_id',$values['recurrence_id'])
        ->remove();
        $this->add_select_options($values);
    }
    public function get_recurrence_tasks_by_group_id($group_id){
         return $this->db->table('group_has_recurrence_tasks')
        ->columns('recurrence_id')
        ->eq('group_id', $group_id)
        ->findAll();
    }
    public function get_meta_header($recurrence_id){
        return array_column($this->db->table('recurrence_tasks_meta')
        ->columns('title')
        ->eq('recurrence_id',$recurrence_id)
        ->findAll(),'title');
    }
    public function get_meta_data($recurrence_id){
        return $this->db->table('recurrence_tasks_meta')
        ->eq('recurrence_id',$recurrence_id)
        ->findAll();
    }
    public function get_recurrence_id_by_task_id($task_id){
         $r_id=$this->db->table('tasks')->columns('recurrence_id')->eq('id',$task_id)->findOne();
         if ($r_id['recurrence_id'])
         return $r_id;
         else
         return false;
    }
    public function get_recurrence_select($recurrence_id){
        $add = $this->db->table('recurrence_tasks_select')
        ->columns('type','value')
        ->eq('ignore', 0)
        ->eq('recurrence_id',$recurrence_id)
        ->findAll();
        $ignore = $this->db->table('recurrence_tasks_select')
        ->columns('type','value')
        ->eq('ignore', 1)
        ->eq('recurrence_id',$recurrence_id)
        ->findAll();
        return(array('add'=>$add,'ignore'=>$ignore));
    }
    public function get_recurring_time_params($recurrence_id){
        return $this->db->table('recurrence_tasks')->columns('recurrence_factor','time_due','recurring_time')->eq('id',$recurrence_id)->findOne();
    }
    public function remove_users_recurrence_task($recurrence_id){
        return $this->db->table('user_has_recurrence_tasks')->eq('recurrence_id',$recurrence_id)->remove();
    }
    public function set_group_recurrence_task($values){
        $values['ignore_admin'] = $values['ignore_admin'] ? 'true' : 'false';
        $values['ignore_exec'] = $values['ignore_exec'] ? 'true' : 'false';

        return $this->db->table('group_has_recurrence_tasks')->persist(array('group_id'=>$values['group_id'],'recurrence_id'=>$values['recurrence_id'],'ignore_admin'=>$values['ignore_admin'],'ignore_exec'=>$values['ignore_exec']));
    }
    public function getRecurrenceController($group_id){
        while($group_id){
        $controllers=$this->groupRelatedModel->getAdminsByGroup($group_id);
        if (isset($controllers[0]['id'])){
            break;
        }
        $group_id=$this->db->table('group_has_owners_groups')->columns('group_owner_id')->eq('group_id',$group_id)->findOne()['group_owner_id'];
        } 
        return $controllers;

    }
    public function set_recurrence_task_proof($values){
        return $this->db->table('recurrence_task_has_proof')->persist($values);
    }
    public function update_recurrence_task_proof($values){
        return $this->db->table('recurrence_task_has_proof')->eq('id',$values['id'])->update($values);
    }
    public function remove_recurrence_task_proof($id){
        return $this->db->table('recurrence_task_has_proof')->eq('id',$id)->remove();
    }
    public function update_group_recurrence_task($values){
        $this->db->table('group_has_recurrence_tasks')
        ->notIn('group_id',$values['groups']['ids'])
        ->eq('recurrence_id',$values['recurrence_id'])
        ->remove();
        $this->db->table('group_has_recurrence_tasks')
        ->in('group_id',$values['groups']['ids'])
        ->eq('recurrence_id',$values['recurrence_id'])
        ->remove();
        foreach($values['groups']['ids'] as $id){
            $this->db->table('group_has_recurrence_tasks')
            ->persist(array('group_id'=>$id,'recurrence_id'=>$values['recurrence_id']));
        }
        return;
    }
    private function create_select_options(array $values){
        $this->db->table('recurrence_tasks_select')->persist($values);
        return;
    }
}