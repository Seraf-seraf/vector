<?php

namespace Kanboard\Plugin\VectorTasks\Model;

use Kanboard\Model\SubtaskModel;
use PicoDb\Database;

class VectorSubtaskModel extends SubtaskModel
{
    /**
     * Duplicate all subtasks to another task
     *
     * @access public
     * @param  integer $srcTaskId
     * @param  integer $dstTaskId
     * @return bool
     */
    public function duplicate($srcTaskId, $dstTaskId)
    {

        return $this->db->transaction(function (Database $db) use ($srcTaskId, $dstTaskId) {
            $subtasks = $db->table(SubtaskModel::TABLE)
                ->columns('title', 'time_estimated', 'position','user_id', 'ischeckbox')
                ->eq('task_id', $srcTaskId)
                ->asc('position')
                ->findAll();

            foreach ($subtasks as &$subtask) {
                $subtask['task_id'] = $dstTaskId;
                $subtask['status'] = 0;

                if (! $db->table(SubtaskModel::TABLE)->save($subtask)) {
                    return false;
                }
            }
        });
    }
}