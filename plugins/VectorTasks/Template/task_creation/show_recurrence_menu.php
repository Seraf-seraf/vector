<?php 
?>
<div class="page-header">
    <h2><?= t('Добавить повторяющуюся задачу') ?></h2>
</div>
    <?=$this->modal->medium('refresh fa-rotate-90', 'Повторяющиеся задачи', 'VectorTasksController', 'showRecurrenceTask', array('plugin'=> 'VectorTasks','group_id' => $group_id));?>
    <div>
        <?php foreach($values as $key=>$type){
            ?>
            <div class='recurrence_task'>
            <h5><?=$type['title'][0]?></h5>
            <?=$this->modal->medium('edit', '', 'VectorTasksController', 'editRecurrenceTask', array('plugin'=> 'VectorTasks','recurrence_id' => $key,'select_start'=>$type));?>
            <?= $this->modal->confirm('trash-o', t('Remove'), 'VectorTasksController', 'removeRecurrenceTaskMenu', array('plugin'=> 'VectorTasks','recurrence_id' => $key,'title'=>$type['title'][0])) ?>
            <div class='who-select'>
            <span class='select_target'>Кому</span>
            <?php
            foreach($type['add'] as $sub_type=>$el){
            foreach($el as $end_el){
            ?>
            <div class='target'>
                <?=$this->helper->convertHelper->convert_select_type($sub_type);?>
                <?=$this->helper->convertHelper->convert_select_value($sub_type,$end_el);?>
            </div>       
    <?php
    }
}
    ?>
                </div>
                <div class='who-select'>
                <?php if(isset($type['ignore'])):?>
                <span class='select_target'>Кроме</span>
                <?php endif;?>
            <?php
            if(isset($type['ignore']))
            foreach($type['ignore'] as $sub_type=>$el){
            foreach($el as $end_el){
            ?>

            <div class='target'>

            <?=$this->helper->convertHelper->convert_select_type($sub_type);?>
                <?=$this->helper->convertHelper->convert_select_value($sub_type,$end_el);?>
            </div>       
    <?php
            }
    }
    ?>
                </div>
                </div>
    <?php
}
        ?>
    </>