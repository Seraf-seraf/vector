<div class="page-header">
    <h2><?= t('Добавить повторяющуюся задачу') ?></h2>
</div>
    <form method="post" id='TaskForm' enctype="multipart/form-data" action="<?= $this->url->href('VectorTasksController', 'saveRecurrenceTask', array('plugin'=>'VectorTasks','recurrence_id' => '')) ?>" autocomplete="off">
    <input type="hidden" name="SelectList[]" id="SelectList">
    <input type="hidden" name="IgnoreList[]" id="IgnoreList">
    <style>
        /* Стили для основного контейнера dropdown */
        .dropdown {
            position: relative;
            display: inline-block;
        }

        /* Стили для кнопки, отображаемой в dropdown */
        .dropdown-button,.dropdown-button-two {
            padding: 10px;
            text-decoration:none;
            color: black;
            font-size:15px;
            font-weight:700;
            border: none;
            cursor: pointer;
            border: black;
            border-width: 2px;
            border-style: solid;
            display: block;
        }
        .dropdown-button,.dropdown-button-two :hover{
            color:black;
        }
        .dropdown-button-two{
            margin-left:10px;
            display:none;
        }

        /* Стили для контейнера, который содержит элементы в выпадающем списке */
        .dropdown-content,.dropdown-content-two {
            display: none;
            position: absolute;
            background-color: #f9f9f9;
            box-shadow: 0 8px 16px rgba(0, 0, 0, 0.2);
            min-width: 160px;
            z-index: 1;
            overflow: scroll;
            max-height: 200px;
        }
        

        /* Стили для элементов в dropdown */
        .dropdown-item {
            padding: 12px;
            text-decoration: none;
            display: block;
            color: #333;
        }

        /* При наведении на элемент списка изменить цвет фона */
        .dropdown-item:hover {
            background-color: #ddd;
        }
        .show{
            display: block;
        }
        .target{
            border-width:2px;
            border-color:black;
            border-style:solid;
            padding:5px;
            margin-right:7px;
            border-radius:15px;
            margin-bottom:7px;
            cursor:pointer;
        }
        .who-select,.who-ignore{
         margin-top:15px;
         display: flex;
         flex-flow: wrap;
        }
        .file-button{
            padding:10px;
            color:white;
            background:#55b6e7;
            text-decoration:none;
            display: inline-block;
        }
        .file-field{
            display: flex;
            align-items:center;
        }
        .close_button{
            cursor: pointer;
            position: absolute;
            top:0;
            right: 0;
            color:red;
            margin-right:5px;
        }
        #thumbnails{
            display: flex;
        }
        #thumbnails > div{
            width: 85px;
    margin-left: 10px;
    background: #0df2c9 ;
    padding: 5px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    position: relative;
        }
        #thumbnails > div > .file_name{
            overflow-x:clip;
            white-space: nowrap;
            width: 100%;
            text-overflow:ellipsis;
        }
        #thumbnails > div > img{
        width: 75px;
        height: 75px;
        width: 100%;
        object-fit:contain;
        }
    </style>
        <?= $this->form->csrf() ?>
        <?= $this->form->hidden('id', $values) ?>
        <?= $this->form->hidden('project_id', $values) ?>
        <div class="dropdown">
    <span>Кому назначить задачу</span>
    <br><br>
    <!-- Кнопка, которая отображается в dropdown -->
    <div class="select-nav" style='display:flex;'>
    <div class="dropdown-one">
    <a href="#" class="dropdown-button">Кому</a> 
    <div class="dropdown-content">
        <!-- Элементы в dropdown -->
        <a href="#" class="dropdown-item" data-value="all">Всем</a>
        <a href="#" class="dropdown-item" data-value="admins">Всем администраторам</a>
        <a href="#" class="dropdown-item" data-value="admin_level">Администраторам уровня</a>
        <a href="#" class="dropdown-item" data-value="exec_type">Исполнителям типа</a>
        <a href="#" class="dropdown-item" data-value="admin_function">Администратору функции</a>
        <a href="#" class="dropdown-item" data-value="exec_function">Исполнителям функции</a>
    </div>
    </div>
    <div class="dropdown-two">
    <a href="#" class="dropdown-button-two"></a>
    <div class="dropdown-content-two">
        <!-- Элементы в dropdown -->
        <!-- <a href="#" class="dropdown-item" data-value="all">Всем</a>
        <a href="#" class="dropdown-item" data-value="admins">Всем администраторам</a>
        <a href="#" class="dropdown-item" data-value="admin_level">Администраторам уровня</a>
        <a href="#" class="dropdown-item" data-value="exec_type">Исполнителям типа</a>
        <a href="#" class="dropdown-item" data-value="admin_function">Администратору функции</a>
        <a href="#" class="dropdown-item" data-value="exec_function">Исполнителям функции</a> -->
    </div>
    </div>
    </div>
    <div class='who-select'></div>
    <span>Кому не назначать задачу</span>
    <br><br>
    <div class="select-ignore" style='display:flex;'>
    <div class="dropdown-one">
    <a href="#" class="dropdown-button">Кроме</a> 
    <div class="dropdown-content">
        <!-- Элементы в dropdown -->
        <a href="#" class="dropdown-item" data-value="admins">Всем администраторам</a>
        <a href="#" class="dropdown-item" data-value="admin_level">Администраторам уровня</a>
        <a href="#" class="dropdown-item" data-value="exec_type">Исполнителям типа</a>
        <a href="#" class="dropdown-item" data-value="admin_function">Администратору функции</a>
        <a href="#" class="dropdown-item" data-value="exec_function">Исполнителям функции</a>
    </div>
    </div>
    <div class="dropdown-two">
    <a href="#" class="dropdown-button-two"></a>
    <div class="dropdown-content-two">
        <!-- Элементы в dropdown -->
        <!-- <a href="#" class="dropdown-item" data-value="all">Всем</a>
        <a href="#" class="dropdown-item" data-value="admins">Всем администраторам</a>
        <a href="#" class="dropdown-item" data-value="admin_level">Администраторам уровня</a>
        <a href="#" class="dropdown-item" data-value="exec_type">Исполнителям типа</a>
        <a href="#" class="dropdown-item" data-value="admin_function">Администратору функции</a>
        <a href="#" class="dropdown-item" data-value="exec_function">Исполнителям функции</a> -->
    </div>
    </div>
    </div>
    <div class='who-ignore'></div>
    </div>

    <!-- Контейнер с элементами в dropdown -->
    <div name='SelectData' data-array="<?=htmlspecialchars(json_encode($selectData)) ?>"></div>
    </div>
        <?= $this->form->label(t('Название задачи'), 'recurrence_title') ?>
        <input type="text" name="title"></input>
        <?= $this->form->label(t('Описание задачи'), 'recurrence_description') ?>
        <input type="text" name="description"></input>
        <?= $this->form->label(t('Ожидаемый результат'), 'recurrence_name') ?>
        <textarea type="text" name="result"></textarea>
        <br><br>
        <?= $this->app->component('file-upload', array(
    'csrf'              => $this->app->getToken()->getReusableCSRFToken(),
    'maxSize'           => $max_size=500000000,
    // 'url'               => $this->url->to('VectorTasksController', 'saveRecurrenceTask', array('plugin' => 'VectorTasks')),
    'labelDropzone'     => t('Drag and drop your files here'),
    'labelOr'           => t('or'),
    'labelChooseFiles'  => t('choose files'),
    'labelOversize'     => $max_size > 0 ? t('The maximum allowed file size is %sB.', $this->text->bytes($max_size)) : null,
    'labelSuccess'      => t('All files have been uploaded successfully.'),
    'labelCloseSuccess' => t('Close this window'),
    'labelUploadError'  => t('Unable to upload this file.'),
)) ?>
        <div class='file-field'>
        <a href='#' class="file-button">Добавить файл</a>
        <div id='thumbnails'></div>
    </div>
        <?= $this->form->label(t('Периодичность создания задач'), 'recurrence_factor') ?>
        <?= $this->form->number('recurrence_factor', $values, $errors) ?>
        <?= $this->form->select('recurrence_timeframe', $recurrence_timeframe_list, $values, $errors) ?>
        <?= $this->form->label(t('Время на выполнение после создания задачи'), 'recurrence_factor') ?>
        <input type="time" name="time_due" required>
        <?= $this->form->label(t('Время создания повторяющейся задачи'), 'recurrence_factor') ?>
        <input type="time" name="recurrence_date" required>
        <?= $this->form->label(t('Тип доказательства'), 'recurrence_factor') ?>
        <select name="proof-select">
            <option value="not" selected>Не требуется</option>
            <option value="text">Текст</option>
            <option value="photo">Фото с камеры телефона</option>
            <option value="file">Файл</option>
            <option value="link">Ссылка</option>
            <option value="image">Изображение</option>
        </select>
        <?= $this->form->label(t('Описание доказательства'), 'recurrence_factor') ?>
        <textarea type="text" name="proof_desc"></textarea>
        <?= $this->form->label(t('Количество'), 'recurrence_factor') ?>
        <input type="text" name="proof_count" min="1"></input>
        <?= $this->form->label(t('Контроллирующий задачу'), 'recurrence_factor') ?>
    <div style="display:flex;flex-direction:column;">
    <div>
    <input type="radio" name="controller" value="group_controller" id='radio_main' checked/>Непосредственный руководитель
        </div>
    <div>
    <input type="radio" name="controller" value="admin_controller" id='radio_admin' />Администратор функции
    <select name="admin-select" class="admin-select" style='display:none;' disabled>
    <?php foreach($selectData['groups'] as $group):?>
    <option value="<?=$group['id']?>"><?=$group['name']?></option>
    <?php endforeach;?>
    </select>
        </div>
    <div>
    <input type="radio" name="controller" value="exec_controller" id='radio_exec'/>Исполнитель функции
        <select name="exec-select" class="exec-select" disabled>
    <?php foreach($selectData['groups'] as $group):?>
    <option value="<?=$group['id']?>"><?=$group['name']?></option>
    <?php endforeach;?>
    </select>
        </div>
    </div>

        <?= $this->render('CheckListPlugin:recurrence_checklist/checklist', [
                'checklist' => $checklist,
        ]) ?>

        <?= $this->modal->submitButtons() ?>
    </form>
