<details class="accordion-section" <?= empty($proofs['values']) ? '' : 'open' ?>>
    <summary class="accordion-title"><?= t('Доказательства') ?></summary>
    <div class="accordion-content">
        <?php if ($proofs):?>
        <?= $this->render('task_proof/table', array(
            'proofs' => $proofs,
            'task' => $task,
        )) ?>
        <?php endif;?>
    </div>
</details>