
<?php if ($proofs['type'] == 'file'): ?>
<?php if (!empty($proofs['values'])): ?>
    <?php if ($proofs['count'] < $proofs['count_due']): ?>
    <strong>Вам необходимо прикрепить еще <?=$proofs['count_due'] - $proofs['count']?> доказательство</strong>
    <?php else: ?>
    <strong>Все доказательства прикреплены</strong>
    <?php endif;?>
    <table class="table-striped table-scrolling">
        <tr>
            <th><?=t('Filename')?></th>
            <th class="column-15"><?=t('Size')?></th>
            <th class="column-15"><?=t('Creator')?></th>
            <th class="column-10"><?=t('Date')?></th>
        </tr>
        <?php foreach ($proofs['values'] as $file): ?>
            <tr>
                <td>
                    <i class="fa <?=$this->file->icon($file['name'])?> fa-fw"></i>
                    <div class="dropdown">
                        <a href="#" class="dropdown-menu dropdown-menu-link-text"><?=$this->text->e($file['name'])?> <i class="fa fa-caret-down"></i></a>
                        <ul>
                            <?php if ($this->file->getPreviewType($file['name']) !== null): ?>
                                <li>
                                    <?=$this->modal->large('eye', t('View file'), 'FileViewerController', 'show', array('task_id' => $task['id'], 'file_id' => $file['id']))?>
                                </li>
                            <?php elseif ($this->file->getBrowserViewType($file['name']) !== null): ?>
                                <li>
                                    <i class="fa fa-eye fa-fw"></i>
                                    <?=$this->url->link(t('View file'), 'FileViewerController', 'browser', array('task_id' => $task['id'], 'file_id' => $file['id']), false, '', '', true)?>
                                </li>
                            <?php endif?>
                            <li>
                                <?=$this->url->icon('download', t('Download'), 'FileViewerController', 'download', array('task_id' => $task['id'], 'file_id' => $file['id']))?>
                            </li>
                            <?php if ($this->user->hasProjectAccess('TaskFileController', 'remove', $task['project_id'])): ?>
                                <li>
                                    <?=$this->modal->confirm('trash-o', t('Remove'), 'TaskFileController', 'confirm', array('task_id' => $task['id'], 'file_id' => $file['id']))?>
                                </li>
                            <?php endif?>
                            <?=$this->hook->render('template:task-file:documents:dropdown', array('task' => $task, 'file' => $file))?>
                        </ul>
                    </div>
                </td>
                <td>
                    <?=$this->text->bytes($file['size'])?>
                </td>
                <td>
                    <?=$this->text->e($file['user_name'] ?: $file['username'])?>
                </td>
                <td>
                    <?=$this->dt->date($file['date'])?>
                </td>
            </tr>
        <?php endforeach?>
    </table>
<?php endif;?>
<?php endif;?>
<?php if ($proofs['type'] == 'image'): ?>
    <?php if (!empty($proofs['values'])): ?>
        <?php if ($proofs['count'] < $proofs['count_due']): ?>
    <strong>Вам необходимо прикрепить еще <?=$proofs['count_due'] - $proofs['count']?> доказательство</strong>
    <?php else: ?>
    <strong>Все доказательства прикреплены</strong>
    <?php endif;?>
    <div class="file-thumbnails">
        <?php foreach ($proofs['values'] as $file): ?>
            <div class="file-thumbnail">
                <?=$this->app->component('image-slideshow', array(
    'images' => $proofs['values'],
    'image' => $file,
    'regex' => 'FILE_ID',
    'url' => array(
        'image' => $this->url->to('FileViewerController', 'image', array('file_id' => 'FILE_ID', 'task_id' => $task['id'])),
        'thumbnail' => $this->url->to('FileViewerController', 'thumbnail', array('file_id' => 'FILE_ID', 'task_id' => $task['id'])),
        'download' => $this->url->to('FileViewerController', 'download', array('file_id' => 'FILE_ID', 'task_id' => $task['id'])),
    ),
))?>

                <div class="file-thumbnail-content">
                    <div class="file-thumbnail-title">
                        <div class="dropdown">
                            <a href="#" class="dropdown-menu dropdown-menu-link-text" title="<?=$this->text->e($file['name'])?>"><?=$this->text->e($file['name'])?> <i class="fa fa-caret-down"></i></a>
                            <ul>
                                <li>
                                    <?=$this->url->icon('external-link', t('View file'), 'FileViewerController', 'image', array('task_id' => $task['id'], 'file_id' => $file['id']), false, '', '', true)?>
                                </li>
                                <li>
                                    <?=$this->url->icon('download', t('Download'), 'FileViewerController', 'download', array('task_id' => $task['id'], 'file_id' => $file['id']))?>
                                </li>
                                <?php if ($this->user->hasProjectAccess('TaskFileController', 'remove', $task['project_id'])): ?>
                                    <li>
                                        <?=$this->modal->confirm('trash-o', t('Remove'), 'TaskFileController', 'confirm', array('task_id' => $task['id'], 'file_id' => $file['id']))?>
                                    </li>
                                <?php endif?>
                                <?=$this->hook->render('template:task-file:images:dropdown', array('task' => $task, 'file' => $file))?>
                            </ul>
                        </div>
                    </div>
                    <div class="file-thumbnail-description">
                        <?=$this->hook->render('template:task-file:images:before-thumbnail-description', array('task' => $task, 'file' => $file))?>
                        <?=$this->app->tooltipMarkdown(t('Uploaded: %s', $this->dt->datetime($file['date'])) . "\n\n" . t('Size: %s', $this->text->bytes($file['size'])))?>
                        <?php if (!empty($file['user_id'])): ?>
                            <?=t('Uploaded by %s', $file['user_name'] ?: $file['username'])?>
                        <?php else: ?>
                            <?=t('Uploaded: %s', $this->dt->datetime($file['date']))?>
                        <?php endif?>
                    </div>
                </div>
            </div>
        <?php endforeach?>
    </div>
<?php endif;?>
<?php endif;?>

<?php if ($proofs['type'] == 'text'): ?>
    <input type='text' readonly value='<?=$proofs['values'][0]['proof']?>'>
<?php endif;?>
