<ul class="views">
    <?= $this->hook->render('template:project-header:view-switcher-before-board-view', array('project' => $project, 'filters' => $filters)) ?>

        <li class="desktop-ver" <?= $this->app->checkMenuSelection('VectorBoardController') ?>>
            <?= $this->url->icon('th', t('Мои'), 'VectorBoardController', 'show_my_tasks', array('plugin' => 'VectorTasks','project_id' => $project['id'], 'search' => $filters['search'], 'task' => 'owner'), false, 'view-board', t('Keyboard shortcut: "%s"', 'v b')) ?>
        </li>
        <li class="desktop-ver" <?= $this->app->checkMenuSelection('ParticipationTaskBoardController') ?>>
            <?= $this->url->icon('th', t('Участвую'), 'ParticipationTaskBoardController', 'show', array('plugin' => 'TasksParticipations','project_id' => $project['id'], 'search' => $filters['search'], 'task' => 'participant'), false, 'view-board', t('Keyboard shortcut: "%s"', 'v b')) ?>
        </li>
        <li class="desktop-ver" <?= $this->app->checkMenuSelection('ControlledTaskBoardController') ?>>
            <?= $this->url->icon('th', t('Контролирую'), 'ControlledTaskBoardController', 'show', array('plugin' => 'ControlledTasks','project_id' => $project['id'], 'search' => $filters['search'], 'task' => 'creator'), false, 'view-board', t('Keyboard shortcut: "%s"', 'v b')) ?>
        </li>

        <!--Mobile links added Fixermobile-->
        <li class="mobile-ver" <?= $this->app->checkMenuSelection('FixerMobileControllerOwner') ?>>
            <?= $this->url->icon('list', t('Мои'), 'FixerMobileController', 'show', array('plugin' => 'StyleFixerMobile', 'project_id' => $project['id'], 'search' => $filters['search'], 'task' => 'owner'), false, 'view-listing', t('Keyboard shortcut: "%s"', 'v b')) ?>
        </li>
        <li class="mobile-ver" <?= $this->app->checkMenuSelection('FixerMobileControllerParticipant') ?>>
            <?= $this->url->icon('list', t('Участвую'), 'FixerMobileController', 'show', array('plugin' => 'StyleFixerMobile', 'project_id' => $project['id'], 'search' => $filters['search'], 'task' => 'participant'), false, 'view-listing', t('Keyboard shortcut: "%s"', 'v b')) ?>
        </li>
        <li class="mobile-ver" <?= $this->app->checkMenuSelection('FixerMobileControllerCreator') ?>>
            <?= $this->url->icon('list', t('Контролирую'), 'FixerMobileController', 'show', array('plugin' => 'StyleFixerMobile', 'project_id' => $project['id'], 'search' => $filters['search'], 'task' => 'creator'), false, 'view-listing', t('Keyboard shortcut: "%s"', 'v b')) ?>
        </li>

    <?= $this->hook->render('template:project-header:view-switcher-before-task-list', array('project' => $project, 'filters' => $filters)) ?>

    <li <?= $this->app->checkMenuSelection('TaskListController') ?>>
        <?= $this->url->icon('list', t('List'), 'TaskListController', 'show', array('project_id' => $project['id'], 'search' => $filters['search']), false, 'view-listing', t('Keyboard shortcut: "%s"', 'v l')) ?>
    </li>

    <?= $this->hook->render('template:project-header:view-switcher', array('project' => $project, 'filters' => $filters)) ?>
</ul>
