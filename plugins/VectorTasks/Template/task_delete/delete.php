<div class="page-header">
    <h2><?= t('Remove a task') ?></h2>
</div>

<div class="confirm">
    <p class="alert alert-info">
        <?= t('Do you really want to remove this task: "%s"?', $this->text->e($title)) ?>
    </p>

    <?= $this->modal->confirmButtons(
        'VectorTasksController',
        'removeRecurrenceTask',
        array('plugin'=>'VectorTasks','recurrence_id' => $recurrence_id)
    ) ?>
</div>
