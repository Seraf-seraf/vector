<div class="page-header">
    <h2><?= t('Добавить доказательство?') ?></h2>
</div>
<?= $this->app->flashMessage() ?>

<form method="post" id='TaskForm' enctype="multipart/form-data" action="<?= $this->url->href('ProofTaskController', 'save', array('plugin'=>'VectorTasks','recurrence_id' => '')) ?>" autocomplete="off">
<?= $this->form->csrf() ?>
<?= $this->form->hidden('proof_type', $params) ?>
<?= $this->form->hidden('task_id', array('task_id'=>$task_id)) ?>
<?= $this->form->hidden('id', $params) ?>
<?= $this->form->label(t('Описание доказательства:'), 'proof_desc') ?>
<div><?=$params['proof_description']?></div>
<?php if ($params['proof_count']>0):?>
<div>Количество доказательств - <?=$params['proof_count']?></div>
<?php endif;?>
<?php if ($params['proof_type']=='text'):?>
    <?= $this->form->label(t('Напишите текст доказательства'), 'proof_text') ?>
    <?= $this->form->text('proof', $values, $errors, array('autofocus', 'required', 'maxlength="191"')) ?>
<?php endif;?>
<?php if ($params['proof_type']=='link'):?>
    <?= $this->form->label(t('Вставьте ссылку доказательства'), 'proof_text') ?>
    <?= $this->form->text('proof', $values, $errors, array('autofocus', 'required', 'maxlength="191"')) ?>
<?php endif;?>
<?php if ($params['proof_type']=='file' || $params['proof_type']=='image'):?>
<?= $this->app->component('file-upload', array(
    'csrf'              => $this->app->getToken()->getReusableCSRFToken(),
    'maxSize'           => $max_size=500000000,
    // 'url'               => $this->url->to('VectorTasksController', 'saveRecurrenceTask', array('plugin' => 'VectorTasks')),
    'labelDropzone'     => t('Drag and drop your files here'),
    'labelOr'           => t('or'),
    'labelChooseFiles'  => t('choose files'),
    'labelOversize'     => $max_size > 0 ? t('The maximum allowed file size is %sB.', $this->text->bytes($max_size)) : null,
    'labelSuccess'      => t('All files have been uploaded successfully.'),
    'labelCloseSuccess' => t('Close this window'),
    'labelUploadError'  => t('Unable to upload this file.'),
)) ?>
<?php endif;?>

<?= $this->modal->submitButtons() ?>
    </form>