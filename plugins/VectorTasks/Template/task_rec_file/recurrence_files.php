<?php if (! empty($files)): ?>
    <table class="table-striped table-scrolling">
        <tr>
            <th><?= t('Filename') ?></th>
            <th class="column-15"><?= t('Size') ?></th>
            <th class="column-15"><?= t('Creator') ?></th>
            <th class="column-10"><?= t('Date') ?></th>
        </tr>
        <?php foreach ($files as $file): ?>
            <tr>
                <td>
                    <i class="fa <?= $this->file->icon($file['name']) ?> fa-fw"></i>
                    <div class="dropdown">
                        <a href="#" class="dropdown-menu dropdown-menu-link-text"><?= $this->text->e($file['name']) ?> <i class="fa fa-caret-down"></i></a>
                        <ul>
                            <?php if ($this->file->getPreviewType($file['name']) !== null): ?>
                                <li>
                                    <?= $this->modal->large('eye', t('View file'), 'VectorFileController', 'show', array('plugin'=>'VectorTasks','task_id' => $recurrence_id, 'file_id' => $file['id'])) ?>
                                </li>
                            <?php endif ?>
                            <li>
                                <?= $this->url->icon('download', t('Download'), 'VectorFileController', 'download', array('plugin'=>'VectorTasks','task_id' => $recurrence_id, 'file_id' => $file['id'])) ?>
                            </li>
                            <?php if ($this->user->hasProjectAccess('VectorFileController', 'remove', $recurrence_id)): ?>
                                <li>
                                    <?= $this->modal->confirm('trash-o', t('Remove'), 'VectorFileController', 'confirm', array('plugin'=>'VectorTasks','recurrence_id' => $recurrence_id, 'file_id' => $file['id'],'start_data'=>$start_data)) ?>
                                </li>
                            <?php endif ?>
                            <?= $this->hook->render('template:task-file:documents:dropdown', array('task' => $recurrence_id, 'file' => $file)) ?>
                        </ul>
                    </div>
                </td>
                <td>
                    <?= $this->text->bytes($file['size']) ?>
                </td>
                <td>
                    <?= $this->text->e($file['user_name'] ?: $file['username']) ?>
                </td>
                <td>
                    <?= $this->dt->date($file['date']) ?>
                </td>
            </tr>
        <?php endforeach ?>
    </table>
<?php endif ?>
