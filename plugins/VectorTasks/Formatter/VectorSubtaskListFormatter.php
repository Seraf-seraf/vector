<?php

namespace Kanboard\Plugin\VectorTasks\Formatter;

class VectorSubtaskListFormatter extends \Kanboard\Formatter\SubtaskListFormatter
{
    /**
     * Apply formatter
     *
     * @access public
     * @return array
     */
    public function format()
    {
        $status = $this->vectorSubtaskModel->getStatusList();
        $subtasks = $this->query->eq('ischeckbox', 0)->findAll();
        foreach ($subtasks as &$subtask) {
            $subtask['status_name'] = $status[$subtask['status']];
            $subtask['timer_start_date'] = isset($subtask['timer_start_date']) ? $subtask['timer_start_date'] : 0;
            $subtask['is_timer_started'] = ! empty($subtask['timer_start_date']);
        }

        return $subtasks;
    }
}