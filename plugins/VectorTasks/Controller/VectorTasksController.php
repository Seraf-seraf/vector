<?php

namespace Kanboard\Plugin\VectorTasks\Controller;

use DateTime;
use Kanboard\Controller\BaseController;
use Kanboard\Core\Controller\PageNotFoundException;
use Kanboard\Model\CategoryModel;
use Kanboard\Model\ColorModel;
use Kanboard\Model\ColumnModel;
use Kanboard\Model\GroupMemberModel;
use Kanboard\Model\ProjectUserRoleModel;
use Kanboard\Model\SwimlaneModel;
use Kanboard\Model\TaskFinderModel;
use Kanboard\Model\TaskProjectDuplicationModel;
use Kanboard\Model\UserModel;
use Kanboard\Plugin\Group_assign\Model\MultiselectMemberModel;
use Kanboard\Plugin\Group_assign\Model\MultiselectModel;
use Kanboard\Plugin\RelatedGroups\Model\GroupRelatedModel;
use Kanboard\Plugin\VectorTasks\Model\ProofModel;
use Kanboard\Plugin\VectorTasks\Model\RecurrenceTaskModel;
use Kanboard\Plugin\VectorTasks\Model\TaskCreationModel;
use Kanboard\Plugin\VectorTasks\Model\VectorTaskFileModel;
use Kanboard\Plugin\CheckListPlugin\Class\CheckList;

class VectorTasksController extends BaseController
{
    /**
     * Display a form to create a new task
     *
     * @access public
     * @param  array $values
     * @param  array $errors
     * @throws PageNotFoundException
     */
    public function show(array $values = array(), array $errors = array())
    {
        $project = $this->getProject();
        $swimlanesList = $this->swimlaneModel->getList($project['id'], false, true);
        $values += $this->prepareValues($project['is_private'], $swimlanesList);

        $values = $this->hook->merge('controller:task:form:default', $values, array('default_values' => $values));
        $values = $this->hook->merge('controller:task-creation:form:default', $values, array('default_values' => $values));

        $this->response->html($this->template->render('task_creation/show', array(
            'project' => $project,
            'errors' => $errors,
            'values' => $values + array('project_id' => $project['id']),
            'columns_list' => $this->columnModel->getList($project['id']),
            'users_list' => $this->projectUserRoleModel->getAssignableUsersList($project['id'], true, false, $project['is_private'] == 1),
            'categories_list' => $this->categoryModel->getList($project['id']),
            'swimlanes_list' => $swimlanesList,
        )));
    }

    /**
     * Validate and save a new task
     *
     * @access public
     */
    public function save()
    {
        $project = $this->getProject();
        $values = $this->request->getValues();
        $values['project_id'] = $project['id'];
        $values['controller_id'] = $this->userSession->getId();
        $proof_type = $values['proof-select'];
        $proof_desc = $values['proof_desc'];
        $proof_count = $values['proof_count'];
        if (!$proof_count) {
            $proof_count = 1;
        }
        unset($values['proof-select']);
        unset($values['proof_desc']);
        unset($values['proof_count']);

        $newCheckList = $values['new-checklist-text'];
        unset($values['new-checklist-text']);
        foreach ($values as $name => $value) if (str_starts_with($name, 'checklist-')) {
            unset($values[$name]);
        }
        
        if (isset($values['owner_gp']) && !empty($values['owner_gp'])) {
            $list = $this->groupMemberModel->getMembers($values['owner_gp']);
            foreach ($list as $el) {
                $values['owner_id'] = $el['user_id'];
                $task_id = $this->taskCreationModel->create($values);
                if ($task_id > 0) {
                    $this->flash->success(t('Task created successfully.'));
                    $this->afterSave($project, $values, $task_id);
                } else {
                    $this->flash->failure(t('Unable to create this task.'));
                    $this->response->redirect($this->helper->url->to('BoardViewController', 'show', array('project_id' => $project['id'])), true);
                }
            }
        }
        if (isset($values['owner_ms']) && !empty($values['owner_ms'])) {
            $ms_id = $this->multiselectModel->create();
            foreach ($values['owner_ms'] as $user) {
                $this->multiselectMemberModel->addUser($ms_id, $user);
            }
            unset($values['owner_ms']);
            $values['owner_ms'] = $ms_id;
        }

        list($valid, $errors) = $this->taskValidator->validateCreation($values);

        if (!$valid) {
            $this->flash->failure(t('Unable to create your task.'));
            $this->show($values, $errors);
        } elseif (!$this->helper->projectRole->canCreateTaskInColumn($project['id'], $values['column_id'])) {
            $this->flash->failure(t('You cannot create tasks in this column.'));
            $this->response->redirect($this->helper->url->to('BoardViewController', 'show', array('project_id' => $project['id'])), true);
        } else {

            $task_id = $this->taskCreationModel->create($values);
            if ($proof_type !== 'not') {
                $this->taskCreationModel->set_task_proof(array('task_id' => $task_id, 'proof_type' => $proof_type, 'proof_description' => $proof_desc, 'proof_count' => $proof_count));
            }

            if ($task_id > 0) {
                $newCheckList = new CheckList($this->helper->user->getId(), $newCheckList);
                $this->checkListModel->saveNewCheckList($task_id, $newCheckList);

                $this->flash->success(t('Task created successfully.'));
                $this->afterSave($project, $values, $task_id);

            } else {
                $this->flash->failure(t('Unable to create this task.'));
                $this->response->redirect($this->helper->url->to('BoardViewController', 'show', array('project_id' => $project['id'])), true);
            }
        }
    }

    /**
     * Duplicate created tasks to multiple projects
     *
     * @throws PageNotFoundException
     */
    public function duplicateProjects()
    {
        $project = $this->getProject();
        $values = $this->request->getValues();

        if (isset($values['project_ids'])) {
            foreach ($values['project_ids'] as $project_id) {
                $this->taskProjectDuplicationModel->duplicateToProject($values['task_id'], $project_id);
            }
        }

        $this->response->redirect($this->helper->url->to('BoardViewController', 'show', array('project_id' => $project['id'])), true);
    }
    public function showRecurrenceTaskMenu(array $values = array(), array $errors = array())
    {
        $group_id = $this->request->getIntegerParam('group_id');
        $rec_tasks = $this->recurrenceTaskModel->get_recurrence_tasks_by_group_id($group_id);
        $rec_data = array();
        $values = array();
        foreach ($rec_tasks as $task) {
            $rec_data[$task['recurrence_id']] = $this->recurrenceTaskModel->get_recurrence_select($task["recurrence_id"]);
        }
        foreach ($rec_data as $key => $item) {
            foreach ($item as $subKey => $subItem) {
                foreach ($subItem as $el) {
                    $values[$key][$subKey][$el['type']][] = $el['value'];
                }
            }
            $title = $this->recurrenceTaskModel->get_meta_header($key)[0];
            if (!$title) {
                $title = 'Без заголовка';
            }

            $values[$key]['title'] = array($title);
        }
        $this->response->html($this->template->render('task_creation/show_recurrence_menu', array(
            'group_id' => $group_id,
            'values' => $values,
        )));
    }

    public function showRecurrenceTask(array $values = array(), array $errors = array())
    {
        $selectData = array();
        $checklist = array();
        $selectData['groups'] = $this->groupRelatedModel->getAll();
        $selectData['exec_types'] = $this->groupRelatedModel->getExecTypes();
        $selectData['admin_level'] = $this->groupRelatedModel->getAdminLevels();
        $task = array();
        $this->response->html($this->template->render('task_creation/show_recurrence', array(
            'values' => $values,
            'selectData' => $selectData,
            'errors' => $errors,
            'task' => $task,
            'checklist' => $checklist,
            'recurrence_status_list' => $this->taskRecurrenceModel->getRecurrenceStatusList(),
            'recurrence_trigger_list' => $this->taskRecurrenceModel->getRecurrenceTriggerList(),
            'recurrence_timeframe_list' => $this->taskRecurrenceModel->getRecurrenceTimeframeList(),
            'recurrence_basedate_list' => $this->taskRecurrenceModel->getRecurrenceBasedateList(),
        )));
    }
    public function editRecurrenceTask(array $values = array(), array $errors = array())
    {
        $start_data = $this->request->getStringParam('select_start');
        $recurrence_id = $this->request->getStringParam('recurrence_id');
        $checkList = $this->recurrenceTaskCheckListModel->getCheckList($recurrence_id);
        $selectData = array();
        $selectData['groups'] = $this->groupRelatedModel->getAll();
        $selectData['exec_types'] = $this->groupRelatedModel->getExecTypes();
        $selectData['admin_level'] = $this->groupRelatedModel->getAdminLevels();
        $convertData = array();
        foreach ($start_data as $key => $type) {
            foreach ($type as $sub_key => $sub_type) {
                $convertData['types'][$sub_key] = $this->helper->convertHelper->convert_select_type($sub_key);
                if (is_array($sub_type)) {
                    foreach ($sub_type as $el) {
                        $convertData['values'][$sub_key][$el] = $this->helper->convertHelper->convert_select_value($sub_key, $el);
                    }
                }

            }

        }
        $title = $start_data['title'];
        $time_params = $this->recurrenceTaskModel->get_recurring_time_params($recurrence_id);
        if ($time_params['recurring_time']) {
            $recurring_time = new DateTime();
            $recurring_time->setTimestamp($time_params['recurring_time']);
            $time_start = new DateTime($recurring_time->format('H:i'));
            $recurring_time = $recurring_time->format('H:i');
        } else {
            $recurring_time = '';
        }

        if ($time_params['time_due'] && $time_params['recurring_time']) {
            $time_due = new DateTime();
            $time_due->setTimestamp($time_params['time_due']);
            $time_end = new DateTime($time_due->format('H:i'));
            $time_due = $time_start->diff($time_end)->format('%H:%I');
        } else {
            $time_due = '';
        }

        $meta_data = $this->recurrenceTaskModel->get_meta_data($recurrence_id);
        unset($start_data['title']);
        $recurrence_params = $this->recurrenceTaskModel->get_params($recurrence_id);
        $proof_params = $this->proofModel->get_proof_params_by_rec_id($recurrence_id);
        if (!$proof_params) {
            $proof_params['proof_count'] = '';
            $proof_params['proof_type'] = '';
            $proof_params['proof_description'] = '';
        }
        $this->response->html($this->template->render('task_edit/edit', array(
            'files' => $this->vectorTaskFileModel->getRecurrenceFiles($recurrence_id),
            'values' => $values,
            'selectData' => $selectData,
            'title' => $title,
            'errors' => $errors,
            'start_data' => $start_data,
            'recurrence_id' => $recurrence_id,
            'recurrence_status_list' => $this->taskRecurrenceModel->getRecurrenceStatusList(),
            'recurrence_trigger_list' => $this->taskRecurrenceModel->getRecurrenceTriggerList(),
            'recurrence_timeframe_list' => $this->taskRecurrenceModel->getRecurrenceTimeframeList(),
            'recurrence_basedate_list' => $this->taskRecurrenceModel->getRecurrenceBasedateList(),
            'convertData' => $convertData,
            'meta_data' => $meta_data,
            'recurrence_params' => $recurrence_params,
            'time_due' => $time_due,
            'recurring_time' => $recurring_time,
            'proof_params' => $proof_params,
            'checklist' => $checkList
        )));
    }
    public function saveRecurrenceTask()
    {
        $values = $this->request->getValues();
//         var_dump($values);
//         return;
        // Применение json_decode ко всем элементам массива
        if ($values['SelectList']) {
            $decodedSelectList = array_map(function ($jsonStr) {
                return json_decode($jsonStr, true);
            }, $values['SelectList']);
        } else {
            $decodedSelectList = array();
        }

        if ($values['IgnoreList']) {
            $decodedIgnoreList = array_map(function ($jsonStr) {
                return json_decode($jsonStr, true);
            }, $values['IgnoreList']);
        } else {
            $decodedIgnoreList = array();
        }

        $allowTasks = $this->prepareRecurrenceData($decodedSelectList, $decodedIgnoreList);
        $ignore_list = $add_list = $user_list = $group_list = array();
        $keys_array = array_keys($allowTasks['add']);
        $keys_array = array_unique(array_merge($keys_array, array_keys($allowTasks['ignore'])));
        $filterData = $this->filterGroupsAndUsersByRecurrenceTask($keys_array, $allowTasks);
        $ignore_list = $filterData['ignore_list'];
        $user_list = $filterData['user_list'];
        $add_list = $filterData['add_list'];
        $group_list = $filterData['group_list'];
        $add_list = array_unique($add_list);
        $ignore_list = array_unique($ignore_list);
        sort($add_list);
        sort($ignore_list);
        $result_list = array_diff($add_list, $ignore_list);
        sort($result_list);
        $task_data = array();
        $task_data['title'] = $values['title'];
        $task_data['description'] = $values['description'];
        $task_data['result'] = $values['result'];
        $task_data['recurrence_trigger'] = 4;
        if (!$values['recurrence_factor']) {
            $values['recurrence_factor'] = 1;
        }
        $task_data['recurrence_factor'] = $values['recurrence_factor'];
        $task_data['recurrence_timeframe'] = $values['recurrence_timeframe'];
        $task_data['recurrence_basedate'] = 0;
        $recurrence_date = new DateTime($values['recurrence_date']);
        $time_due = new DateTime($values['time_due']);
        $time_now = new DateTime();
        $recurring_time = $time_now->modify('today ' . $recurrence_date->format('H:i'));
        $time_now_2 = clone $time_now;
        $isToday = $recurring_time->getTimestamp() > time();
        $time_due = $time_now_2->modify('+' . $time_due->format('H') . ' hours +' . $time_due->format('i') . ' minutes');
        if ($isToday) {
            $task_data['date_due'] = $time_due->getTimestamp();
            $task_data['recurring_time'] = $recurring_time->getTimestamp();
        } else {
            $time_due->modify('+1 day');
            $recurring_time->modify('+1 day');
            $task_data['date_due'] = $time_due->getTimestamp();
            $task_data['recurring_time'] = $recurring_time->getTimestamp();
        }
        $task_data['color_id'] = 'red';
        $task_data['project_id'] = 1;
        $task_data['column_id'] = 1;
        $task_data['recurrence_status'] = 1;
        $r_task_id = 0;

        $groups = $this->group_select($group_list);
        if (!isset($values['recurrence_id'])) {
            $r_task_id = $this->recurrenceTaskModel->create(array('recurrence_factor' => $values['recurrence_factor'], 'recurrence_timeframe' => $values['recurrence_timeframe'], 'time_due' => $time_due->getTimestamp(), 'recurring_time' => $recurring_time->getTimestamp()));
        }

        $newCheckList = $values['new-checklist-text'];

        if (!isset($values['recurrence_id'])) {
            $newCheckList = new CheckList($this->helper->user->getId(), $newCheckList);
            $this->recurrenceTaskCheckListModel->save($r_task_id, $newCheckList);
        } else {
            $newCheckList = new CheckList($this->helper->user->getId(), $newCheckList);
            $this->recurrenceTaskCheckListModel->save($values['recurrence_id'], $newCheckList);
        }

        $oldCheckList = array_filter($values, function ($value, $key) {
            return str_starts_with($key, 'checklist-');
        }, ARRAY_FILTER_USE_BOTH);

        if (isset($values['recurrence_id'])) {
            $oldCheckList = array_filter($values, function ($value, $key) {
                return str_starts_with($key, 'checklist-');
            }, ARRAY_FILTER_USE_BOTH);

            $oldCheckList = CheckList::prepareDataForEdit($oldCheckList);
            $this->recurrenceTaskCheckListModel->update($oldCheckList);
        }

//        var_dump($oldCheckList);die();

        if (isset($values['recurrence_id'])) {
            $proof_params = $this->proofModel->get_proof_params_by_rec_id($values['recurrence_id']);
            if ($values['proof-select'] !== 'not') {
                if ($proof_params) {
                    $this->recurrenceTaskModel->update_recurrence_task_proof(array('id' => $proof_params['id'], 'recurrence_id' => $values['recurrence_id'], 'proof_type' => $values['proof-select'], 'proof_count' => $values['proof_count'], 'proof_description' => $values['proof_desc']));
                } else {
                    $this->recurrenceTaskModel->set_recurrence_task_proof(array('recurrence_id' => $r_task_id, 'proof_type' => $values['proof-select'], 'proof_count' => $values['proof_count'], 'proof_description' => $values['proof_desc']));
                }

            }
            if ($proof_params && $values['proof-select'] == 'not') {
                $this->recurrenceTaskModel->remove_recurrence_task_proof($proof_params['id']);
            }
            $this->recurrenceTaskModel->update_group_recurrence_task(array('groups' => $groups, 'recurrence_id' => $values['recurrence_id']));
        } else {
            if ($values['proof-select'] !== 'not') {
                $this->recurrenceTaskModel->set_recurrence_task_proof(array('recurrence_id' => $r_task_id, 'proof_type' => $values['proof-select'], 'proof_count' => $values['proof_count'], 'proof_description' => $values['proof_desc']));
            }

            if (isset($groups['ids'])) {
                foreach ($groups['ids'] as $group) {
                    $this->recurrenceTaskModel->set_group_recurrence_task(array('group_id' => $group, 'recurrence_id' => $r_task_id, 'ignore_admin' => $groups[$group]['ignore_admin'], 'ignore_exec' => $groups[$group]['ignore_exec']));
                }
            }
        }

        if (!isset($values['recurrence_id'])) {
            $task_data['recurrence_id'] = $r_meta['recurrence_id'] = $allowTasks['recurrence_id'] = $r_task_id;
        } else {
            $task_data['recurrence_id'] = $r_meta['recurrence_id'] = $allowTasks['recurrence_id'] = $values['recurrence_id'];
        }
        $r_meta['title'] = $values['title'];
        $r_meta['description'] = $values['description'];
        $r_meta['result'] = $values['result'];
        if (!isset($values['recurrence_id'])) {
            $this->recurrenceTaskModel->add_meta_info($r_meta);
            $this->recurrenceTaskModel->add_select_options($allowTasks);
            foreach ($result_list as $user_id) {
                $group_id = $this->groupRelatedModel->groupContainUser($groups['ids'], $user_id)['group_id'];
                if ($values['controller'] == 'group_controller') {
                    $controller_id = $this->recurrenceTaskModel->getRecurrenceController($group_id);
                }

                if (isset($controller_id[0]['id'])) {
                    $task_data['controller_id'] = $controller_id[0]['id'];
                    $this->recurrenceTaskModel->set_recurrence_user_task($user_id, $r_task_id, $controller_id[0]['id']);
                } else {
                    $task_data['controller_id'] = $this->userSession->getId();
                    $this->recurrenceTaskModel->set_recurrence_user_task($user_id, $r_task_id, $this->userSession->getId());
                }
            }
        } else {
            $this->recurrenceTaskModel->update_recurrence_task(array('id' => $values['recurrence_id'], 'recurrence_factor' => $values['recurrence_factor'], 'recurrence_timeframe' => $values['recurrence_timeframe'], 'time_due' => $time_due->getTimestamp(), 'recurring_time' => $recurring_time->getTimestamp()));
            $this->recurrenceTaskModel->update_meta_info($r_meta);
            $this->recurrenceTaskModel->update_select_options($allowTasks);
            $exist_users = array();
            $exist_users = $this->taskCreationModel->update(array('owner_ids' => $result_list, 'recurrence_id' => $values['recurrence_id']), $task_data);
            $result_list = array_diff($result_list, array_column($exist_users, 'owner_id'));
            foreach ($result_list as $user_id) {
                $group_id = $this->groupRelatedModel->groupContainUser($groups['ids'], $user_id)['group_id'];
                $controller_id = $this->recurrenceTaskModel->getRecurrenceController($group_id);
                if (isset($controller_id[0]['id'])) {
                    $task_data['controller_id'] = $controller_id[0]['id'];
                    $this->recurrenceTaskModel->set_recurrence_user_task($user_id, $values['recurrence_id'], $controller_id[0]['id']);
                } else {
                    $task_data['controller_id'] = $this->userSession->getId();
                    $this->recurrenceTaskModel->set_recurrence_user_task($user_id, $values['recurrence_id'], $task_data['controller_id']);
                }

            }
        }

        if (!$this->request->getFileInfo('files')['error'][0]) {
            if (!isset($values['recurrence_id'])) {
                $result = $this->vectorTaskFileModel->uploadFilesRecurrenceTask($this->request->getFileInfo('files'), $r_task_id);
            } else {
                // $exist_tasks=array_column($this->recurrenceTaskModel->get_tasks_by_recurrence_id($values['recurrence_id']),'id');
                // $result = $this->vectorTaskFileModel->uploadFiles($exist_tasks, $this->request->getFileInfo('files'), $values['recurrence_id']);
                $result = $this->vectorTaskFileModel->uploadFilesRecurrenceTask($this->request->getFileInfo('files'), $values['recurrence_id']);
            }
        }
    }
    private function filterGroupsAndUsersByRecurrenceTask(array $data, array $allowTasks)
    {
        $user_list = $group_list = $add_list = $ignore_list = array();
        foreach ($data as $key) {
            switch ($key) {
                case "all":
                    $user_list = $this->userModel->getActiveUsersList();
                    $group_list['add']['all'] = array_column($this->groupRelatedModel->getAll(), 'id');
                    foreach ($user_list as $user => $name) {
                        $add_list[] = $user;
                    }
                    break;
                case "admins":
                    if (array_key_exists($key, $allowTasks['ignore']) || array_key_exists($key, $allowTasks['add'])) {
                        if (array_key_exists($key, $allowTasks['add'])) {
                            $group_list['add']['admins'] = array_column($this->groupRelatedModel->getAll(), 'id');
                        } else {
                            $group_list['ignore']['admins'] = array_column($this->groupRelatedModel->getAll(), 'id');
                        }

                        $unique_admins = array_column($this->groupRelatedModel->getAdminsUnique(), 'id');
                        foreach ($unique_admins as $admin) {
                            if (array_key_exists($key, $allowTasks['ignore'])) {
                                $ignore_list[] = $admin;
                            } else {
                                $add_list[] = $admin;
                            }

                        }
                    }
                    break;
                case "admin_level":
                    if (array_key_exists($key, $allowTasks['add'])) {
                        foreach ($allowTasks['add'][$key][0] as $el => $val) {
                            $arr = array_column($this->groupRelatedModel->getAdminsByLevel($val), 'id');
                            foreach ($arr as $col) {
                                $add_list[] = $col;
                            }
                        }
                    }
                    if (array_key_exists($key, $allowTasks['ignore'])) {
                        $group_list['ignore']['admin_level'] = array_column($this->groupRelatedModel->getGroupsByAdminLevel($allowTasks['ignore']['admin_level'][0]), 'id');
                        foreach ($allowTasks['ignore'][$key][0] as $el => $val) {
                            $arr = array_column($this->groupRelatedModel->getAdminsByLevel($val), 'id');
                            foreach ($arr as $col) {
                                $ignore_list[] = $col;
                            }
                        }
                    }
                    // $ignore_list=array_merge($ignore_list,$admin_level_list);
                    break;
                case "exec_type":
                    if (array_key_exists($key, $allowTasks['add'])) {
                        $group_list['add']['exec_type'] = array_column($this->groupRelatedModel->getGroupsByExecType($allowTasks['add']['exec_type'][0]), 'group_id');
                        foreach ($allowTasks['add'][$key] as $el => $val) {
                            $executors_by_exec_type = array_column($this->groupRelatedModel->getExecutorsByType($val[0]), 'id');
                            foreach ($executors_by_exec_type as $executor) {
                                $add_list[] = $executor;
                            }
                        }
                    }
                    if (array_key_exists($key, $allowTasks['ignore'])) {
                        $group_list['ignore']['exec_type'] = array_column($this->groupRelatedModel->getGroupsByExecType($allowTasks['ignore']['exec_type'][0]), 'group_id');
                        foreach ($allowTasks['ignore'][$key] as $el => $val) {
                            $executors_by_exec_type = array_column($this->groupRelatedModel->getExecutorsByType($val[0]), 'id');
                            foreach ($executors_by_exec_type as $executor) {
                                $ignore_list[] = $executor;
                            }
                        }
                    }
                    break;
                case "admin_function":
                    //     var_dump($allowTasks['ignore']);
                    // return;
                    if (array_key_exists($key, $allowTasks['add'])) {
                        $group_list['add']['admin_function'] = array_column($this->groupRelatedModel->getGroupsById($allowTasks['add']['admin_function'][0]), 'id');
                        foreach ($allowTasks['add'][$key] as $el => $val) {
                            $admins_by_group = array_column($this->groupRelatedModel->getAdminsByGroup($val[0]), 'id');
                            foreach ($admins_by_group as $admin) {
                                $add_list[] = $admin;
                            }
                        }
                    }
                    if (array_key_exists($key, $allowTasks['ignore'])) {
                        $group_list['ignore']['admin_function'] = array_column($this->groupRelatedModel->getGroupsById($allowTasks['ignore']['admin_function'][0]), 'id');
                        foreach ($allowTasks['ignore'][$key][0] as $el => $val) {
                            $admins_by_group = array_column($this->groupRelatedModel->getAdminsByGroup($val[0]), 'id');
                            foreach ($admins_by_group as $admin) {
                                $ignore_list[] = $admin;
                            }
                        }
                    }
                    // var_dump($add_list);
                    // return;
                    break;
                case "exec_function":
                    if (array_key_exists($key, $allowTasks['add'])) {
                        $group_list['add']['exec_function'] = array_column($this->groupRelatedModel->getGroupsById($allowTasks['add']['exec_function'][0]), 'id');
                        $executors_by_group = array_column($this->groupRelatedModel->getExecutorsByGroup($allowTasks['add'][$key][0]), 'id');
                        foreach ($executors_by_group as $executor) {
                            $add_list[] = $executor;
                        }
                    }
                    if (array_key_exists($key, $allowTasks['ignore'])) {
                        $group_list['ignore']['exec_function'] = array_column($this->groupRelatedModel->getGroupsById($allowTasks['ignore']['exec_function'][0]), 'id');
                        $executors_by_group = array_column($this->groupRelatedModel->getExecutorsByGroup($allowTasks['ignore'][$key][0]), 'id');
                        foreach ($executors_by_group as $executor) {
                            $ignore_list[] = $executor;
                        }
                    }
            }
            // var_dump($add_list);
            // var_dump($ignore_list);
        }
        return array('add_list' => $add_list, 'ignore_list' => $ignore_list, 'user_list' => $user_list, 'group_list' => $group_list);
    }
    private function prepareRecurrenceData(array $decodedSelectList, array $decodedIgnoreList)
    {
        $allowTasks = array();
        if ($decodedSelectList[0] && $decodedIgnoreList[0]) {
            foreach ($decodedSelectList[0] as $key => $value) {
                $diff_arr = array();
                if (array_key_exists($key, $decodedIgnoreList[0])) {
                    $diff_arr = array_diff($decodedSelectList[0][$key], $decodedIgnoreList[0][$key]);
                    foreach ($diff_arr as $el => $val) {
                        $allowTasks['add'][$key][] = array($val);
                    }
                } else {
                    $allowTasks['add'][$key][] = $value;
                }
            }
        } elseif ($decodedSelectList[0]) {
            foreach ($decodedSelectList[0] as $key => $val) {
                $allowTasks['add'][$key][] = $val;
            }
        }

        if ($decodedSelectList[0] && $decodedIgnoreList[0]) {
            foreach ($decodedIgnoreList[0] as $key => $value) {
                $diff_arr = array();
                if (array_key_exists($key, $decodedSelectList[0])) {
                    $diff_arr = array_diff($decodedIgnoreList[0][$key], $decodedSelectList[0][$key]);
                    foreach ($diff_arr as $el => $val) {
                        $allowTasks['ignore'][$key][] = array($val);
                    }
                } else {
                    $allowTasks['ignore'][$key][] = $value;
                }

            }
            // var_dump($allowTasks['ignore']);
            // return;
        } elseif ($decodedIgnoreList[0]) {
            foreach ($decodedIgnoreList[0] as $key => $val) {
                $allowTasks['ignore'][$key][] = $val;
            }
        }

        if (!array_key_exists('ignore', $allowTasks)) {
            $allowTasks['ignore'] = array();
        }

        if (!array_key_exists('add', $allowTasks)) {
            $allowTasks['add'] = array();
        }

        return $allowTasks;
    }
    public function removeRecurrenceTaskMenu(array $array = array())
    {
        $recurrence_id = $this->request->getIntegerParam('recurrence_id');
        $title = $this->request->getStringParam('title');
        $this->response->html($this->template->render('task_delete/delete', array(
            'recurrence_id' => $recurrence_id,
            'title' => $title,
        )));
    }
    public function removeRecurrenceTask()
    {
        $this->checkCSRFParam();
        $recurrence_id = $this->request->getIntegerParam('recurrence_id');
        $this->recurrenceTaskModel->remove($recurrence_id);
        $this->flash->success('Задача успешно удалена');
        $this->response->redirect($this->helper->url->to('GroupListController', 'index'), true);
    }
    protected function group_select($group_list)
    {
        $merge_arr = array();
        $merge_exec_arr = array();
        $merge_admin_arr = array();
        if (!isset($group_list['ignore'])) {
            $group_list['ignore'] = array();
        }

        $keys_ignore = array_keys($group_list['ignore']);
        if (!isset($group_list['add'])) {
            $group_list['add'] = array();
        }

        $all = array_key_exists('all', $group_list['add']);
        foreach ($group_list['add'] as $group => $arr) {
            if (!$all) {
                if ($group == 'exec_type' || $group == 'exec_function') {
                    $merge_exec_arr = array_merge($merge_exec_arr, $arr);
                }
                if ($group == 'all_admins' || $group == 'admin_level' || $group == 'admin_function') {
                    $merge_admin_arr = array_merge($merge_admin_arr, $arr);
                }
            }

            $merge_arr = array_merge($merge_arr, $arr);
        }
        $merge_exec_arr = array_unique($merge_exec_arr);
        $merge_admin_arr = array_unique($merge_admin_arr);
        $merge_arr = array_unique($merge_arr);
        $result_arr = array();
        foreach ($merge_arr as $el) {
            if (!$all) {
                if (in_array($el, $merge_admin_arr)) {
                    $ignore_admin = false;
                } else {
                    $ignore_admin = true;
                }

                if (in_array($el, $merge_exec_arr)) {
                    $ignore_exec = false;
                } else {
                    $ignore_exec = true;
                }
            } else {
                $ignore_exec = false;
            }

            $ignore_admin = false;

            foreach ($keys_ignore as $key) {
                foreach ($group_list['ignore'][$key] as $group) {
                    if (($el == $group) && ($key == 'all_admins' || $key == 'admin_level' || $key == 'admin_function') && ($ignore_admin == false)) {
                        $ignore_admin = true;
                        continue;
                    }
                    if (($el == $group) && ($key == 'exec_type' || $key == 'exec_function') && ($ignore_exec == false)) {
                        $ignore_exec = true;
                        continue;
                    }
                }
            }
            if ($ignore_admin && $ignore_exec) {
                continue;
            } else {
                $result_arr['ids'][] = $el;
                $result_arr[$el]['ignore_admin'] = $ignore_admin;
                $result_arr[$el]['ignore_exec'] = $ignore_exec;
            }

        }
        return $result_arr;
    }

    //     foreach ($selects['add'])
    //     var_dump($selects);
    // }
    // protected function array_diff_assoc_recursive($array1, $array2) {
    //     foreach ($array1 as $key => $value) {
    //         if (is_array($value) && isset($array2[$key]) && is_array($array2[$key])) {
    //             $diff = $this->array_diff_assoc_recursive($value, $array2[$key]);
    //             if (empty($diff)) {
    //                 unset($array1[$key]);
    //             } else {
    //                 $array1[$key] = $diff;
    //             }
    //         } elseif (isset($array2[$key]) && $array2[$key] === $value) {
    //             unset($array1[$key]);
    //         }
    //     }
    //     return $array1;
    // }

    /**
     * Executed after the task is saved
     *
     * @param array   $project
     * @param array   $values
     * @param integer $task_id
     */
    protected function afterSave(array $project, array &$values, $task_id)
    {
        if (isset($values['duplicate_multiple_projects']) && $values['duplicate_multiple_projects'] == 1) {
            $this->chooseProjects($project, $task_id);
        } elseif (isset($values['another_task']) && $values['another_task'] == 1) {
            $this->show(array(
                'owner_id' => $values['owner_id'],
                'color_id' => $values['color_id'],
                'category_id' => isset($values['category_id']) ? $values['category_id'] : 0,
                'column_id' => $values['column_id'],
                'swimlane_id' => isset($values['swimlane_id']) ? $values['swimlane_id'] : 0,
                'another_task' => 1,
            ));
        } else {
            $this->response->redirect($this->helper->url->to('BoardViewController', 'show', array('project_id' => $project['id'])), true);
        }
    }

    /**
     * Prepare form values
     *
     * @access protected
     * @param  bool  $isPrivateProject
     * @param  array $swimlanesList
     * @return array
     */
    protected function prepareValues($isPrivateProject, array $swimlanesList)
    {
        $values = array(
            'swimlane_id' => $this->request->getIntegerParam('swimlane_id', key($swimlanesList)),
            'column_id' => $this->request->getIntegerParam('column_id'),
            'color_id' => $this->colorModel->getDefaultColor(),
        );

        if ($isPrivateProject) {
            $values['owner_id'] = $this->userSession->getId();
        }

        return $values;
    }

    /**
     * Choose projects
     *
     * @param array $project
     * @param integer $task_id
     */
    protected function chooseProjects(array $project, $task_id)
    {
        $task = $this->taskFinderModel->getById($task_id);
        $projects = $this->projectUserRoleModel->getActiveProjectsByUser($this->userSession->getId());
        unset($projects[$project['id']]);

        $this->response->html($this->template->render('task_creation/duplicate_projects', array(
            'project' => $project,
            'task' => $task,
            'projects_list' => $projects,
            'values' => array('task_id' => $task['id']),
        )));
    }
}
