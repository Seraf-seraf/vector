<?php

namespace Kanboard\Plugin\VectorTasks\Controller;

use Kanboard\Controller\BaseController;
use Kanboard\Plugin\VectorTasks\Model\ProofFileModel;
use Kanboard\Plugin\VectorTasks\Model\ProofModel;
use Kanboard\Plugin\VectorTasks\Model\RecurrenceTaskModel;

class ProofTaskController extends BaseController
{
    public function show($task_id = 0, array $values = array(), array $errors = array())
    {
        if (!$task_id) {
            $task_id = $this->request->getIntegerParam('task_id');
        }

        $r_id = $this->recurrenceTaskModel->get_recurrence_id_by_task_id($task_id);
        if ($r_id) {
            $proof_params = $this->proofModel->get_proof_params_by_rec_id($r_id['recurrence_id']);
        } else {
            $proof_params = $this->proofModel->get_proof_params_by_task_id($task_id);
        }

        $this->response->html($this->template->render('proof_creation/show', array(
            'params' => $proof_params,
            'task_id' => $task_id,
            'errors' => $errors,
            'values' => $values,
        )));
    }
    public function save(array $values = array(), array $errors = array())
    {
        $values = $this->request->getValues();
        if ($values['proof_type'] == 'file' || $values['proof_type'] == 'image') {
            if (!$this->request->getFileInfo('files')['error'][0]) {
                $result = $this->proofFileModel->uploadProofFiles($values['task_id'], $this->request->getFileInfo('files'), 'true', $values['proof_type']);
            }
        }
        list($valid, $errors) = $this->proofValidator->validateField($values);
        if ($valid) {
            list($valid, $errors) = $this->proofValidator->validateType($values);
            if ($valid) {
                $this->proofModel->set_proof(array('task_id' => $values['task_id'], 'proof_id' => $values['id'], 'proof' => $values['proof']));
            } else {
                return $this->show($values['task_id'], $values, $errors);
            }
        } else {
            return $this->show($values['task_id'], $values, $errors);
        }
        $this->flash->success(t('Доказательство успешно добавлено'));
        $this->response->redirect($this->helper->url->to('VectorTaskViewController', 'show', array('plugin' => 'VectorTasks', 'task_id' => $values['task_id'])));
        // return;
    }
}
