<?php

namespace Kanboard\Plugin\VectorTasks\Controller;

use Kanboard\Core\Controller\AccessForbiddenException;
use Kanboard\Model\TaskModel;
use Kanboard\Plugin\VectorTasks\Formatter\VectorBoardFormatter;
use Kanboard\Controller\BaseController;


class VectorBoardController extends BaseController
{
    /**
     * Display the public version of a board
     * Access checked by a simple token, no user login, read only, auto-refresh
     *
     * @access public
     */
    public function readonly()
    {
        $token = $this->request->getStringParam('token');
        $project = $this->projectModel->getByToken($token);

        if (empty($project)) {
            throw AccessForbiddenException::getInstance()->withoutLayout();
        }

        $query = $this->taskFinderModel
            ->getExtendedQuery()
            ->eq(TaskModel::TABLE.'.is_active', TaskModel::STATUS_OPEN);

        $this->response->html($this->helper->layout->app('board/view_public', array(
            'project' => $project,
            'swimlanes' => $this->vectorBoardFormatter
                ->withProjectId($project['id'])
                ->withQuery($query)
                ->format()
            ,
            'title' => $project['name'],
            'description' => $project['description'],
            'no_layout' => true,
            'not_editable' => true,
            'board_public_refresh_interval' => $this->configModel->get('board_public_refresh_interval'),
            'board_private_refresh_interval' => $this->configModel->get('board_private_refresh_interval'),
            'board_highlight_period' => $this->configModel->get('board_highlight_period'),
        )));
    }

    /**
     * Show a board for a given project
     *
     * @access public
     */
    public function show_my_tasks()
    {
        $project = $this->getProject();
        $this->response->html($this->helper->layout->app('board/view_private', array(
            'project' => $project,
            'title' => $project['name'],
            'my_tasks' => $this->request->getStringParam('task'),
            'description' => $this->helper->projectHeader->getDescription($project),
            'board_private_refresh_interval' => $this->configModel->get('board_private_refresh_interval'),
            'board_highlight_period' => $this->configModel->get('board_highlight_period'),
            'swimlanes' => $this->taskLexer
                ->build('')
                ->format($this->vectorBoardFormatter->withProjectId($project['id']),$this->vectorBoardFormatter->withUserId($this->getUser()['id']))
        )));
    }
}
