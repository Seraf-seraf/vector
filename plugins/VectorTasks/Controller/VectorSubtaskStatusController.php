<?php

namespace Kanboard\Plugin\VectorTasks\Controller;

use Kanboard\Controller\SubtaskStatusController;
use Kanboard\Core\Controller\AccessForbiddenException;
use Kanboard\Core\Controller\PageNotFoundException;
use Kanboard\Model\SubtaskModel;

class VectorSubtaskStatusController extends SubtaskStatusController
{
    protected function getSubtask(array $task)
    {
        $subtask = $this->vectorSubtaskListFormatter->withQuery(
                $this->db->table(SubtaskModel::TABLE)
                ->eq('id', $this->request->getIntegerParam('subtask_id'))
        )->format()[0];

        if (empty($subtask)) {
            throw new PageNotFoundException();
        }

        if ($subtask['task_id'] != $task['id']) {
            throw new AccessForbiddenException();
        }

        return $subtask;
    }

    /**
     * Change status to the next status: Toto -> In Progress -> Done
     *
     * @access public
     */
    public function change()
    {
        $this->checkReusableGETCSRFParam();
        $task = $this->getTask();
        $subtask = $this->getSubtask($task);
        $fragment = $this->request->getStringParam('fragment');

        $status = $this->subtaskStatusModel->toggleStatus($subtask['id']);
        $subtask['status'] = $status;

        if ($fragment === 'table') {
            $html = $this->renderTable($task);
        } elseif ($fragment === 'rows') {
            $html = $this->renderRows($task);
        } else {
            $html = $this->helper->vectorSubtaskHelper->renderToggleStatus($task, $subtask);
        }

        $this->response->html($html);
    }

    /**
     * Start/stop timer for subtasks
     *
     * @access public
     */
    public function timer()
    {
        $this->checkReusableGETCSRFParam();
        $task = $this->getTask();
        $subtask = $this->getSubtask($task);
        $timer = $this->request->getStringParam('timer');

        if ($timer === 'start') {
            $this->subtaskTimeTrackingModel->logStartTime($subtask['id'], $this->userSession->getId());
        } elseif ($timer === 'stop') {
            $this->subtaskTimeTrackingModel->logEndTime($subtask['id'], $this->userSession->getId());
            $this->subtaskTimeTrackingModel->updateTaskTimeTracking($task['id']);
        }

        $this->response->html($this->template->render('VectorTasks:subtask/timer', array(
            'task'    => $task,
            'subtask' => $this->subtaskModel->getByIdWithDetails($subtask['id']),
        )));
    }

    /**
     * Render table
     *
     * @access protected
     * @param  array  $task
     * @return string
     */
    protected function renderTable(array $task)
    {
        return $this->template->render('VectorTasks:subtask/table', array(
            'task'     => $task,
            'subtasks' => $this->vectorSubtaskListFormatter->withQuery(
                    $this->db->table(SubtaskModel::TABLE)
                    ->eq('task_id', $task['id']))->format(),
            'editable' => true,
        ));
    }

    /**
     * Render task list rows
     *
     * @access protected
     * @param  array  $task
     * @return string
     */
    protected function renderRows(array $task)
    {
        $userId = $this->request->getIntegerParam('user_id');

        $task['subtasks'] = $this->vectorSubtaskListFormatter->withQuery(
                            $this->db->table(SubtaskModel::TABLE)
                            ->eq('task_id', $task['id']))->format();

        return $this->template->render('task_list/task_subtasks', array(
            'task'    => $task,
            'user_id' => $userId,
        ));
    }
}
