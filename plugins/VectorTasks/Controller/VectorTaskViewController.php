<?php

namespace Kanboard\Plugin\VectorTasks\Controller;

use Kanboard\Controller\TaskViewController;
use Kanboard\Model\SubtaskModel;
use Kanboard\Model\UserMetadataModel;
use Kanboard\Plugin\VectorTasks\Model\ProofFileModel;
use Kanboard\Plugin\VectorTasks\Model\ProofModel;
use Kanboard\Plugin\VectorTasks\Model\RecurrenceTaskModel;

/**
 * Task Controller
 *
 * @package  Kanboard\Controller
 * @author   Frederic Guillot
 */
class VectorTaskViewController extends TaskViewController
{
    public function show()
    {
        $task = $this->getTask();

//        $subtasks = $this->vectorSubtaskListFormatter->withQuery(
//                $this->db->table(SubtaskModel::TABLE)
//                ->eq('task_id', $task['id'])
//                ->asc('position'))->format();
        $subtasks = [];

        $checklist = $this->checkListModel->getCheckboxes($task['id']);
        $commentSortingDirection = $this->userMetadataCacheDecorator->get(UserMetadataModel::KEY_COMMENT_SORTING_DIRECTION, 'ASC');
        $r_id = $this->recurrenceTaskModel->get_recurrence_id_by_task_id($task['id']);
        if ($r_id) {
            $proof_params = $this->proofModel->get_proof_params_by_rec_id($r_id['recurrence_id']);
        } else {
            $proof_params = $this->proofModel->get_proof_params_by_task_id($task['id']);
        }

        $proofs = array();
        if ($proof_params) {
            $proofs['type'] = $proof_params['proof_type'];
            if ($proof_params['proof_type'] == 'text' || $proof_params['proof_type'] == 'link') {
                $proofs['values'] = $this->proofModel->get_proofs($task['id']);
            }

            if ($proof_params['proof_type'] == 'file' || $proof_params['proof_type'] == 'image') {
                $proofs['values'] = $this->proofFileModel->getAllDocumentsProof($task['id']);
                $proofs['count'] = $this->proofFileModel->proof_count($task['id']);
                $proofs['count_due'] = $proof_params['proof_count'];
            }
        }
        $this->response->html($this->helper->layout->task('task/show', array(
            'task' => $task,
            'project' => $this->projectModel->getById($task['project_id']),
            'files' => $this->proofFileModel->getAllDocuments($task['id']),
            'images' => $this->proofFileModel->getAllImages($task['id']),
            'comments' => $this->commentModel->getAll($task['id'], $commentSortingDirection),
            'subtasks' => $subtasks,
            'checklist' => $checklist,
            'internal_links' => $this->taskLinkModel->getAllGroupedByLabel($task['id']),
            'external_links' => $this->taskExternalLinkModel->getAll($task['id']),
            'link_label_list' => $this->linkModel->getList(0, false),
            'tags' => $this->taskTagModel->getTagsByTask($task['id']),
            'proofs' => $proofs,
        )));
    }
}
