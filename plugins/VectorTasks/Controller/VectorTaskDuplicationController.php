<?php

namespace Kanboard\Plugin\VectorTasks\Controller;

use Kanboard\Controller\TaskDuplicationController;

class VectorTaskDuplicationController extends TaskDuplicationController
{
    public function duplicate()
    {
        $task = $this->getTask();

        if ($this->request->getStringParam('confirmation') === 'yes') {
            $this->checkCSRFParam();
            $task_id = $this->vectorTaskDuplicationModel->duplicate($task['id']);

            if ($task_id > 0) {
                $this->flash->success(t('Task created successfully.'));
                return $this->response->redirect($this->helper->url->to('VectorTaskViewController', 'show', array('plugin' => 'VectorTasks', 'task_id' => $task_id)));
            } else {
                $this->flash->failure(t('Unable to create this task.'));
                return $this->response->redirect($this->helper->url->to('VectorTaskDuplicationController', 'duplicate', array('plugin' => 'VectorTasks', 'task_id' => $task['id'])), true);
            }
        }

        return $this->response->html($this->template->render('VectorTasks:task_duplication/duplicate', array(
            'task' => $task,
        )));
    }
}