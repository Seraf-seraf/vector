<?php

namespace Kanboard\Plugin\VectorTasks\Helper;

use Kanboard\Core\Base;
use Kanboard\Plugin\VectorTasks\Model\ProofModel;

class ProofHelper extends Base
{
    public function proofExist($task_id)
    {
        return $this->proofModel->proof_exist($task_id);
    }
}
