<?php

namespace Kanboard\Plugin\VectorTasks\Helper;

use Kanboard\Core\Base;
use Kanboard\Plugin\RelatedGroups\Model\GroupRelatedModel;

class ConvertHelper extends Base
{
    public function convert_select_type($type)
    {
        switch ($type) {
            case "all":
                $type = 'Всем';
                break;
            case 'admins':
                $type = 'Всем администраторам';
                break;
            case 'admin_level':
                $type = 'Администратору уровня';
                break;
            case 'exec_type':
                $type = 'Исполнителям типа';
                break;
            case 'admin_function':
                $type = 'Администратор функции';
                break;
            case 'exec_function':
                $type = 'Исполнителям функции';
                break;
        }
        return $type;
    }
    public function convert_select_value($type, $value)
    {
        switch ($type) {
            case 'admin_level':
                $type = 'Администратору уровня';
                return $value;
                break;
            case 'exec_type':
                $type = 'Исполнителям типа';
                return $value;
                break;
            case 'admin_function':
                $type = 'Администратор функции';
                $value = $this->groupRelatedModel->getById($value)['name'];
                break;
            case 'exec_function':
                $type = 'Исполнителям функции';
                $value = $this->groupRelatedModel->getById($value)['name'];
                break;
            default:
                $value = '';
        }
        return $value;
    }
}
