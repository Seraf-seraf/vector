<?php

namespace Kanboard\Plugin\VectorTasks\Helper\Subtask;

use \Kanboard\Helper\SubtaskHelper as OldSubtaskHelper;
class VectorSubtaskHelper extends OldSubtaskHelper
{
    /**
     * Get the link to toggle subtask status
     *
     * @access public
     * @param  array  $task
     * @param  array  $subtask
     * @param  string $fragment
     * @param  int    $userId
     * @return string
     */
    public function renderToggleStatus(array $task, array $subtask, $fragment = '', $userId = 0)
    {
        if (! $this->helper->user->hasProjectAccess('SubtaskController', 'edit', $task['project_id'])) {
            $html = $this->renderTitle($subtask);
        } else {
            $title = $this->renderTitle($subtask);
            $params = array(
                'plugin' => 'VectorTasks',
                'project_id' => $task['project_id'],
                'task_id'    => $subtask['task_id'],
                'subtask_id' => $subtask['id'],
                'user_id'    => $userId,
                'fragment'   => $fragment,
                'csrf_token' => $this->token->getReusableCSRFToken()
            );

            if ($subtask['status'] == 0 && $this->hasSubtaskInProgress()) {
                $html = $this->helper->url->link($title, 'SubtaskRestrictionController', 'show', $params, false, 'js-modal-confirm', $this->getSubtaskTooltip($subtask));
            } else {
                $html = $this->helper->url->link($title, 'VectorSubtaskStatusController', 'change', $params, false, 'js-subtask-toggle-status', $this->getSubtaskTooltip($subtask));
            }
        }

        return '<span class="subtask-title">'.$html.'</span>';
    }

    public function renderTimer(array $task, array $subtask)
    {
        $html = '<span class="subtask-timer-toggle">';
        $params = array(
            'plugin' => 'VectorTasks',
            'task_id' => $subtask['task_id'],
            'subtask_id' => $subtask['id'],
            'timer' => '',
            'csrf_token' => $this->token->getReusableCSRFToken(),
        );

        if ($subtask['is_timer_started']) {
            $params['timer'] = 'stop';
            $html .= $this->helper->url->icon('pause', t('Stop timer'), 'VectorSubtaskStatusController', 'timer', $params, false, 'js-subtask-toggle-timer');
            $html .= ' (' . $this->helper->dt->age($subtask['timer_start_date']) .')';
        } else {
            $params['timer'] = 'start';
            $html .= $this->helper->url->icon('play-circle-o', t('Start timer'), 'VectorSubtaskStatusController', 'timer', $params, false, 'js-subtask-toggle-timer');
        }

        $html .= '</span>';

        return $html;
    }
}