<?php

namespace Kanboard\Plugin\VectorTasks;

use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Translator;
use Kanboard\Plugin\VectorTasks\Action\CreateDuplicateTask;
use Kanboard\Plugin\VectorTasks\Controller\VectorTaskViewController;
use Kanboard\Plugin\VectorTasks\Formatter\VectorSubtaskListFormatter;

class Plugin extends Base
{
    public function initialize()
    {
        $this->template->setTemplateOverride('task_creation/show', 'VectorTasks:task_creation/show');
        $this->template->setTemplateOverride('project_header/views', 'VectorTasks:project_header/views');
        $this->template->setTemplateOverride('project_list/project_title', 'VectorTasks:project_list/project_title');
        $this->helper->register('ajaxHelper', '\Kanboard\Plugin\VectorTasks\Helper\AjaxHelper');
        $this->helper->register('convertHelper', '\Kanboard\Plugin\VectorTasks\Helper\ConvertHelper');
        $this->helper->register('proofHelper', '\Kanboard\Plugin\VectorTasks\Helper\ProofHelper');
        $this->helper->register('vectorSubtaskHelper', '\Kanboard\Plugin\VectorTasks\Helper\Subtask\VectorSubtaskHelper');
        $this->helper->register('checkListHelper', 'Kanboard\Plugin\CheckListPlugin\Helper\CheckListHelper');
        $this->template->setTemplateOverride('board/table_container', 'VectorTasks:board/table_container');
        $this->template->setTemplateOverride('task_creation/show_recurrence', 'VectorTasks:task_creation/show_recurrence');
        $this->template->setTemplateOverride('task_creation/show_recurrence_menu', 'VectorTasks:task_creation/show_recurrence_menu');
        $this->template->setTemplateOverride('task_edit/edit', 'VectorTasks:task_edit/edit');
        $this->template->setTemplateOverride('task_duplication/duplicate', 'VectorTasks:task_duplication/duplicate');
        $this->template->setTemplateOverride('task_delete/delete', 'VectorTasks:task_delete/delete');
        $this->template->setTemplateOverride('task_rec_file/recurrence_files', 'VectorTasks:task_rec_file/recurrence_files');
        $this->template->setTemplateOverride('task_rec_file/remove', 'VectorTasks:task_rec_file/remove');
        $this->template->setTemplateOverride('task/show', 'VectorTasks:task_show/show');
        $this->template->setTemplateOverride('proof_creation/show', 'VectorTasks:proof_creation/show');
        $this->template->setTemplateOverride('task_proof/show', 'VectorTasks:task_proof/show');
        $this->template->setTemplateOverride('task_list/task_title', 'VectorTasks:task_list/task_title');
        $this->template->setTemplateOverride('task_proof/table', 'VectorTasks:task_proof/table');
        $this->template->setTemplateOverride('task_modification/show', 'VectorTasks:task_modification/show');
        $this->template->setTemplateOverride('subtask/table', 'VectorTasks:subtask/table');
        $this->template->setTemplateOverride('subtask/timer', 'VectorTasks:subtask/timer');
        $this->hook->on('template:layout:js', array('template' => 'plugins/VectorTasks/Assets/js/multi_tasks.js'));
        $this->hook->on('template:layout:js', array('template' => 'plugins/VectorTasks/Assets/js/test.js'));
        $this->hook->on('template:layout:css', array('template' => 'plugins/VectorTasks/Assets/css/recurrence.css'));
        $this->actionManager->register(new CreateDuplicateTask($this->container));
    }

    public function onStartup()
    {
        Translator::load($this->languageModel->getCurrentLanguage(), __DIR__ . '/Locale');
    }

    public function getPluginName()
    {
        return 'VectorTasks';
    }

    public function getPluginDescription()
    {
        return t('Task modification');
    }

    public function getPluginAuthor()
    {
        return 'Acidhead';
    }

    public function getPluginVersion()
    {
        return '1.0.0';
    }

    public function getPluginHomepage()
    {
        return 'https://github.com/kanboard/plugin-myplugin';
    }
    public function getClasses()
    {
        return [
            'Plugin\VectorTasks\Model' => [
                'TaskCreationModel',
                'VectorTaskFileModel',
                'RecurrenceTaskModel',
                'VectorTaskDuplicationModel',
                'VectorSubtaskModel',
                'VectorTaskRecurrenceModel',
                'ProofModel',
                'ProofFileModel',
            ],
            'Plugin\VectorTasks\Validator' => [
                'ProofValidator',
            ],
            'Plugin\VectorTasks\Formatter' => [
                'VectorBoardFormatter',
                'VectorBoardSwimlaneFormatter',
                'VectorBoardTaskFormatter',
                'VectorBoardColumnFormatter',
                'VectorSubtaskListFormatter',
            ],
            'Plugin\CheckListPlugin\Model' => [
                'RecurrenceTaskCheckListModel',
            ]
        ];
    }
}
