KB.on("modal.afterRender", function () {
  // Объявление переменных
  var dropdown = document.querySelector(".select-nav .dropdown-button");
  var dropdown2 = document.querySelector(".select-nav .dropdown-button-two");
  var dropdown_content_ignore = document.querySelector(
    ".select-ignore .dropdown-content-two"
  );
  var dropdown_content = document.querySelector(
    ".select-nav .dropdown-content-two"
  );
  var ignore_dropdown = document.querySelector(
    ".select-ignore .dropdown-button"
  );
  var ignore_dropdown2 = document.querySelector(
    ".select-ignore .dropdown-button-two"
  );
  var select_who = document.querySelector(".who-select");
  var select_ignore = document.querySelector(".who-ignore");
  var content_text_part = ""; // Инициализация переменной
  var arData = {};
  var ignoreData = {};
  // Получение данных и парсинг JSON

  var dataArray = document
    .querySelector('[name="SelectData"]')
    .getAttribute("data-array");
  dataArray = JSON.parse(dataArray);
  var convertArray = document.querySelector('[name="convertData"]');
  if (convertArray) {
    convertArray = convertArray.getAttribute("data-array");
    convertArray = JSON.parse(convertArray);
  }
  if (document.querySelector('[name="StartData"]'))
    addStartData(document.querySelector('[name="StartData"]'));
  // Обработчик события для первого dropdown
  dropdown.addEventListener("click", function () {
    document
      .querySelector(".select-nav .dropdown-content")
      .classList.toggle("show");
  });
  ignore_dropdown.addEventListener("click", function () {
    document
      .querySelector(".select-ignore .dropdown-content")
      .classList.toggle("show");
  });
  // Обработчик события для второго dropdown
  dropdown2.addEventListener("click", function () {
    document
      .querySelector(".select-nav .dropdown-content-two")
      .classList.toggle("show");
  });
  ignore_dropdown2.addEventListener("click", function () {
    document
      .querySelector(".select-ignore .dropdown-content-two")
      .classList.toggle("show");
  });
  // Обработчик события для элементов в первом dropdown
  document
    .querySelector(".select-nav .dropdown-content")
    .addEventListener("click", function (event) {
      if (event.target.closest(".select-nav .dropdown-item")) {
        if (
          event.target.dataset.value == "all" ||
          event.target.dataset.value == "admins"
        ) {
          // handleItemClick(dropdown, event.target);
          addParam(
            event.target,
            "",
            dropdown,
            dropdown2,
            select_who,
            select_ignore
          );
          return;
        }
        generateSelector2(event.target, dropdown2, dropdown, dropdown_content);
      }
    });
  document
    .querySelector(".select-ignore .dropdown-content")
    .addEventListener("click", function (event) {
      if (event.target.closest(".select-ignore .dropdown-item")) {
        if (
          event.target.dataset.value == "all" ||
          event.target.dataset.value == "admins"
        ) {
          // handleItemClick(ignore_dropdown, event.target);
          addParam(
            event.target,
            "",
            ignore_dropdown,
            ignore_dropdown2,
            select_ignore,
            select_who,
            true
          );
          return;
        }
        generateSelector2(
          event.target,
          ignore_dropdown2,
          ignore_dropdown,
          dropdown_content_ignore
        );
      }
    });

  // Обработчик события для элементов во втором dropdown
  document
    .querySelector(".select-nav .dropdown-content-two")
    .addEventListener("click", function (event) {
      if (event.target.closest(".dropdown-item")) {
        content_text_part = ""; // Сброс значения переменной
        switch (dropdown2.dataset.value) {
          case "admin_level":
            content_text_part = "Администратор уровня - ";
            break;
          case "exec_type":
            content_text_part = "Исполнителям типа - ";
            break;
          case "admin_function":
            content_text_part = "Админстратору функции - ";
            break;
          case "exec_function":
            content_text_part = "Исполнителям функции - ";
            break;
        }
        addParam(
          event.target,
          content_text_part,
          dropdown,
          dropdown2,
          select_who,
          select_ignore
        );
      }
    });
  document
    .querySelector(".select-ignore .dropdown-content-two")
    .addEventListener("click", function (event) {
      if (event.target.closest(".dropdown-item")) {
        content_text_part = ""; // Сброс значения переменной
        switch (ignore_dropdown2.dataset.value) {
          case "admin_level":
            content_text_part = "Администратор уровня - ";
            break;
          case "exec_type":
            content_text_part = "Исполнителям типа - ";
            break;
          case "admin_function":
            content_text_part = "Админстратору функции - ";
            break;
          case "exec_function":
            content_text_part = "Исполнителям функции - ";
            break;
        }
        addParam(
          event.target,
          content_text_part,
          ignore_dropdown,
          ignore_dropdown2,
          select_ignore,
          select_who,
          true
        );
      }
    });

  function addStartData(elem) {
    values = JSON.parse(elem.getAttribute("data-array"));
    fillSelect(select_who);
  }

  function fillSelect(select_who) {
    for (var key in values) {
      if (values.hasOwnProperty(key)) {
        var innerObject = values[key];

        for (var innerKey in innerObject) {
          if (innerObject.hasOwnProperty(innerKey)) {

            var innerArray = innerObject[innerKey];

            for (var i = 0; i < innerArray.length; i++) {
              (function (currentInnerKey, currentInnerArrayItem, key) {
                var ignore = key == "ignore";
                var newElement = document.createElement("span");
                // newElement.dataset.value = option.dataset.value;
                newElement.className = "target";
                value = convertArray.values[innerKey][currentInnerArrayItem];
                if (value)
                  newElement.dataset.value =
                    innerKey + "-" + currentInnerArrayItem;
                else newElement.dataset.value = innerKey;
                newElement.textContent = convertType(innerKey) + " " + value;
                if (!ignore) {
                  select_who.appendChild(newElement);
                  if (!arData[innerKey]) arData[innerKey] = [];
                  if (currentInnerArrayItem == 1) currentInnerArrayItem = true;
                  arData[innerKey].push(currentInnerArrayItem);
                } else {
                  if (!ignoreData[innerKey]) ignoreData[innerKey] = [];
                  if (currentInnerArrayItem == 1) currentInnerArrayItem = true;
                  ignoreData[innerKey].push(currentInnerArrayItem);
                  select_ignore.appendChild(newElement);
                }
                updateData(ignore);
                newElement.addEventListener("click", function () {
                  if (!ignore) {
                    deleteAddData(newElement, [
                      currentInnerKey,
                      currentInnerArrayItem,
                    ]);
                  } else {
                    deleteIgnoreData(newElement, [
                      currentInnerKey,
                      currentInnerArrayItem,
                    ]);
                  }
                  updateData(ignore);
                });
              })(innerKey, innerArray[i], key);
            }
          }
        }
      }
    }
  }

  function convertType(type) {
    switch (type) {
      case "all":
        type = "Все";
        break;
      case "admins":
        type = "Всем администраторам";
        break;
      case "admin_level":
        type = "Администраторам уровня";
        break;
      case "exec_type":
        type = "Исполнители типа";
        break;
      case "exec_function":
        type = "Исполнители функции";
      case "admin_function":
        type = "Администратору функции";
        break;
    }
    return type;
  }
  // Обработчик выбора опции
  function generateSelector2(elem, button, dropdown_item, dropdown_content) {
    handleItemClick(dropdown_item, elem);
    var button_text = "";
    content_text_part = ""; // Сброс значения переменной
    var array_group = [];
    var prop1;
    var prop2;
    switch (elem.dataset.value) {
      case "admin_level":
        button_text = "Выбрать уровень";
        content_text_part = "Администратор уровня - ";
        array_group = dataArray.admin_level;
        prop1 = "admin_level";
        prop2 = "admin_level";
        break;
      case "exec_type":
        button_text = "Выбрать тип";
        content_text_part = "Исполнителям типа - ";
        array_group = dataArray.exec_types;
        prop1 = "exec_type";
        prop2 = "exec_type";
        break;
      case "admin_function":
        button_text = "Выбрать функцию";
        content_text_part = "Админстратору функции - ";
        array_group = dataArray.groups;
        prop1 = "id";
        prop2 = "name";
        break;
      case "exec_function":
        button_text = "Выбрать функцию";
        content_text_part = "Исполнителям функции - ";
        prop1 = "id";
        prop2 = "name";
        array_group = dataArray.groups;
        break;
    }
    button.textContent = button_text;
    button.dataset.value = elem.dataset.value;
    button.style.display = "block";
    // var tst = document.querySelector(".dropdown-content-two");
    dropdown_content.innerHTML = "";
    array_group.forEach(function (item) {
      var newElement = document.createElement("a");
      newElement.dataset.value = elem.dataset.value + "-" + item[prop1];
      newElement.className = "dropdown-item";
      newElement.textContent = item[prop2];
      newElement.href = "#";
      dropdown_content.appendChild(newElement);
    });
  }

  // Обработчик выбора опции
  function handleItemClick(item, option) {
    item.textContent = option.textContent;
    // Дополнительные действия при выборе опции
  }
  // Добавление параметра при выборе опции
  function addParam(
    option,
    part_text = "",
    dropdown,
    dropdown2,
    select_who_1,
    select_who_2,
    ignore = false
  ) {
    var exist_elem = select_who_1.querySelector(
      '[data-value="' + option.dataset.value + '"]'
    );
    var exist_elem2 = select_who_2.querySelector(
      '[data-value="' + option.dataset.value + '"]'
    );
    if (exist_elem || exist_elem2) return;
    var tst = document.querySelector(".dropdown-content-two");
    tst.textContent = "";
    var newElement = document.createElement("span");
    newElement.dataset.value = option.dataset.value;
    newElement.className = "target";
    newElement.textContent = part_text + option.textContent;
    select_who_1.appendChild(newElement);
    dropdown.textContent = "Кому";
    dropdown2.style.display = "none";
    var keyParts = option.dataset.value.split("-");
    if (!ignore) {
      if (!arData[keyParts[0]]) arData[keyParts[0]] = [];
      if (!keyParts[1]) keyParts[1] = true;
      arData[keyParts[0]].push(keyParts[1]);
    } else {
      if (!ignoreData[keyParts[0]]) ignoreData[keyParts[0]] = [];
      if (!keyParts[1]) keyParts[1] = true;
      ignoreData[keyParts[0]].push(keyParts[1]);
    }
    updateData(ignore);
    newElement.addEventListener("click", function () {
      if (!ignore) {
        deleteAddData(newElement, keyParts);
      } else {
        deleteIgnoreData(newElement, keyParts);
      }
      updateData(ignore);
    });
  }

  function deleteAddData(el, keyParts) {
    var index = arData[keyParts[0]].indexOf(keyParts[1]);
    arData[keyParts[0]].splice(index, 1);
    if (arData[keyParts[0]].length == 0) delete arData[keyParts[0]];
    el.remove();
  }

  function deleteIgnoreData(el, keyParts) {
    var index = ignoreData[keyParts[0]].indexOf(keyParts[1]);
    ignoreData[keyParts[0]].splice(index, 1);
    if (ignoreData[keyParts[0]].length == 0) delete ignoreData[keyParts[0]];
    el.remove();
  }

  function updateData(ignore) {
    if (!ignore) { 
      document.getElementById("SelectList").value = JSON.stringify(arData);
    } else {
      document.getElementById("IgnoreList").value = JSON.stringify(ignoreData);
    }
  }
  // Закрытие dropdown при клике за его пределами
  function closeDropdowns(selector, className) {
    var dropdowns = document.getElementsByClassName(selector);
    for (var i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains(className)) {
        openDropdown.classList.remove(className);
      }
    }
  }

  // Закрытие dropdown при клике за его пределами
  window.onclick = function (event) {
    if (!event.target.matches(".dropdown-button")) {
      closeDropdowns("dropdown-content", "show");
    }
    if (!event.target.matches(".dropdown-button-two")) {
      closeDropdowns("dropdown-content-two", "show");
    }
  };
  var file_button = document.querySelector(".file-button");
  var input_file = document.getElementById("file-input-element");
  var selectedFiles = new DataTransfer();
  // var files_start_data = document
  //   .querySelector('[name="FilesData"]')
  //   .getAttribute("data-array");
  // files_start_data = JSON.parse(files_start_data);
  // // console.log(files_start_data);
  // for (var i = 0; i < files_start_data.length; i++) {
  //   var mimeType = 'application/json';
  //   var file = new File([], files_start_data[i].name, { type: 'application/json' });
  //   selectedFiles.items.add(file);
  // }
  // console.log(selectedFiles);

  // update_files();

  file_button.addEventListener("click", function () {
    input_file.click();
  });
  input_file.addEventListener("change", function () {
    var newFiles = input_file.files;

    for (var i = 0; i < newFiles.length; i++) {
      selectedFiles.items.add(newFiles[i]);
    }
    update_files();
    // Опционально: очищаем выбранные файлы из input_file
    // input_file.value = "";
    // displayThumbnails();
  });


  function update_files() {
    input_file.files = selectedFiles.files;
    console.log(input_file.files);
  }
});
