  KB.on("modal.afterRender", function () {
    $('.admin-select').select2();
    $('.exec-select').select2();
    $('.admin-select').next().hide();
    $('.exec-select').next().hide();
    $('#radio_admin').click(function() {
        $('.admin-select').prop('disabled', false);
        $('.admin-select').next().show();
        $('.exec-select').next().hide();
        $('.exec-select').prop('disabled', true);
    });
    $('#radio_main').click(function() {
        $('.admin-select').next().hide();
        $('.exec-select').next().hide();
    });
    $('#radio_exec').click(function() {
        $('.exec-select').prop('disabled', false);
        $('.exec-select').next().show();
        $('.admin-select').next().hide();
        $('.admin-select').prop('disabled', true);
    });
  });
  