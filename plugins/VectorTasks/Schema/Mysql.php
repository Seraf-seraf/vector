<?php

namespace Kanboard\Plugin\VectorTasks\Schema;

use PDO;

const VERSION = 9;

function version_9(PDO $pdo) {
    $pdo->exec("
    ALTER TABLE `tasks` ADD COLUMN `recurrence_id` INT,
    ADD CONSTRAINT `fk_recurrence_task`
    FOREIGN KEY (`recurrence_id`)
    REFERENCES `recurrence_tasks` (`id`)
    ON DELETE CASCADE
    ");
}

function version_8(PDO $pdo){
    $pdo->exec("CREATE TABLE `group_has_recurrence_tasks`(
        group_id INT NOT NULL,
        recurrence_id INT NOT NULL,
        FOREIGN KEY(recurrence_id) REFERENCES `recurrence_tasks`(id) ON DELETE CASCADE,
        FOREIGN KEY(group_id) REFERENCES `groups` (id) ON DELETE CASCADE,
        UNIQUE KEY `unique_group_recurrence` (`group_id`, `recurrence_id`)
        ) ENGINE=InnoDB CHARSET=utf8");
}
function version_7(PDO $pdo){
    $pdo->exec("ALTER TABLE `recurrence_tasks_select`
    MODIFY COLUMN `ignore` TINYINT
    ");
}
function version_6(PDO $pdo){
    $pdo->exec("ALTER TABLE `recurrence_tasks_select` ADD COLUMN `ignore` BIT");
}
function version_5(PDO $pdo){
    $pdo->exec("ALTER TABLE `recurrence_tasks` ADD COLUMN `date_creation` BIGINT");
}
function version_4(PDO $pdo){
    $pdo->exec("ALTER TABLE `recurrence_tasks` ADD COLUMN `creator_id` INT");
}
function version_3(PDO $pdo){
    $pdo->exec("
    ALTER TABLE `task_has_files` ADD COLUMN `recurrence_id` INT,
    ADD CONSTRAINT `fk_recurrence_id`
    FOREIGN KEY (`recurrence_id`)
    REFERENCES `recurrence_tasks` (`id`)
    ON DELETE CASCADE
    ");
}
function version_2(PDO $pdo)
{
    $pdo->exec("
        CREATE TABLE `recurrence_tasks`(
            id INT NOT NULL AUTO_INCREMENT PRIMARY KEY) ENGINE=InnoDB CHARSET=utf8
     ");
     $pdo->exec("
     CREATE TABLE `recurrence_tasks_select`(
         recurrence_id INT NOT NULL,
         type CHAR(16) NOT NULL,
         value CHAR(32) NOT NULL,
         PRIMARY KEY (recurrence_id),
         FOREIGN KEY(recurrence_id) REFERENCES `recurrence_tasks`(id) ON DELETE CASCADE
         ) ENGINE=InnoDB CHARSET=utf8
  ");
     $pdo->exec("
      CREATE TABLE `recurrence_tasks_meta`(
      recurrence_id INT NOT NULL,
      title TEXT DEFAULT NULL,
      description TEXT DEFAULT NULL,
      result TEXT DEFAULT NULL,
      PRIMARY KEY (recurrence_id),
      FOREIGN KEY(recurrence_id) REFERENCES `recurrence_tasks`(id) ON DELETE CASCADE
      ) ENGINE=InnoDB CHARSET=utf8
");
}
function version_1(PDO $pdo)
{
    $pdo->exec("ALTER TABLE `tasks` ADD COLUMN `result` TEXT");
}
