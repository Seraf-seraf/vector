<?php

namespace Kanboard\Plugin\VectorTasks\Schema;

use PDO;

const VERSION = 19;
function version_19(PDO $pdo)
{
    $pdo->exec("ALTER TABLE recurrence_task_has_proof ADD COLUMN task_id INTEGER,
        ADD CONSTRAINT fk_task_id
        FOREIGN KEY (task_id)
        REFERENCES tasks (id)
        ON DELETE CASCADE
    ");

}
function version_18(PDO $pdo)
{
    $pdo->exec("ALTER TABLE task_has_files ADD COLUMN proof BOOLEAN DEFAULT 'false'");
}
function version_17(PDO $pdo)
{
    $pdo->exec("CREATE TABLE task_has_proof (
        id SERIAL PRIMARY KEY,
        proof_id INTEGER NOT NULL,
        task_id INTEGER NOT NULL,
        proof TEXT NOT NULL DEFAULT '',
        FOREIGN KEY(task_id) REFERENCES tasks(id) ON DELETE CASCADE,
        FOREIGN KEY(proof_id) REFERENCES recurrence_task_has_proof(id) ON DELETE CASCADE
    );");
}
function version_16(PDO $pdo)
{
    $pdo->exec("ALTER TABLE recurrence_task_has_proof ADD COLUMN proof_description TEXT");

}
function version_15(PDO $pdo)
{
    $pdo->exec("CREATE TABLE recurrence_task_has_proof (
        id SERIAL PRIMARY KEY,
        recurrence_id INTEGER,
        proof_type TEXT NOT NULL DEFAULT '',
        proof_count INTEGER DEFAULT NULL,
        FOREIGN KEY(recurrence_id) REFERENCES recurrence_tasks(id) ON DELETE CASCADE
    );");
}
function version_14(PDO $pdo)
{
    $pdo->exec("CREATE TABLE user_has_recurrence_tasks (
        id SERIAL PRIMARY KEY,
        recurrence_id INTEGER NOT NULL,
        user_id INTEGER NOT NULL DEFAULT 0,
        creator_id INTEGER,
        FOREIGN KEY(recurrence_id) REFERENCES recurrence_tasks(id) ON DELETE CASCADE,
        FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE
    );");
}
function version_13(PDO $pdo)
{
    $pdo->exec("ALTER TABLE group_has_recurrence_tasks ADD COLUMN ignore_admin BOOLEAN");
    $pdo->exec("ALTER TABLE group_has_recurrence_tasks ADD COLUMN ignore_exec BOOLEAN");
}
function version_12(PDO $pdo)
{
    $pdo->exec("ALTER TABLE recurrence_tasks ADD COLUMN time_due INTEGER");
    $pdo->exec("ALTER TABLE recurrence_tasks ADD COLUMN recurring_time INTEGER");
}
function version_11(PDO $pdo)
{
    $pdo->exec("
    ALTER TABLE tasks ADD COLUMN recurring_time INTEGER");
}
function version_10(PDO $pdo)
{
    $pdo->exec("
        ALTER TABLE task_has_files ADD COLUMN recurrence_file_id INTEGER,
        ADD CONSTRAINT fk_recurrence_file_id
        FOREIGN KEY (recurrence_file_id)
        REFERENCES recurrence_task_has_files (id)
        ON DELETE CASCADE
    ");
    $pdo->exec("
    ALTER TABLE task_has_files
    DROP COLUMN IF EXISTS recurrence_id
    ");
}
function version_9(PDO $pdo)
{
    $pdo->exec("CREATE TABLE recurrence_task_has_files (
        id SERIAL PRIMARY KEY,
        name TEXT,
        path TEXT,
        is_image BOOLEAN DEFAULT '0',
        date INTEGER NOT NULL DEFAULT 0,
        user_id INTEGER NOT NULL DEFAULT 0,
        size INTEGER NOT NULL DEFAULT 0,
        recurrence_id INTEGER,
        FOREIGN KEY(recurrence_id) REFERENCES recurrence_tasks(id) ON DELETE CASCADE
    );");
    $pdo->exec('CREATE INDEX files_recurrence_task_idx ON recurrence_task_has_files(recurrence_id)');

}
function version_8(PDO $pdo)
{
    $pdo->exec("ALTER TABLE recurrence_tasks ADD COLUMN recurrence_factor INTEGER");
    $pdo->exec("ALTER TABLE recurrence_tasks ADD COLUMN recurrence_timeframe INTEGER");

}
function version_7(PDO $pdo)
{
    $pdo->exec("
    ALTER TABLE tasks ADD COLUMN recurrence_id INTEGER,
    ADD CONSTRAINT fk_recurrence_id
    FOREIGN KEY (recurrence_id)
    REFERENCES recurrence_tasks (id)
    ON DELETE CASCADE
    ");
}
function version_6(PDO $pdo)
{
    $pdo->exec("CREATE TABLE group_has_recurrence_tasks (
        group_id INTEGER NOT NULL,
        recurrence_id INTEGER NOT NULL,
        FOREIGN KEY(recurrence_id) REFERENCES recurrence_tasks(id) ON DELETE CASCADE,
        FOREIGN KEY(group_id) REFERENCES groups (id) ON DELETE CASCADE,
        UNIQUE (group_id, recurrence_id)
    )");
}

function version_5(PDO $pdo)
{
    $pdo->exec("ALTER TABLE recurrence_tasks_select ADD COLUMN ignore SMALLINT");
}

function version_4(PDO $pdo)
{
    $pdo->exec("ALTER TABLE recurrence_tasks ADD COLUMN date_creation BIGINT");
    $pdo->exec("ALTER TABLE recurrence_tasks ADD COLUMN creator_id INTEGER");
}

function version_3(PDO $pdo)
{
    $pdo->exec("
        ALTER TABLE task_has_files ADD COLUMN recurrence_id INTEGER,
        ADD CONSTRAINT fk_recurrence_id
        FOREIGN KEY (recurrence_id)
        REFERENCES recurrence_tasks (id)
        ON DELETE CASCADE
    ");
}

function version_2(PDO $pdo)
{
    $pdo->exec("
        CREATE TABLE recurrence_tasks (
            id SERIAL PRIMARY KEY
        )
     ");
    $pdo->exec("
     CREATE TABLE recurrence_tasks_select (
         recurrence_id INTEGER NOT NULL,
         type VARCHAR(16) NOT NULL,
         value VARCHAR(32) NOT NULL,
         PRIMARY KEY (recurrence_id),
         FOREIGN KEY(recurrence_id) REFERENCES recurrence_tasks(id) ON DELETE CASCADE
     )
  ");
    $pdo->exec("
      CREATE TABLE recurrence_tasks_meta (
      recurrence_id INTEGER NOT NULL,
      title TEXT DEFAULT NULL,
      description TEXT DEFAULT NULL,
      result TEXT DEFAULT NULL,
      PRIMARY KEY (recurrence_id),
      FOREIGN KEY(recurrence_id) REFERENCES recurrence_tasks(id) ON DELETE CASCADE
      )
");
}

function version_1(PDO $pdo)
{
    $pdo->exec("ALTER TABLE tasks ADD COLUMN result TEXT");
}
