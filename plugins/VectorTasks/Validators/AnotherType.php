<?php

namespace Kanboard\Plugin\VectorTasks\Validators;
use SimpleValidator\Validators\Base;
class AnotherType extends Base
{
    private $type;
    public function __construct($field, $error_message, $type)
    {
        parent::__construct($field, $error_message,$type);
        $this->type = $type;
    }

    public function execute(array $data)
    {
        if ($this->isFieldNotEmpty($data)) {
            switch ($data[$this->type]) {
                case 'link':
                    if (filter_var($data[$this->field], FILTER_VALIDATE_URL) === FALSE) {
                        return false; // Невалидный URL
                    }else return true;
                    break;
                
                default:
                    # code...
                    break;
            }
            // $length = mb_strlen($data[$this->field], 'UTF-8');
            // return $length <= $this->max;
        }

        return true;
    }
}
