<?php

namespace Kanboard\Plugin\SchemeDraw\Model;

use Kanboard\Core\Base;

class GroupHasOwnersGroupsModel extends Base {
    const TABLE = 'group_has_owners_groups';

    public function getChilds($groupId) {
        return $this->db->table(self::TABLE)
            ->eq('group_owner_id', $groupId)
            ->findOne();
    }

    public function getRoot($groupId) {
        return $this->db->table(self::TABLE)
            ->eq('group_id', $groupId)
            ->isNull('group_owner_id')
            ->findOne();
    }
}