<?php

namespace Kanboard\Plugin\SchemeDraw\Controller;
use Kanboard\Controller\BaseController;
use Kanboard\Plugin\RelatedGroups\Model\GroupRelatedModel;
use Kanboard\Plugin\RelatedGroups\Model\GroupModel;
use Kanboard\Model\UserModel;


/**
 * SchemeDraw Controller
 *
 * @package  Kanboard\Controller
 * @author   Frederic Guillot
 */
class SchemeDrawController extends BaseController
{
    /**
     * Dashboard overview
     *
     * @access public
     */
    public function show()
    {
        $user = $this->getUser();
        $groups = $this->groupRelatedModel->getRelatedGroups();
        $admin_list = $this->groupRelatedModel->getAdmins();
        $user_list = $this->groupRelatedModel->getExecutors();
        $users_info=$this->groupRelatedModel->getUsersInfo();
        $usersData = array();
        foreach ($users_info as $item) {
            $usersData[$item["id"]] = $item;
        }

        $users=array();
        foreach ($user_list as $member) {
            $groupId = $member["group_id"];
            $users[$groupId]["members"][] = ["id" => $member["id"]];
        }
        
        foreach ($admin_list as $admin) {
            $groupId = $admin["group_id"];
            $users[$groupId]["administrators"][] = ["id" => $admin["id"]];
        }

        $this->response->html($this->helper->layout->dashboard('dashboard/scheme', array(
            'plugin'             =>'SchemeDraw',
            'title'              => t('Dashboard for %s', $this->helper->user->getFullname($user)),
            'user'               => $user,
            'groups'             => $groups,
            'users_data'         =>$usersData,
            'users'              => $users,
        )));
    }
    public function show_group_create(array $values = array(), array $errors = array())
    {
        $exec_type_all_data=$this->groupRelatedModel->getExecTypes();
        foreach($exec_type_all_data as $el){
            $exec_type_all[]=$el['exec_type'];
        }
        $admin_levels_all_data=$this->groupRelatedModel->getAdminLevels();
        foreach($admin_levels_all_data as $el){
            $admin_levels_all[]=$el['admin_level'];
        }
        $this->response->html($this->template->render('group_creation_on_scheme/show', array(
            'group_id' =>$this->request->getIntegerParam('group_id'),
            'errors' => $errors,
            'values' => $values,
            'exec_type_all' => $exec_type_all,
            'admin_levels_all'=>$admin_levels_all,
        )));
    }

    /**
     * My tasks
     *
     * @access public
     */
    public function tasks()
    {
        $user = $this->getUser();

        $this->response->html($this->helper->layout->dashboard('dashboard/tasks', array(
            'title' => t('Tasks overview for %s', $this->helper->user->getFullname($user)),
            'paginator' => $this->taskPagination->getDashboardPaginator($user['id'], 'tasks', 50),
            'user' => $user,
        )));
    }

    /**
     * My subtasks
     *
     * @access public
     */
    public function subtasks()
    {
        $user = $this->getUser();

        $this->response->html($this->helper->layout->dashboard('dashboard/subtasks', array(
            'title' => t('Subtasks overview for %s', $this->helper->user->getFullname($user)),
            'paginator' => $this->subtaskPagination->getDashboardPaginator($user['id']),
            'user' => $user,
            'nb_subtasks' => $this->subtaskModel->countByAssigneeAndTaskStatus($user['id']),
        )));
    }

    /**
     * My projects
     *
     * @access public
     */
    public function projects()
    {
        $user = $this->getUser();

        $this->response->html($this->helper->layout->dashboard('dashboard/projects', array(
            'title' => t('Projects overview for %s', $this->helper->user->getFullname($user)),
            'paginator' => $this->projectPagination->getDashboardPaginator($user['id'], 'projects', 25),
            'user' => $user,
        )));
    }
}
