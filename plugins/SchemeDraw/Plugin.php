<?php

namespace Kanboard\Plugin\SchemeDraw;

use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Translator;

class Plugin extends Base
{
    public function initialize()
    {
        $this->template->hook->attach('template:dashboard:sidebar', 'SchemeDraw:dashboard/sidebar');
        $this->template->setTemplateOverride('dashboard/scheme', 'SchemeDraw:dashboard/scheme');
        $this->hook->on('template:layout:js', array('template' => 'plugins/SchemeDraw/Assets/js/schemedraw.js'));
        $this->template->setTemplateOverride('group_creation_on_scheme/show', 'SchemeDraw:group_creation_on_scheme/show');
    }

    public function onStartup()
    {
        Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getClasses() {
        return [
            'Plugin\SchemeDraw\Model' => [
                'GroupHasOwnersGroupsModel',
            ],
        ];
    }

    public function getPluginName()
    {
        return 'SchemeDraw';
    }

    public function getPluginDescription()
    {
        return t('Plugin for draw functional scheme');
    }

    public function getPluginAuthor()
    {
        return 'Michael';
    }

    public function getPluginVersion()
    {
        return '1.0.0';
    }

    public function getPluginHomepage()
    {
        return 'https://github.com/kanboard/plugin-myplugin';
    }
}

