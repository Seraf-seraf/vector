document.addEventListener("DOMContentLoaded", function() {
  const table = document.getElementById('output');
  table.addEventListener('mousedown', (e) => {
    isDragging = true;
    pixelsPassed = 0;
    startX = e.pageX - table.offsetLeft;
    startY = e.pageY - table.offsetTop; // Добавляем эту строку для вертикального перемещения
    scrollLeft = table.scrollLeft;
    scrollTop = table.scrollTop; // Добавляем эту строку для вертикального перемещения
    table.style.cursor='grabbing';
});
  
  table.addEventListener('mouseup', () => {
      isDragging = false;
      table.style.cursor='grab';
      

  });
  
  table.addEventListener('mouseleave', () => {
      isDragging = false;
      table.style.cursor='grab';

  });
  
  table.addEventListener('mousemove', (e) => {
    if (!isDragging) return;
    e.preventDefault();
    const x = e.pageX - table.offsetLeft;
    const y = e.pageY - table.offsetTop; // Добавляем эту строку для вертикального перемещения
    const walkX = (x - startX) * 0.7;
    const walkY = (y - startY) * 0.7; // Добавляем эту строку для вертикального перемещения
    table.scrollLeft = scrollLeft - walkX;
    table.scrollTop = scrollTop - walkY; // Добавляем эту строку для вертикального перемещения
    table.style.cursor='grabbing';

    // Вызывать draw только при прохождении порога пикселей
});
var uls=document.getElementsByClassName('ul');
Array.from(uls).forEach(element => {
  // console.log(element);
  var liElements = element.children;
  var wrapDiv = document.createElement('div');
  wrapDiv.style.flexDirection='column';
  wrapDiv.className='column-li';
  Array.from(liElements).forEach(lIelement => {
    var children=lIelement.children;
    var res = Array.from(children).some(childElement => {
      if (childElement.className === 'ul') {
        // выполните ваш код для элемента с классом 'ul'
        return true; // прервать выполнение цикла
      }
      return false; // продолжить цикл для остальных элементов
    });
    if (!res)
    wrapDiv.appendChild(lIelement);
  });
  element.appendChild(wrapDiv);

});

let isDragging = false;
let startX;
let scrollLeft;

var jsonData = document.getElementById("myData").getAttribute("data-array");
    // Преобразование JSON-строки в JavaScript-массив
var myArray = JSON.parse(jsonData);
function schemeDraw(){
  console.log('Выполни');
  const scrollX = window.pageXOffset || document.documentElement.scrollLeft;
  const scrollY = window.pageYOffset || document.documentElement.scrollTop;
  var canvas = document.getElementById('myCanvas');
  var ctx = canvas.getContext('2d');
  var canvasCords=canvas.getBoundingClientRect();
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  const rootGroups = myArray.filter(group => group.group_owner_id === null);
  rootGroups.forEach(rootGroup => {
      processGroup(rootGroup,ctx,scrollX,scrollY,canvasCords.left,canvasCords.top);
  });
}
schemeDraw();
function processGroup(group,ctx,scrollX,scrollY,dx,dy,indent = 0) {
    // Вывести информацию о группе (или выполнить другие действия)
    var from=document.getElementById((group.group_owner_id));
    var to=document.getElementById(group.group_id);
    if (from && to)
    {
      from=from.getBoundingClientRect();
      to=to.getBoundingClientRect();
      ctx.beginPath();  // Начинаем новый путь
      ctx.moveTo(to.left-10-scrollX-dx,from.bottom-20-scrollY-dy);  // Устанавливаем начальные координаты
      ctx.lineTo(to.left-10-scrollX-dx,(to.bottom+to.top)/2-scrollY-dy);
      ctx.lineTo(to.left+20-scrollX-dx, (to.bottom+to.top)/2-scrollY-dy);  // Устанавливаем конечные координаты
      ctx.stroke();  // Рисуем линию
      ctx.closePath();  // Закрываем пdуть
      
    }
    // Рекурсивно обойти подгруппы
    const subGroups = myArray.filter(subGroup => subGroup.group_owner_id === group.group_id);
    subGroups.forEach(subGroup => {
        processGroup(subGroup,ctx,scrollX,scrollY,dx,dy, indent + 1);
    });
}

// Вывод HTML-кода на веб-страницу (например, в элемент с id="output")

    // Теперь переменная myArray содержит массив в JavaScript
// console.log(myArray);
// var myElement = document.getElementById('card');
// // Получение координат с помощью getBoundingClientRect()
// var rect = myElement.getBoundingClientRect();
// console.log('Top: ' + rect.top);
// console.log('Left: ' + rect.left);
// console.log('Bottom: ' + rect.bottom);
// console.log('Right: ' + rect.right);
// console.log('Width: ' + rect.width);
// console.log('Height: ' + rect.height);
// var from = document.getElementById(5);
// var to = document.getElementById(7);
// console.log(from.getBoundingClientRect());
// console.log(to.getBoundingClientRect());
// var canvas = document.getElementById('myCanvas');
// var context = canvas.getContext('2d');
// var rect2 = myElement.getBoundingClientRect();
// console.log('Top: ' + rect.top);
// console.log('Left: ' + rect.left);
// console.log('Bottom: ' + rect.bottom);
// console.log('Right: ' + rect.right);
// console.log('Width: ' + rect.width);
// console.log('Height: ' + rect.height);
// var example = document.getElementById("myCanvas"),
// ctx     = example.getContext('2d');
// ctx.beginPath();  // Начинаем новый путь
// ctx.moveTo((rect.left+rect.right)/2,rect.bottom);  // Устанавливаем начальные координаты
// ctx.lineTo((rect2.left+rect2.right)/2, rect2.top);  // Устанавливаем конечные координаты
// ctx.stroke();  // Рисуем линию
// ctx.closePath();  // Закрываем путь
});
