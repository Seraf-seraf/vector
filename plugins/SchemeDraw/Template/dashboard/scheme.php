<style>
  .frame{
    overflow: hidden;
  }
    .li:not(:first-child) {
      /* padding-left:10px; */
      /* padding-top:20px; */
    }
    .ul:first-child{
      background:red;
    }
    .ul > .li:not(:first-child) {
  padding-left: 50px;
}
.ul > .li {
  padding-top: 30px;
  padding-left:20px;
}
.li .li:not(:first-child){
  /* padding-left:60px; */
}
.column-li{
  padding-top:30px;
}
.column-li > .li:not(:first-child) {
  padding-top:30px;
  /* padding-left:20px; */
}
.column-li{
  padding-left:40px;
}


    .ul > .li{
      /* padding-left:10px; */
    }
    body{
        overflow:hidden;
    }
    header{
        position: relative;
        z-index: 2;
        margin:0;
        padding-bottom:5px;
    }

    .sidebar{
        position: relative;
        z-index: 2;
    }
    .page-header{
        position: relative;
        z-index: 2;
        margin-bottom:0;
        padding-bottom:20px;
        background:white;
    }
    .column-li{
      padding-left:20px;
    }
    .column-li > .li{
      padding-left:0;
    }
  #id-1{
    display:flex;
    flex-direction:row !important;
  }
  #id-1 > li:not(:first-child){
    padding-left:40px;
  }
  #id-0{
    width:fit-content;
    padding-top:0 !important;
  }
  #id-0 > li >.card{
    width: 100% !important;
  }
  .card{
    position: relative;
    z-index: 2;
  }
  #my-list{
    height:2000px;
  }
</style>
<div>
<?php
$inputArray = $groups;
function buildTree($array, $users, $users_data, $avatar, $modal, $url, $parentId = null, &$count = 0)
{
    if ($count == 0) {
        $tree = '<div id="my-list" style="display:flex;user-select: none;">';
    } else {
        $tree = '<div class="ul" style="display:flex;">';
    }

    $j = 0;
    foreach ($array as $item) {
        if ($item['group_owner_id'] == $parentId) {
            $j++;
            $count++;
            $tree .= '<div class="li">';
            $tree .= '<div id=' . $item['group_id'] . ' class="card" id="card" style="min-width:200px;min-height:100px;background:#6293ef;border-radius:10px;padding:5px;display:flex;flex-direction:column;justify-content:space-between;">';
            $tree .= '<div>';
            $tree .= '<h1 style="color:white;font-size:14px;"><B>' . $item['name'] . ' </B></h1>';
            $tree .= '<div class="dropdown" style="position:absolute;right:5px;top:5px;">';
            $tree .= '<a href="#" class="dropdown-menu dropdown-menu-link-icon"><strong><i class="fa fa-caret-down"></i></strong></a>';
            $tree .= '<ul><li>';
            $tree .= $modal->medium('edit', t('Edit'), 'GroupListController', 'show_edit_group', array('plugin' => 'RelatedGroups', 'group_id' => $item['group_id']));
            $tree .= '</li><li>';
            $tree .= $url->link('Участники', 'GroupListController', 'users', array('group_id' => $item['group_id'], 'plugin' => 'RelatedGroups'));
            $tree .= '</li><li>';
            $tree .= $modal->medium('refresh fa-rotate-90', 'Повторяющиеся задачи', 'VectorTasksController', 'showRecurrenceTaskMenu', array('plugin' => 'VectorTasks', 'group_id' => $item['group_id']));
            $tree .= '</li><li>';
            $tree .= $modal->medium('user-plus', t('New group'), 'SchemeDrawController', 'show_group_create', array('plugin' => 'SchemeDraw', 'group_id' => $item['group_id']));
            $tree .= '</li><li>';
            $tree .= $modal->confirm('trash-o', t('Remove'), 'GroupListController', 'confirm', array('group_id' => $item['group_id']));
            '</li></ul>';
            $tree .= '</div>';
            if ($item['result']) {
                $tree .= '<div>';
                $tree .= '<p style="color:white;font-size:12px;padding-top:3px;">Ожидаемый результат</p>';
                $tree .= '<p style="background: gainsboro;padding: 3px;border-radius: 5px;font-size:12px;padding-top:3px;">' . $item['result'] . '</p>';
                $tree .= '</div>';
            }
            $tree .= '</div><div>';
            if (isset($users[$item['group_id']]['administrators'])) {
                $tree .= '<span style="color:white;font-size:13px;padding-top:3px;">Администратор:</span>';
                foreach ($users[$item['group_id']]['administrators'] as $admin) {
                    $tree .= '<p><span>' . $avatar->small(
                        $users_data[$admin['id']]['id'],
                        $users_data[$admin['id']]['username'],
                        $users_data[$admin['id']]['name'],
                        $users_data[$admin['id']]['email'],
                        $users_data[$admin['id']]['avatar_path'],
                        'avatar-inline'
                    ) . '<span>';
                    $tree .= '<span style="color:white;font-size:13px;padding-top:3px;">' . $url->link($users_data[$admin['id']]['name'] ?: $users_data[$admin['id']]['username'], 'UserViewController', 'show', array('user_id' => $admin['id'])) . '</span></p>';
                }
            }
            if (isset($users[$item['group_id']]['members'])) {
                $tree .= '<span style="color:white;font-size:13px;padding-top:3px;">Исполнители:</span>';
                foreach ($users[$item['group_id']]['members'] as $member) {
                    $tree .= '<p><span>' . $avatar->small(
                        $users_data[$member['id']]['id'],
                        $users_data[$member['id']]['username'],
                        $users_data[$member['id']]['name'],
                        $users_data[$member['id']]['email'],
                        $users_data[$member['id']]['avatar_path'],
                        'avatar-inline'
                    ) . '<span>';
                    $tree .= '<span style="color:white;font-size:13px;padding-top:3px;">' . $url->link($users_data[$member['id']]['name'] ?: $users_data[$member['id']]['username'], 'UserViewingController', 'show', array('user_id' => $member['id'], 'plugin' => 'UserProfileEdit')) . '</span></p>';
                }
            }
            $tree .= '</div></div>';
            $tree .= buildTree($array, $users, $users_data, $avatar, $modal, $url, $item['group_id'], $count);
            $tree .= '</div>';
        }
    }
    if ($j == 0) {
        return;
    }

    $tree .= '</div>';

    return $tree;
}
// Выводим результат
$jsonArray = json_encode($groups);
?>
<div id="myData" data-array="<?php echo htmlspecialchars($jsonArray); ?>"></div>
</div>
<div class="frame" id='output' style='background-color:#7ddbe7;width:100%;height:100vh;border-radius:10px;padding:20px;position:relative'>
<?=buildTree($groups, $users, $users_data, $this->avatar, $this->modal, $this->url);?>
<canvas id="myCanvas" width="12000" height='6000' style="position:absolute;top:0;left:0"></canvas>
</div>