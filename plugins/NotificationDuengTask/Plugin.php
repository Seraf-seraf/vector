<?php

namespace Kanboard\Plugin\NotificationDuengTask;
use Kanboard\Core\Plugin\Base as PluginBase;
use Kanboard\Core\Base;
use Kanboard\Core\Translator;
use Kanboard\Model\TaskFinderModel;
use Kanboard\Model\TaskModel;
use Kanboard\Plugin\VectorTasks\Model\RecurrenceTaskModel;

use Kanboard\Plugin\NotificationDuengTask\Action\NotificationDuengTask;
use Kanboard\Plugin\NotificationDuengTask\Action\NotificationAdmins;
use DateTimeImmutable;

use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram as TelegramClass;
use Longman\TelegramBot\Exception\TelegramException;


class Plugin extends PluginBase
{
    public function initialize()
    {
        // date_default_timezone_set($this->timezoneModel->getCurrentTimezone());
        // $datt = date_default_timezone_get();
        //$hours = date('H');
        //$minutes = date('i');

        //var_dump($this->projectModel->getById(1));

        // $task = $this->taskFinderModel->getById(47);
        // //var_dump($task);
        // foreach ($task as $key => $value) {
        //     echo $key. ': '; var_dump($value); echo '<br>';
        // }
        // $datedue = new DateTimeImmutable(date('Y-m-d', $task['date_due']));
        // var_dump($datedue);
        //$user = $this->userModel->getById(8);
        //$this->notifyUser($user);
        // // //var_dump($datt);
        // var_dump($hours); var_dump($minutes);


        // date_default_timezone_set($this->timezoneModel->getCurrentTimezone());
        // $tasks = $this->taskFinderModel->getAll(1);
        // $date = new DateTimeImmutable(date('Y-m-d'));
        // var_dump($date->format('Y-m-d'));
        // $percent = '25%';

        // if (date('H') == $hours && date('i') == $minutes) {
        // foreach ($tasks as $key => $value) {
        //     //var_dump($value);
        //     if(!empty($value['date_due']) && ($value['column_id'] != 4 || $value['column_id'] != 5)){
        //         echo '<br>'; var_dump($value); echo '<br>';
        //         $user = $this->userModel->getById($value['owner_id']);
        //         $datecreate = new DateTimeImmutable(date('Y-m-d', $value['date_creation']));
        //         $datedue = new DateTimeImmutable(date('Y-m-d', $value['date_due']));
        //         var_dump($datedue); echo '<br>';
        //         $daysall = $datecreate->diff($datedue)->d;
        //         $daysleft = $date->diff($datedue)->d;
        //         $subject = 'Задача: ' . $value['title'] . '. Дней до истечения срока: ' . $daysleft;
        //         $daysgone = $date->diff($datecreate)->d;
        //         $daypercent = floor($daysall * (25 / 100));
        //         echo 'Всего дней: ' . $daysall . '<br>'; 
        //         // echo 'Осталось дней: ' . $daysleft . '<br>';
        //         // echo 'Прошло дней: ' . $daysgone . '<br>';  
        //         echo 'Процент дней: ' . $daypercent . '<br>'; 
        //         //echo 'Процент дней 2: ' . $daysall * (25 / 100) . '<br>'; 
        //         //var_dump($datecreate);
        //         if ($date > $datedue) {
        //             continue;
        //         }

        //         switch ($percent) {
        //             case '25%':
        //                 $percent = 25;
        //                 $percent2 = 50;
        //                 $percent3 = 75;
        //                 $daysleft = 6;

        //                 if ($daysleft == floor($daysall * ($percent3 / 100))) {
        //                     echo $subject;
        //                     //$results[] = $this->sendEmail($subject, $value, $user);
        //                 }elseif ($daysleft == floor($daysall * ($percent2 / 100))) {
        //                     echo $subject;
        //                     //$results[] = $this->sendEmail($subject, $value, $user);
        //                 }elseif ($daysleft == floor($daysall * ($percent / 100))) {
        //                     //$results[] = $this->sendEmail($subject, $value, $user);
        //                     echo $subject . '<br>';
        //                 }
        //                 break;
        //             case '30%':
        //                 $percent = 30;
        //                 $percent2 = 60;
        //                 $percent3 = 90;

        //                 if ($daysleft == floor($daysall * ($percent3 / 100))) {
        //                     echo $subject;
        //                     //$results[] = $this->sendEmail($subject, $value, $user);
        //                 }elseif ($daysleft == floor($daysall * ($percent2 / 100))) {
        //                     //$results[] = $this->sendEmail($subject, $value, $user);
        //                 }elseif ($daysleft == floor($daysall * ($percent / 100))) {
        //                     echo $subject;
        //                     //$results[] = $this->sendEmail($subject, $value, $user);
        //                 }
        //                 break;
        //             case '50%':
        //                 $percent = 50;
        //                 $daysleft = 13;

        //                 if ($daysleft == floor($daysall * ($percent / 100))) {
        //                     echo $subject;
        //                     //$results[] = $this->sendEmail($subject, $value, $user);
        //                 }
        //                 break;
        //             default:
        //                 # code...
        //                 break;
        //         }
        //     }else {
        //         echo 'net';
        //         //continue;
        //     }
        //     break;
            
        // }
        // }

        //$this->logger->debug(__METHOD__.' Plugin started... ');
        $this->template->setTemplateOverride('notification/task_dueng','NotificationDuengTask:notification/task_dueng');
        $this->actionManager->register(new NotificationDuengTask($this->container));
        //$this->actionManager->register(new NotificationAdmins($this->container));
    }
    public function notifyUser(array $user)
    {
        $this->logger->debug(__METHOD__.' Get user... ');
        $apikey = $this->userMetadataModel->get($user['id'], 'telegram_apikey', $this->configModel->get('telegram_apikey'));
        $bot_username = $this->userMetadataModel->get($user['id'], 'telegram_username', $this->configModel->get('telegram_username'));
        $chat_id = $this->userMetadataModel->get($user['id'], 'telegram_user_cid');
        $forward_attachments = $this->userMetadataModel->get($user['id'], 'forward_attachments', $this->configModel->get('forward_attachments'));
        $telegram_proxy = $this->userMetadataModel->get($user['id'], 'telegram_proxy', $this->configModel->get('telegram_proxy'));
        
        if (! empty($apikey)) 
        {
            //$project = $this->projectModel->getById($eventData['task']['project_id']);
            $this->sendMessage($apikey, $bot_username, $forward_attachments, $telegram_proxy, $chat_id);
        }
    }
    /**
     * Send message to Telegram
     *
     * @access protected
     * @param  string    $apikey
     * @param  string    $bot_username
     * @param  string    $chat_id
     * @param  array     $project
     * @param  string    $eventName
     * @param  array     $eventData
     */
    protected function sendMessage($apikey, $bot_username, $forward_attachments, $telegram_proxy, $chat_id)
    {
    
        // Get required data
        
        $daysleft = 22;
        $proj_name = 'Vector';
        $task_title = 'TastTask 123';
        $title = 'Задача: '. $task_title . ' Дней до истечения срока: '. $daysleft;
        $date_created = 'Создана: 02/03/2024 12:41';
        $date_due = 'Дата завершения: 02/29/2024 00:00';
        $inspector = 'Проверяющий: не назначен';
        $column = 'Колонка на доске: 2';
        $position = 'Позиция задачи: 1';

        
        
        // Build message
        
        $message = "[".htmlspecialchars($proj_name, ENT_NOQUOTES | ENT_IGNORE)."]\n";
        $message .= htmlspecialchars($title, ENT_NOQUOTES | ENT_IGNORE)."\n";
        $message .= htmlspecialchars($task_title, ENT_NOQUOTES | ENT_IGNORE)."\n";
        $message .= htmlspecialchars($date_created, ENT_NOQUOTES | ENT_IGNORE)."\n";
        $message .= htmlspecialchars($date_due, ENT_NOQUOTES | ENT_IGNORE)."\n";
        $message .= htmlspecialchars($inspector, ENT_NOQUOTES | ENT_IGNORE)."\n";
        $message .= htmlspecialchars($column, ENT_NOQUOTES | ENT_IGNORE)."\n";
        $message .= htmlspecialchars($position, ENT_NOQUOTES | ENT_IGNORE)."\n";
        
        // Send Message
        $this->logger->debug(__METHOD__.' Send Message... ');
        try
        {   
            $this->logger->debug(__METHOD__.' Try send Message... ');
            // Create Telegram API object
            $telegram = new TelegramClass($apikey, $bot_username);
            $this->logger->debug(__METHOD__.' Get Telegram API... ');
            //var_dump($telegram);
            // Setup proxy details if set in kanboard configuration
            if ($telegram_proxy == '')
	        {
                $this->logger->debug(__METHOD__.' Request to send... ');
                Request::setClient(new \GuzzleHttp\Client([
	                    'base_uri' => 'https://api.telegram.org',
                        'timeout' => 20,
                        'verify' => false,
                        'proxy'   => $telegram_proxy,
                ]));
                
            }

            // Message pay load
            $data = array('chat_id' => $chat_id, 'text' => $message, 'parse_mode' => 'HTML');
            
            // Send message
            $result = Request::sendMessage($data);
            $this->logger->debug(__METHOD__.' Message Sended ');
            
        }
        catch (TelegramException $e)
        {
            // log telegram errors
            error_log($e->getMessage());
        }
    }


    public function onStartup()
    {
        //Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getPluginName()
    {
        return 'NotificationDuengTask';
    }

    public function getPluginDescription()
    {
        return t('Notifies the performer when a task begins to due');
    }

    public function getPluginAuthor()
    {
        return 'Rzer_1';
    }

    public function getPluginVersion()
    {
        return '1.0.0';
    }

    public function getPluginHomepage()
    {
        return 'https://github.com/kanboard/';
    }
    public function getClasses()
    {
        return [
            'Plugin\VectorTasks\Model' => [
                'TaskCreationModel',
                'VectorTaskFileModel',
                'RecurrenceTaskModel',
            ],
            'Plugin\RelatedGroups\Model' => [
                'GroupMemberModel','GroupModel','GroupRelatedModel',
            ],
        ];
    }
}

