<?php

namespace Kanboard\Plugin\NotificationDuengTask\Action;

use Kanboard\Model\TaskModel;
use Kanboard\Action\Base;
use Kanboard\Core\Translator;

use DateTimeImmutable;

use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram as TelegramClass;
use Longman\TelegramBot\Exception\TelegramException;

/**
 * Send notification to superiors when due date is expired
 *
 * @package Kanboard\Plugin\NotificationDuengTask\Action
 * @author  Rzer_1
 */
class NotificationDuengTask extends Base
{
    /**
     * Get action description
     *
     * @access public
     * @return string
     */
    public function getDescription()
    {
        date_default_timezone_set($this->timezoneModel->getCurrentTimezone());
        $h = date('H');
        $i = date('i');
        $timezone = date_default_timezone_get();
        return t('Notifies performer that a task is about to expire. Server Time: ' . $h . ':' . $i . ' - '. $timezone);
    }

    /**
     * Get the list of compatible events
     *
     * @access public
     * @return array
     */
    public function getCompatibleEvents()
    {
        return array(
            TaskModel::EVENT_UPDATE,
            TaskModel::EVENT_DAILY_CRONJOB,
        );
    }

    /**
     * Get the required parameter for the action
     *
     * @access public
     * @return array
     */
    public function getActionRequiredParameters()
    {
        return array(
            'percent' => array(
                '25%' => '25%',
                '30%' => '30%',
                '50%' => '50%'
            ),
            'hours' => array('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23'),
            'minutes' => array(
                '00',
                '10',
                '20',
                '30',
                '40',
                '50'
            ),
        );
    }

    /**
     * Get all tasks
     *
     * @access public
     * @return array
     */

    public function getEventRequiredParameters()
    {
        return array(
            'project_id',
        );
    }

    /**
     * Check if the event data meet the action condition
     *
     * @access public
     * @param  array   $data   Event data dictionary
     * @return bool
     */
    public function hasRequiredCondition(array $data)
    {
        return true;
        //return date('D') == $this->getParam('day_to_create_task');
        //return $data['task']['column_id'] == $this->getParam('column_id');
        //return ! empty($data['task_id']);
        //return count($data['task']) > 0;
        //return;
    }

    public function doAction(array $data)
    {
        date_default_timezone_set($this->timezoneModel->getCurrentTimezone());
        $results = array();
        $percent = $this->getParam('percent');
        $hours = $this->getParam('hours');
        $minutes = $this->getParam('minutes');
        $tasks = $this->taskFinderModel->getAll(1);
        $date = new DateTimeImmutable(date('Y-m-d'));
        // $hours = '01';
        // $minutes = '41';
        // $hours = date('H');
        // $minutes = date('i');
        // $percent = '25%';

        if (date('H') == $hours && date('i') == $minutes) {
            foreach ($tasks as $key => $value) {
                if(!empty($value['date_due']) && ($value['column_id'] != 4 || $value['column_id'] != 5)){
                    $user = $this->userModel->getById($value['owner_id']);
                    //$user = $this->userModel->getById(8); //тестовый юзер
                    $datecreate = new DateTimeImmutable(date('Y-m-d', $value['date_creation']));
                    $datedue = new DateTimeImmutable(date('Y-m-d', $value['date_due']));
                    $daysall = $datecreate->diff($datedue)->d;
                    $daysleft = $date->diff($datedue)->d;
                    $subject = 'Задача: ' . $value['title'] . '. Дней до истечения срока: ' . $daysleft;
                    $daysgone = $date->diff($datecreate)->d;

                    if ($date > $datedue) {
                        continue;
                    }
                    //запись данных для передачи в сообщения уведомлений
                    $creator = $this->userModel->getById($value['creator_id']);
                    if ($creator['name'] == null){
                        $creator_name = $creator['username'];
                    }else {
                        $creator_name = $creator['name'];
                    }
                    //$creator_link = 'https://vk.acidhead.ru/?controller=UserViewingController&action=show&user_id='.$value['creator_id'].'&plugin=UserProfileEdit';
                    $task_link = 'https://vk.acidhead.ru/?controller=ActivityController&action=task&task_id='.$value['id'];
                    $proj_name = $this->projectModel->getById(1);
                    $column = $this->columnModel->getById($value['column_id']);
                    $messagedata = array(
                        'proj_name' => $proj_name['name'],
                        'task' => $value,
                        'task_link' => $task_link,
                        'user' => $user,
                        'subject' => $subject,
                        'datecreate' => $datecreate->format('Y-m-d'),
                        'daysleft' => $daysleft,
                        'datedue' => $datedue->format('Y-m-d'),
                        'column' => $column['title'],
                        'creator_name' => $creator_name,
                    );
                    if($value['owner_ms'] != 0){
                        $ownerms = $this->userModel->getById($value['owner_ms']);
                        $messagedata['owner_ms_username'] = $ownerms['username'];
                        $messagedata['owner_ms_name'] = $ownerms['name'];
                    }

                    switch ($percent) {
                        case '25%':
                            $percent = 25;
                            $percent2 = 50;
                            $percent3 = 75;
                            //$daysleft = 6;

                            if ($daysleft == 0){
                                $results[] = $this->sendNotification($messagedata);
                            }elseif ($daysleft == floor($daysall * ($percent3 / 100))) {
                                $results[] = $this->sendNotification($messagedata);
                            }elseif ($daysleft == floor($daysall * ($percent2 / 100))) {
                                $results[] = $this->sendNotification($messagedata);
                            }elseif ($daysleft == floor($daysall * ($percent / 100))) {
                                $results[] = $this->sendNotification($messagedata);
                            }
                            break;
                        case '30%':
                            $percent = 30;
                            $percent2 = 60;
                            $percent3 = 90;
    
                            if ($daysleft == 0){
                                $results[] = $this->sendNotification($messagedata);
                            }elseif ($daysleft == floor($daysall * ($percent3 / 100))) {
                                $results[] = $this->sendNotification($messagedata);
                            }elseif ($daysleft == floor($daysall * ($percent2 / 100))) {
                                $results[] = $this->sendNotification($messagedata);
                            }elseif ($daysleft == floor($daysall * ($percent / 100))) {
                                $results[] = $this->sendNotification($messagedata);
                            }
                            break;
                        case '50%':
                            $percent = 50;
    
                            if ($daysleft == 0){
                                $results[] = $this->sendNotification($messagedata);
                            }elseif ($daysleft == floor($daysall * ($percent / 100))) {
                                $results[] = $this->sendNotification($messagedata);
                            }
                            break;
                        default:
                            # code...
                            break;
                    }
                }else {
                    continue;
                }
            }
            return in_array(true, $results, true);
            //return true;
        }else {
            return false;
        }
    }

    private function sendNotification($messagedata){
        $this->sendEmail($messagedata);
        $this->sendTelegram($messagedata);

        return true;
    }
    
    private function sendEmail($messagedata){

        $this->emailClient->send(
            $messagedata['user']['email'],
            $messagedata['user']['name'] ?: $messagedata['user']['username'],
            $messagedata['subject'],
            $this->template->render('notification/task_dueng', array('task' => $messagedata['task']))
            );
    }
    private function sendTelegram($messagedata)
    {
        $apikey = $this->userMetadataModel->get($messagedata['user']['id'], 'telegram_apikey', $this->configModel->get('telegram_apikey'));
        $bot_username = $this->userMetadataModel->get($messagedata['user']['id'], 'telegram_username', $this->configModel->get('telegram_username'));
        $chat_id = $this->userMetadataModel->get($messagedata['user']['id'], 'telegram_user_cid');
        $forward_attachments = $this->userMetadataModel->get($messagedata['user']['id'], 'forward_attachments', $this->configModel->get('forward_attachments'));
        $telegram_proxy = $this->userMetadataModel->get($messagedata['user']['id'], 'telegram_proxy', $this->configModel->get('telegram_proxy'));
    
        // Get required data
        
        $daysleft = $messagedata['daysleft'];
        $proj_name = $messagedata['proj_name'];
        $task_title = $messagedata['task']['title'];
        //$title = 'Задача: '. $task_title . ' Дней до истечения срока: '. $daysleft;
        $date_created = 'Создана: '.$messagedata['datecreate'];
        $date_due = 'Завершается: '.$messagedata['datedue'];
        $column = 'Колонка на доске: '.$messagedata['column'];
        $position = 'Позиция задачи: '.$messagedata['task']['position'];
        $creator = 'Создатель: '.$messagedata['creator_name'];

        // Build message
        
        $message = "[".htmlspecialchars($proj_name, ENT_NOQUOTES | ENT_IGNORE)."]\n";
        $message .= 'Задача: <a href="'.$messagedata['task_link'].'">'.$task_title.'</a> Дней до истечения срока: '. $daysleft."\n";
        //$message .= htmlspecialchars($title, ENT_NOQUOTES | ENT_IGNORE)."\n";
        $message .= htmlspecialchars($date_created, ENT_NOQUOTES | ENT_IGNORE)."\n";
        $message .= htmlspecialchars($date_due, ENT_NOQUOTES | ENT_IGNORE)."\n";
        if ($messagedata['owner_ms_name'] != 0) {
            $inspector = 'Проверяющий: '.$messagedata['owner_ms_name'];
        }else {
            $inspector = 'Проверяющий: не назначен';
        }
        $message .= htmlspecialchars($inspector, ENT_NOQUOTES | ENT_IGNORE)."\n";
        $message .= htmlspecialchars($column, ENT_NOQUOTES | ENT_IGNORE)."\n";
        $message .= htmlspecialchars($position, ENT_NOQUOTES | ENT_IGNORE)."\n";
        $message .= htmlspecialchars($creator, ENT_NOQUOTES | ENT_IGNORE)."\n";

        //$message .= 'Назначено: <a href="'.$messagedata['creator_link'].'">'.$messagedata['creator_name'].'</a>';
        
        // Send Message
        try
        {   
            // Create Telegram API object
            $telegram = new TelegramClass($apikey, $bot_username);
            //var_dump($telegram);
            // Setup proxy details if set in kanboard configuration
            if ($telegram_proxy == '')
	        {
                Request::setClient(new \GuzzleHttp\Client([
	                    'base_uri' => 'https://api.telegram.org',
                        'timeout' => 20,
                        'verify' => false,
                        'proxy'   => $telegram_proxy,
                ]));
                
            }

            // Message pay load
            $data = array('chat_id' => $chat_id, 'text' => $message, 'parse_mode' => 'HTML');
            
            // Send message
            $result = Request::sendMessage($data);
            //$this->logger->debug(__METHOD__.' Message Sended ');
            
        }
        catch (TelegramException $e)
        {
            // log telegram errors
            error_log($e->getMessage());
        }
    }
}