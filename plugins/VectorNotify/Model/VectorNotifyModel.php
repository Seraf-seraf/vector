<?php

namespace Kanboard\Plugin\VectorNotify\Model;

use Kanboard\Core\Base;

use DateTimeImmutable;

use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram as TelegramClass;
use Longman\TelegramBot\Exception\TelegramException;

/**
 * VectorNotify Model
 *
 * @package  Kanboard\Plugin\VectorNotify\
 * @author   Author
 */
class VectorNotifyModel extends Base
{
    /**
     * Send email and telegram notifications
     *
     * @access public
     * @param  integer $task_id
     * @param  integer $user_id
     * @param  string $title
     * @param  string $message
     */
    public function sendNotifications($task_id = null, $user_id, $title, $message = '', $emailsend = true, $telegramsend = true) {

        date_default_timezone_set($this->timezoneModel->getCurrentTimezone());
        $user = $this->userModel->getById($user_id);
        $proj_name = $this->projectModel->getById(1);

        $messagedata = array(
            'user' => $user,
            'proj_name' => $proj_name['name'],
            'title' => $title,
            'message' => $message,
        );
        if ($task_id != null) { 
            $task = $this->taskFinderModel->getById($task_id);
            $messagedata['task'] = $task;
            //$messagedata['task_link'] = $this->url->absoluteLink(t($this->text->e($task['title']) . '(#' . $task['id'] . ')'), 'TaskViewController', 'show', array('task_id' => $task['id']));
            $messagedata['task_link'] = 'http://vektor.acidhead.ru/?controller=ActivityController&action=task&task_id='.$task_id;
            $date_creation = new DateTimeImmutable(date('Y-m-d', $task['date_creation']));
            $messagedata['date_creation'] =  $date_creation->format('Y-m-d');
            if ($task['date_due']) {
                $date_due = new DateTimeImmutable(date('Y-m-d', $task['date_due']));
                $messagedata['date_due'] = $date_due->format('Y-m-d');
            }
            $messagedata['column'] = $this->columnModel->getById($task['column_id']);
            if ($task['owner_id'] != 0) {
                $messagedata['owner'] = $this->userModel->getById($task['owner_id']);
            }
            if ($task['controller_id'] != 0) {
                $messagedata['controller'] = $this->userModel->getById($task['controller_id']);
            }
        }
        if ($emailsend === true) {
            $this->sendEmail($messagedata);
        }
        if ($telegramsend === true) {
            $this->sendTelegram($messagedata);
        }
        
        return;
    }

    private function sendEmail($messagedata){

        //$this->logger->debug(__METHOD__.' Model start message... ' . $messagedata['user']['email'] . $messagedata['user']['name'] . $messagedata['title']);
        $this->emailClient->send(
            $messagedata['user']['email'],
            $messagedata['user']['name'],
            $messagedata['title'],
            $this->template->render('vector_notifications/message', array('task' => $messagedata['task'], 'messagedata' => $messagedata))
        );
        return;
    }
    private function sendTelegram($messagedata)
    {
        $apikey = $this->userMetadataModel->get($messagedata['user']['id'], 'telegram_apikey', $this->configModel->get('telegram_apikey'));
        $bot_username = $this->userMetadataModel->get($messagedata['user']['id'], 'telegram_username', $this->configModel->get('telegram_username'));
        $chat_id = $this->userMetadataModel->get($messagedata['user']['id'], 'telegram_user_cid');
        $forward_attachments = $this->userMetadataModel->get($messagedata['user']['id'], 'forward_attachments', $this->configModel->get('forward_attachments'));
        $telegram_proxy = $this->userMetadataModel->get($messagedata['user']['id'], 'telegram_proxy', $this->configModel->get('telegram_proxy'));
        
        // Get required data
        
        $proj_name = $messagedata['proj_name'];
        $title = $messagedata['title'];
        $message1 = $messagedata['message'];

        // Build message
        
        $message = "<strong>[".$proj_name."] ".$title."</strong>" . "\n";
        $message .= $message1 ."\n";
        
        if (isset($messagedata['task'])) {
            $message .= 'Задача: <a href="'.$messagedata['task_link'].'">'.$messagedata['task']['title']. '(#'.$messagedata['task']['id'].')'.'</a>'."\n";
            $date_creation = 'Создана: '.$messagedata['date_creation'];
            if ($messagedata['date_due']) {
                $date_due = 'Создана: '.$messagedata['date_due'];
            }
            $owner = 'Исполнитель: '.$messagedata['owner']['name'];
            $controller = 'Контролирующий: '.$messagedata['controller']['name'];
            $column = 'Колонка на доске: '.$messagedata['column']['title'];
            $position = 'Позиция задачи: '.$messagedata['task']['position'];

            $message .= htmlspecialchars($date_creation, ENT_NOQUOTES | ENT_IGNORE)."\n";
            if ($messagedata['date_due']) {
                $message .= htmlspecialchars($date_due, ENT_NOQUOTES | ENT_IGNORE)."\n";
            }
            $message .= htmlspecialchars($owner, ENT_NOQUOTES | ENT_IGNORE)."\n";
            $message .= htmlspecialchars($controller, ENT_NOQUOTES | ENT_IGNORE)."\n";
            $message .= htmlspecialchars($column, ENT_NOQUOTES | ENT_IGNORE)."\n";
            $message .= htmlspecialchars($position, ENT_NOQUOTES | ENT_IGNORE)."\n";
        }

        //$message .= htmlspecialchars($message, ENT_NOQUOTES | ENT_IGNORE)."\n";

        //$message .= 'Назначено: <a href="'.$messagedata['creator_link'].'">'.$messagedata['creator_name'].'</a>';
        
        // Send Message
        try
        {   
            // Create Telegram API object
            $telegram = new TelegramClass($apikey, $bot_username);
            //var_dump($telegram);
            // Setup proxy details if set in kanboard configuration
            if ($telegram_proxy == '')
	        {
                Request::setClient(new \GuzzleHttp\Client([
	                    'base_uri' => 'https://api.telegram.org',
                        'timeout' => 20,
                        'verify' => false,
                        'proxy'   => $telegram_proxy,
                ]));
                
            }

            // Message pay load
            $data = array('chat_id' => $chat_id, 'text' => $message, 'parse_mode' => 'HTML');
            
            // Send message
            $result = Request::sendMessage($data);
            //$this->logger->debug(__METHOD__.' Message Sended ');
            
        }
        catch (TelegramException $e)
        {
            // log telegram errors
            error_log($e->getMessage());
        }
        return;
    }
}