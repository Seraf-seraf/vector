<html>
<body>
<h2><?= $this->text->e('['. $messagedata['proj_name']) . '] ' ?><?= $this->text->e($messagedata['title']) ?></h2>

<p><?= $this->text->e($messagedata['message']) ?></p>

<ul>
    <?php if (isset($task['id'])): ?>
        <li>
            <?= t('Задача:').' '.$this->url->absoluteLink(t($this->text->e($task['title']) . '(#' . $task['id'] . ')'), 'TaskViewController', 'show', array('task_id' => $task['id'])) ?>
        </li>
        <li>
            <?= t('Created:').' '.$this->dt->datetime($task['date_creation']) ?>
        </li>
        <?php if ($task['date_due']): ?>
        <li>
            <?= t('Due date:').' '.$this->dt->datetime($task['date_due']) ?>
        </li>
        <?php endif ?>
        <?php if (! empty($messagedata['owner'])): ?>
        <li>
            <?= t('Исполнитель:').' '.$messagedata['owner']['name'] ?: $messagedata['owner']['username'] ?>
        </li>
        <?php endif ?>
        <li>

            <?php if (! empty($messagedata['controller'])): ?>
                <?= t('Проверяющий:').' '.$messagedata['controller']['name'] ?: $messagedata['controller']['username'] ?>
            <?php else: ?>
                <?= t('There is nobody assigned') ?>
            <?php endif ?>
        </li>
        <li>
            <?= t('Column on the board:').' '.$messagedata['column']['title'] ?>
        </li>
        <li><?= t('Task position:').' '.$this->text->e($task['position']) ?></li>
        <?php if (! empty($task['category_name'])): ?>
        <li>
            <?= t('Category:') ?> <strong><?= $this->text->e($task['category_name']) ?></strong>
        </li>
        <?php endif ?>
    <?php endif ?>
</ul>

<?php if (! empty($task['description'])): ?>
    <h2><?= t('Description') ?></h2>
    <?= $this->text->markdown($task['description'], true) ?>
<?php endif ?>

</body>
</html>