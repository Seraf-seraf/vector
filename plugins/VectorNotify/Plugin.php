<?php

namespace Kanboard\Plugin\VectorNotify;
use Kanboard\Core\Plugin\Base as PluginBase;
use Kanboard\Core\Base;
use Kanboard\Core\Translator;


class Plugin extends PluginBase
{
    public function initialize()
    {
        $this->template->setTemplateOverride('vector_notifications/message','VectorNotify:vector_notifications/message');
        //$this->actionManager->register(new NotificationCompleteTask($this->container));
    }
    public function onStartup()
    {
        //Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getPluginName()
    {
        return 'VectorNotify';
    }

    public function getPluginDescription()
    {
        return t('Plugin that adds various notifications for the Vector application');
    }

    public function getPluginAuthor()
    {
        return 'Rzer_1';
    }

    public function getPluginVersion()
    {
        return '1.0.0';
    }

    public function getPluginHomepage()
    {
        return 'https://github.com/kanboard/';
    }
    public function getClasses()
    {
        return [
            'Plugin\VectorNotify\Model' => [
                'VectorNotifyModel',
            ],
        ];
    }
}

