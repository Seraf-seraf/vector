<?php

namespace Kanboard\Plugin\UserProfileEdit\Schema;

use PDO;

const VERSION = 5;

function version_5(PDO $pdo) {
    $pdo->exec("ALTER TABLE users ALTER COLUMN location TYPE TEXT");
}

function version_4(PDO $pdo) {
    $pdo->exec("ALTER TABLE users ALTER COLUMN city TYPE TEXT");
}

function version_3(PDO $pdo)
{
    $pdo->exec("ALTER TABLE users ADD COLUMN location CHAR(63) DEFAULT ' '");
}

function version_2(PDO $pdo)
{
    $pdo->exec("ALTER TABLE users ADD COLUMN city CHAR(63) DEFAULT ' '");
}

function version_1(PDO $pdo)
{
    $pdo->exec("ALTER TABLE users ADD COLUMN phone_number CHAR(12) DEFAULT ' '");
}
