<?php

namespace Kanboard\Plugin\UserProfileEdit\Schema;

use PDO;

const VERSION = 3;

function version_3(PDO $pdo)
{
    $pdo->exec("ALTER TABLE users ADD COLUMN location TEXT DEFAULT ' '");
}

function version_2(PDO $pdo)
{
    $pdo->exec("ALTER TABLE users ADD COLUMN city TEXT DEFAULT ' '");
}

function version_1(PDO $pdo)
{
    $pdo->exec("ALTER TABLE users ADD COLUMN phone_number TEXT DEFAULT ' '");
}
