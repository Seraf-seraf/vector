<?php

namespace Kanboard\Plugin\UserProfileEdit\Model;

use Kanboard\Core\Base;

class LocationModel extends Base {
    public function getLocations()
    {
        return [
            'from_office' => 'Из офиса',
            'from_home' => 'Удаленно',
        ];
    }
}