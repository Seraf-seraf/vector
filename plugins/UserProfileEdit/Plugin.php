<?php

namespace Kanboard\Plugin\UserProfileEdit;

use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Translator;

class Plugin extends Base
{
    public function initialize()
    {
        $this->template->setTemplateOverride('user_list/dropdown', 'UserProfileEdit:user_list/dropdown');
        $this->template->setTemplateOverride('user_list/user_title', 'UserProfileEdit:user_list/user_title');
        $this->template->setTemplateOverride('task_internal_link/table', 'UserProfileEdit:task_internal_link/table');
        $this->template->setTemplateOverride('task/transitions', 'UserProfileEdit:task/transitions');
        $this->template->setTemplateOverride('task/time_tracking_details', 'UserProfileEdit:task/time_tracking_details');
        $this->template->setTemplateOverride('group/users', 'UserProfileEdit:group/users');
        $this->template->setTemplateOverride('header/user_dropdown', 'UserProfileEdit:header/user_dropdown');
        $this->template->setTemplateOverride('user_view/show', 'UserProfileEdit:user_view/show');
        $this->template->setTemplateOverride('user_view/sidebar', 'UserProfileEdit:user_view/sidebar');
        $this->template->setTemplateOverride('user_modification/show', 'UserProfileEdit:user_modification/show');
        
        $this->hook->on('template:layout:css', array('template' => 'plugins/UserProfileEdit/Assets/css/user_profile_edit.css'));
    }

    public function onStartup()
    {
        Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getClasses()
    {
        return [
            'Plugin\UserProfileEdit\Model' => [
                'LocationModel',
            ],
        ];
    }

    public function getPluginName()
    {
        return 'UserProfileEdit';
    }

    public function getPluginDescription()
    {
        return t('Editing user\'s profile');
    }

    public function getPluginAuthor()
    {
        return 'Rustam Urazov';
    }

    public function getPluginVersion()
    {
        return '1.0.0';
    }

    public function getPluginHomepage()
    {
        return 'https://github.com/kanboard/plugin-myplugin';
    }
}

