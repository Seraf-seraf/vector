<?php

namespace Kanboard\Plugin\UserProfileEdit\Controller;

use Kanboard\Controller\BaseController;

class UserEditingController extends BaseController {
    public function show(array $values = array(), array $errors = array())
    {
        $user = $this->getUser();

        if (empty($values)) {
            $values = $user;
            unset($values['password']);
        }

        return $this->response->html($this->helper->layout->user('user_modification/show', array(
            'values' => $values,
            'errors' => $errors,
            'user' => $user,
            'locations' => $this->locationModel->getLocations(),
        )));
    }

    public function save()
    {
        $user = $this->getUser();
        $values = $this->request->getValues();

        if (! $this->userSession->isAdmin()) {
            $values = array(
                'id' => $this->userSession->getId(),
                'name' => isset($values['name']) ? $values['name'] : $user['name'],
                'phone_number' => isset($values['phone_number']) ? $values['phone_number'] : $user['phone_number'],
                'email' => isset($values['email']) ? $values['email'] : $user['email'],
                'city' => isset($values['city']) ? trim($values['city']) : '',
                'location' => isset($values['location']) ? trim($values['location']) : '',
            );
        }

        if ($this->userModel->update($values)) {
            $this->flash->success(t('User updated successfully.'));
            $this->response->redirect($this->helper->url->to('UserViewingController', 'show', array('user_id' => $user['id'], 'plugin' => 'UserProfileEdit')), true);
            return;
        } else {
            $this->flash->failure(t('Unable to update this user.'));
        }

        $this->show($values);
    }
}