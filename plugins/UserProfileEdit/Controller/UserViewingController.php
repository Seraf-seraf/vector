<?php

namespace Kanboard\Plugin\UserProfileEdit\Controller;

use Kanboard\Controller\UserViewController;

class UserViewingController extends UserViewController {
    public function show()
    {
        $user = $this->getUser();
        $this->response->html($this->helper->layout->user('user_view/show', array(
            'user'      => $user,
            'locations' => $this->locationModel->getLocations(),
        )));
    }
}