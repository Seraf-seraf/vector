<div class="page-header">
    <h2><?= t('Edit user') ?></h2>
</div>
<form method="post" action="<?= $this->url->href('UserEditingController', 'save', array('user_id' => $user['id'], 'plugin' => 'UserProfileEdit')) ?>">
    <?= $this->form->csrf() ?>
    <?= $this->form->hidden('id', $values) ?>

    <div class="user-profile-container">
        <div class="user_info_column">
            <p class="fullname">
                <?= $values['name'] ?>
            </p>

            <p class="contact-info">
                <?= $values['phone_number'] ?>
            </p>

            <p class="contact-info">
                <?= $values['email'] ?>
            </p>
        </div>
        <div class="user-data-column-1">
            <?= $this->form->label(t('Город'), 'city') ?>
            <?= $this->form->text('city', $values, $errors, array($this->user->hasAccess('UserEditingController', 'show/edit_city') ? 'autocomplete="city"' : 'readonly')) ?>
        </div>
        <div class="user-data-column-2">
            <?= $this->form->label(t('Расположение'), 'location') ?>
            <?= $this->form->select('location', $locations, $values, $errors, array($this->user->hasAccess('UserEditingController', 'show/edit_location') ? '' : 'disabled')) ?>
        </div>
    </div>
    
    <?= $this->modal->submitButtons() ?>
</form>
