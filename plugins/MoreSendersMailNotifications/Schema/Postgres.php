<?php

namespace Kanboard\Plugin\MoreSendersMailNotifications\Schema;

use PDO;

const VERSION = 1;

function version_1(PDO $pdo)
{
    $pdo->exec("
        CREATE TABLE senders_email (
            id SERIAL PRIMARY KEY,
            hostname VARCHAR(255) NOT NULL,
            port INTEGER NOT NULL,
            adress VARCHAR(255) NOT NULL UNIQUE,
            app_password VARCHAR(255) NOT NULL,
            encription VARCHAR(255) NOT NULL
        )
    ");
}
