<?php

namespace Kanboard\Plugin\MoreSendersMailNotifications;

use Kanboard\Plugin\MoreSendersMailNotifications\Model\SendersEmailModel;
use Kanboard\Plugin\MoreSendersMailNotifications\Controller\MoreSendersController;
use Kanboard\Plugin\MoreSendersMailNotifications\Model\AllUserNotificationModel;
use Kanboard\Plugin\MoreSendersMailNotifications\Job\MSNotificationJob;
use Kanboard\Plugin\MoreSendersMailNotifications\ServiceProvider\MSJobProvider;

use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Mail\ClientInterface;
use Kanboard\Core\Security\Role;
use Pimple\Container;

/**
 * MoreSendersMailNotifications Plugin
 *
 * @package  Kanboard\Plugin\MoreSendersMailNotifications
 * @author   Rzer_1
 */
class Plugin extends Base
{
    public function initialize()
    {

        $this->helper->register('MoreSendersHelper', '\Kanboard\Plugin\MoreSendersMailNotifications\Helper\MoreSendersHelper');
        $this->emailClient->setTransport('smtp(MoreSenders)', '\Kanboard\Plugin\MoreSendersMailNotifications\SmtpHandler');

        //$this->template->hook->attach('template:config:email', 'MoreSendersMailNotifications:config/email', array('senders_data' => $this->sendersEmailModel->getAll(), 'random_sender' => $this->sendersEmailModel->getRandom()));
        $this->template->hook->attach('template:config:email', 'MoreSendersMailNotifications:config/email', array('senders_data' => $this->sendersEmailModel->getAll()));
        $this->template->setTemplateOverride('forms/add', 'MoreSendersMailNotifications:forms/add');
        $this->template->setTemplateOverride('forms/edit', 'MoreSendersMailNotifications:forms/edit');
        $this->template->setTemplateOverride('forms/remove', 'MoreSendersMailNotifications:forms/remove');
        //удаление задачи пользователем которому она назначена Плагин TaskClosing
        //$this->template->setTemplateOverride('task/sidebar', 'MoreSendersMailNotifications:task/sidebar');
        $this->template->setTemplateOverride('task_suppression/remove', 'MoreSendersMailNotifications:task_suppression/remove');
        $this->applicationAccessMap->add('MoreSendersController', 'show', Role::APP_ADMIN);

        //для уведомлений самому себе
        $this->container->register(new MSJobProvider());

        //$this->route->addRoute('settings/email', 'MoreSendersController', 'show');

        // $this->template->hook->attach('template:project:integrations', 'mailgun:project/integration');
        // $this->template->hook->attach('template:config:integrations', 'mailgun:config/integration');
    }

    public function onStartup()
    {
        //Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getPluginDescription()
    {
        return 'Plugin for increasing the number of mailing accounts.';
    }

    public function getPluginAuthor()
    {
        return 'Rzer_1';
    }

    public function getPluginVersion()
    {
        return '1.0.0';
    }

    public function getPluginHomepage()
    {
        return 'https://github.com/kanboard/';
    }

    public function getCompatibleVersion()
    {
        return '>=1.0.0';
    }

    public function getClasses()
    {
        return [
            'Plugin\MoreSendersMailNotifications\Model' => [
                'SendersEmailModel',
                'AllNotificationModel'
            ],
        ];
    }
}