<?php

namespace Kanboard\Plugin\MoreSendersMailNotifications;



use Kanboard\Plugin\MoreSendersMailNotifications\Job\SmtpJob;
use Pimple\Container;
use Kanboard\Core\Base;
use Kanboard\Core\Mail\ClientInterface;
use League\HTMLToMarkdown\HtmlConverter;

/**
 * Mail SmtpHandler
 *
 * @package  Kanboard\Plugin\MoreSendersMailNotifications\
 * @author   Rzer_1
 */
class SmtpHandler extends Base
{
    /**
     * Mail transport instances
     *
     * @access private
     * @var \Pimple\Container
     */
    private $transports;

    /**
     * Constructor
     *
     * @access public
     * @param  \Pimple\Container   $container
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
        $this->transports = new Container;
    }

    /**
     * Send a HTML email
     *
     * @access public
     * @param  string  $recipientEmail
     * @param  string  $recipientName
     * @param  string  $subject
     * @param  string  $html
     * @return Client
     */
    public function sendEmail($recipientEmail, $recipientName, $subject, $html, $authorName = null, $authorEmail = null)
    {
        if (! empty($recipientEmail)) {
            $this->queueManager->push(SmtpJob::getInstance($this->container)->withParams(
                $recipientEmail,
                $recipientName,
                $subject,
                $html,
                is_null($authorName) ? $this->getAuthorName() : $authorName,
                is_null($authorEmail) ? $this->getAuthorEmail() : $authorEmail
            ));
        }

        return $this;
    }

    /**
     * Get author name
     *
     * @access public
     * @return string
     */
    public function getAuthorName()
    {
        $author = 'Kanboard';

        if ($this->userSession->isLogged()) {
            $author = e('%s via Kanboard', $this->helper->user->getFullname());
        }

        return $author;
    }

    /**
     * Get author email
     *
     * @access public
     * @return string
     */
    public function getAuthorEmail()
    {
        if ($this->userSession->isLogged()) {
            $userData = $this->userSession->getAll();
            return ! empty($userData['email']) ? $userData['email'] : '';
        }

        return '';
    }
}