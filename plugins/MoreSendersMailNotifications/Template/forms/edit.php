<div class="page-header">
    <h2><?= t('Изменение адреса') ?></h2>
</div>
<form method="post" action="<?= $this->url->href('MoreSendersController', 'edit',array('plugin'=> 'MoreSendersMailNotifications')) ?>" autocomplete="off">
    <?= $this->form->csrf() ?>
    <?= $this->form->hidden('id', $sender_data) ?>

    <?= $this->form->label(t('Адрес'), 'adress') ?>
    <?= $this->form->email('adress', array(), array(), array('autofocus', 'required', 'maxlength="50"', 'value=' . $sender_data['adress'])) ?>
    <?= $this->form->label(t('Хост'), 'hostname') ?>
    <?= $this->form->text('hostname', array(), array(), array('autofocus', 'required', 'maxlength="50"', 'value=' . $sender_data['hostname'])) ?>
    <?= $this->form->label(t('Порт'), 'port') ?>
    <?= $this->form->text('port', array(), array(), array('autofocus', 'required', 'maxlength="20"', 'value=' . $sender_data['port'])) ?>
    <?= $this->form->label(t('Пароль'), 'app_password') ?>
    <?= $this->form->text('app_password', array(), array(), array('autofocus', 'required', 'maxlength="191"', 'value=' . $sender_data['app_password'])) ?>
    <?= $this->form->label(t('Шифрование'), 'encription') ?>
    <!-- <? //$this->form->text('encription', array(), array(), array('autofocus', 'required', 'maxlength="4"', 'value=' . $sender_data['encription'])) ?> -->
    <!-- <? //$this->form->select('encription', array('0' => 'none','1' => 'ssl','2' => 'tls'), array('name' => 1), array(), array()) ?> -->
    <select name="encription" id="form-encription" class="">
        <?php foreach (array('0' => 'none','1' => 'ssl','2' => 'tls') as $key => $elem): ?>
            <?php if($sender_data['encription'] == $elem): ?>
                <option value='<?= $key ?>' selected><?= $elem ?></option>
            <?php else: ?>
                <option value='<?= $key ?>'><?= $elem ?></option>
            <?php endif ?>
        <?php endforeach ?>
    </select>

    <?= $this->modal->submitButtons() ?>
</form>