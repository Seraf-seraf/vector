<div class="page-header">
    <h2><?= t('Добавление адреса') ?></h2>
</div>
<form method="post" action="<?= $this->url->href('MoreSendersController', 'add',array('plugin'=> 'MoreSendersMailNotifications')) ?>" autocomplete="off">
    <?= $this->form->csrf() ?>

    <?= $this->form->label(t('Адрес'), 'adress') ?>
    <?= $this->form->email('adress', array(), array(), array('autofocus', 'required', 'maxlength="50"')) ?>
    <?= $this->form->label(t('Хост'), 'hostname') ?>
    <?= $this->form->text('hostname', array(), array(), array('autofocus', 'required', 'maxlength="50"')) ?>
    <?= $this->form->label(t('Порт'), 'port') ?>
    <?= $this->form->text('port', array(), array(), array('autofocus', 'required', 'maxlength="20"')) ?>
    <?= $this->form->label(t('Пароль'), 'app_password') ?>
    <?= $this->form->text('app_password', array(), array(), array('autofocus', 'required', 'maxlength="191"')) ?>
    <?= $this->form->label(t('Шифрование'), 'encription') ?>
    <!-- <? //$this->form->text('encription', $values, $errors, array('autofocus', 'required', 'maxlength="3"')) ?> -->
    <?= $this->form->select('encription', array('0' => 'none','1' => 'ssl','2' => 'tls',), array(), array()) ?>

    <?= $this->modal->submitButtons() ?>
</form>