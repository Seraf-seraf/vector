<div class="page-header">
    <h2><?= t('Удаление адреса') ?></h2>
</div>
<div class="confirm">
    <p class="alert alert-info"><?= t('Вы уверены, что хотите удалить этот адрес: "%s"?', $adress['adress']) ?></p>

    <?= $this->modal->confirmButtons(
        'MoreSendersController',
        'delete',
        array('plugin'=> 'MoreSendersMailNotifications','id' => $adress['id'])
    ) ?>
</div>
