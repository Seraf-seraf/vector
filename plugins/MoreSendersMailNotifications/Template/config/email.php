<h3>MoreSendersMailNotifications</h3>
<div class="panel">
    <h4><?= t('Список адресов') ?></h4>
    <!-- <? //$this->modal->medium('plus', t('Новый адрес'), 'MoreSendersController', 'index') ?> -->
    <!-- <? //t(print_r($this->modal->medium('plus', t('Новый адрес'), 'MoreSendersController', 'index'))) ?> -->
    <!-- <? //$this->url->link(t('Все адреса'), 'MoreSendersController', 'show', array('plugin' => 'MoreSendersMailNotifications')) ?> -->
    <!-- <? //t(print_r($senders_data[0])) ?> -->
    <!-- <? //t(print_r($random_sender)) ?> -->

    <?php if (isset($senders_data)): ?>
        <table>
            <thead>
                <tr>
                    <th><?= t('Адрес') ?></th>
                    <th><?= t('Хост') ?></th>
                    <th><?= t('Порт') ?></th>
                    <th><?= t('Пароль') ?></th>
                    <th><?= t('Шифрование') ?></th>
                    <th><?= t('Управление') ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($senders_data as $elem): ?>
                    <tr>
                        <td><?= $elem['adress'] ?></td>
                        <td><?= $elem['hostname'] ?></td>
                        <td><?= $elem['port'] ?></td>
                        <td><?= $elem['app_password'] ?></td>
                        <td><?= $elem['encription'] ?></td>
                        <td><?= $this->modal->medium('edit', t('Edit'), 'MoreSendersController', 'formedit', array('plugin' => 'MoreSendersMailNotifications', 'id' => $elem['id'])) ?></td>
                        <td><?= $this->modal->confirm('trash-o', t('Remove'), 'MoreSendersController', 'confirm_delete', array('plugin' => 'MoreSendersMailNotifications', 'id' => $elem['id'])) ?></td>
                    </tr>
                <?php endforeach ?>
                    <!-- <tr>
                        <td><strong><? //$random_sender['adress'] ?></strong></td>
                        <td><strong><? //$random_sender['hostname'] ?></strong></td>
                        <td><strong><? //$random_sender['port'] ?></strong></td>
                        <td><strong><? //$random_sender['app_password'] ?></strong></td>
                        <td><strong><? //$random_sender['encription'] ?></strong></td>
                        <td><button class="btn btn-white" style="border-color: blue;"><? //$this->url->link($this->text->e('Изменить'), 'MoreSendersController', 'formedit', array('plugin' => 'MoreSendersMailNotifications', 'id' => $random_sender['id'])) ?></button></td>
                        <td><button class="btn btn-white" style="border-color: red;"><? //$this->url->link($this->text->e('Удалить'), 'MoreSendersController', 'delete', array('plugin' => 'MoreSendersMailNotifications', 'sender_id' => $random_sender['id'])) ?></button></td>
                    </tr> -->
            </tbody>
        </table>
    <?php else: ?>
        <p class="alert"><?= t('Нет адресов') ?></p>
    <?php endif ?>
    <?= $this->modal->medium('plus', t('Добавить адрес'), 'MoreSendersController', 'formadd',array('plugin'=> 'MoreSendersMailNotifications')) ?>
</div>

