<?php

namespace Kanboard\Plugin\MoreSendersMailNotifications\Controller;

use Kanboard\Plugin\MoreSendersMailNotifications\Model\SendersEmailModel;
use PicoDb\Table;
use PicoDb\Database;

use Kanboard\Controller\BaseController;

/**
 * MoreSenders Controller
 *
 * @package  Kanboard\Plugin\MoreSendersMailNotifications\Controller
 * @author   Rzer_1
 */
class MoreSendersController extends BaseController
{
    public const ID = 3;

    public function show()
    {
        $senders_data = $this->sendersEmailModel->getAll();
        $random = rand(1, count($senders_data));
        $random_sender = $this->sendersEmailModel->getById($random);
        $this->response->html($this->template->render('config/email', array(
            'senders_data' => $senders_data,
            'random_sender' => $random_sender,
        )));

    }

    public function formadd()
    {
        $this->response->html($this->template->render('forms/add', array()));
    }

    public function add()
    {
        $values = $this->request->getValues();
        //$sender_id = self::ID;
        if ($this->sendersEmailModel->create($values['adress'],$values['hostname'],$values['port'],$values['app_password'],$values['encription']) !== false) {
            $this->flash->success(t('Адресс успешно добавлен.'));
        } else {
            $this->flash->failure(t('Не получилось добавить адресс.'));
        }

        return $this->response->redirect($this->helper->url->to('ConfigController', 'email', array()), true);
        //return $this->show();
    }

    public function formedit()
    {
        //$sender_id = self::ID;
        //var_dump($sender_id);
        $sender_id = $this->request->getIntegerParam('id');
        $sender_data = $this->sendersEmailModel->getById($sender_id);
        $this->response->html($this->template->render('forms/edit', array(
            'sender_data' => $sender_data,
        )));
    }

    public function edit()
    {
        $values = $this->request->getValues();
        //var_dump($values);
        //$sender_id = self::ID;
        if ($this->sendersEmailModel->edit($values['adress'],$values['hostname'],$values['port'],$values['app_password'],$values['encription'],$values['id']) !== false) {
            $this->flash->success(t('Адресс успешно изменен.'));
        } else {
            $this->flash->failure(t('Не получилось изменить адресс.'));
        }

        return $this->response->redirect($this->helper->url->to('ConfigController', 'email', array()), true);
    }

    public function confirm_delete()
    {
        $values = $this->request->getIntegerParam('id');
        $adress = $this->sendersEmailModel->getById($values);

        $this->response->html($this->template->render('forms/remove', array(
            'adress' => $adress,
        )));
    }

    public function delete()
    {
        //$sender_id = self::ID;
        $sender_id = $this->request->getIntegerParam('id');
        if ($this->sendersEmailModel->delete($sender_id)) {
            $this->flash->success(t('Адресс успешно удалён.'));
        } else {
            $this->flash->failure(t('Не получилось удалить адресс.'));
        }
        return $this->response->redirect($this->helper->url->to('ConfigController', 'email', array()), true);
    }

    /**
     * Confirmation dialog box before to remove the task
     */
    public function confirm()
    {
        $task = $this->getTask();

        $this->response->html($this->template->render('task_suppression/remove', array(
            'task' => $task,
            'redirect' => $this->request->getStringParam('redirect'),
        )));
    }

    /**
     * Remove a task
     */
    public function remove()
    {
        $task = $this->getTask();
        $this->checkCSRFParam();

        if ($this->taskModel->remove($task['id'])) {
            $this->flash->success(t('Task removed successfully.'));
        } else {
            $this->flash->failure(t('Unable to remove this task.'));
        }

        $redirect = $this->request->getStringParam('redirect') === '';
        $this->response->redirect($this->helper->url->to('VectorBoardController', 'show_my_tasks', array('plugin' => 'VectorTasks', 'project_id' => $task['project_id'], 'task' => 'owner')), $redirect);
    }
}
