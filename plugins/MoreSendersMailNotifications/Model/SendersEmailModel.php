<?php

namespace Kanboard\Plugin\MoreSendersMailNotifications\Model;

use Kanboard\Core\Base;

/**
 * SendersEmail Model
 *
 * @package  Kanboard\Plugin\MoreSendersMailNotifications
 * @author   Rzer_1
 */
class SendersEmailModel extends Base
{
    /**
     * SQL table name
     *
     * @var string
     */
    public const TABLE = 'senders_email';

    /**
     * Get a specific sender by id
     *
     * @access public
     * @param  integer $sender_id
     * @return array
     */
    public function getById($sender_id)
    {
        return $this->db->table(self::TABLE)->eq('id', $sender_id)->findOne();
    }

    /**
     * Get a random sender by id
     *
     * @access public
     * @return array
     */
    public function getRandom()
    {
        $senders_data = $this->db->table(self::TABLE)->columns('id')->findAll();
        $random = array_rand($senders_data);
        $random_sender = $this->sendersEmailModel->getById($senders_data[$random]['id']);
        return $random_sender;
    }

    /**
     * Get all senders
     *
     * @access public
     * @return array
     */
    public function getAll()
    {
        return $this->db->table(self::TABLE)->asc('id')->findAll();
    }

    /**
     * Create a new adress
     *
     * @access public
     * @param  string  $adress
     * @param  string  $hostname
     * @param  string  $port
     * @param  string  $app_password
     * @param  string  $encription
     * @param  string  $sender_id
     * @return integer|boolean
     */
    public function create($adress, $hostname, $port, $app_password, $encription)
    {
        switch ($encription) {
            case 0:
                $encription = 'none';
                break;
            case 1:
                $encription = 'ssl';
                break;
            case 2:
                $encription = 'tls';
                break;
        }
        
        return $this->db->table(self::TABLE)->persist(array(
            'adress' => $adress,
            'hostname' => $hostname,
            'port' => $port,
            'app_password' => $app_password,
            'encription' => $encription,
        ));
    }

    /**
     * Edit senders
     *
     * @access public
     * @param  string  $adress
     * @param  string  $hostname
     * @param  string  $port
     * @param  string  $app_password
     * @param  string  $encription
     * @param  string  $sender_id
     * @return integer|boolean
     */
    public function edit($adress, $hostname, $port, $app_password, $encription, $sender_id)
    {
        switch ($encription) {
            case 0:
                $encription = 'none';
                break;
            case 1:
                $encription = 'ssl';
                break;
            case 2:
                $encription = 'tls';
                break;
        }
        return $this->db->table(self::TABLE)->eq('id', $sender_id)->update(array(
            'adress' => $adress,
            'hostname' => $hostname,
            'port' => $port,
            'app_password' => $app_password,
            'encription' => $encription,
        ));
    }

    /**
     * Delete sender by id
     *
     * @access public
     * @param  integer $sender_id
     * @return array
     */
    public function delete($sender_id)
    {
        return $this->db->table(self::TABLE)->eq('id', $sender_id)->remove();
    }
}