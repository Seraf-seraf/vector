<?php

namespace Kanboard\Plugin\MoreSendersMailNotifications\Job;

use Kanboard\Event\GenericEvent;
use Kanboard\Job\NotificationJob;

/**
 * Class NotificationJob
 *
 * @package Kanboard\Job
 * @author  Frederic Guillot
 */
class MSNotificationJob extends NotificationJob
{
    /**
     * Set job parameters
     *
     * @param GenericEvent $event
     * @param string       $eventName
     * @return $this
     */
    public function withParams(GenericEvent $event, $eventName)
    {
        $this->jobParams = array($event->getAll(), $eventName);
        return $this;
    }

    /**
     * Execute job
     *
     * @param array  $eventData
     * @param string $eventName
     */
    public function execute(array $eventData, $eventName)
    {
        if (! empty($eventData['mention'])) {
            $this->allNotificationModel->sendUserNotification($eventData['mention'], $eventName, $eventData);
        } else {
            $this->allNotificationModel->sendNotifications($eventName, $eventData);
            $this->projectNotificationModel->sendNotifications($eventData['task']['project_id'], $eventName, $eventData);
        }
    }
}
