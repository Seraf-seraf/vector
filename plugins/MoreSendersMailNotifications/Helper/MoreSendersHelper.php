<?php

namespace Kanboard\Plugin\MoreSendersMailNotifications\Helper;

use Kanboard\Core\Base;
use Swift_SmtpTransport;
use Swift_Message;
use Swift_Mailer;
use Swift_TransportException;
use Kanboard\Plugin\MoreSendersMailNotifications\Model\SendersEmailModel;
use Kanboard\Core\Mail\ClientInterface;

/**
 * Class MoreSendersHelper
 *
 * @package Kanboard\Plugin\MoreSendersMailNotifications\Helper
 * @author  Rzer_1
 */
class MoreSendersHelper extends Base implements ClientInterface
{
    public function sendEmail($recipientEmail, $recipientName, $subject, $html, $authorName, $authorEmail = ''){
        try {
            $random_sender = $this->sendersEmailModel->getRandom();
            $message = Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($random_sender['adress'], $authorName)
                ->setTo(array($recipientEmail => $recipientName));

            $headers = $message->getHeaders();
            $headers->addTextHeader('Auto-Submitted', 'auto-generated');

            if (! empty($authorEmail)) {
                $message->setReplyTo($authorEmail);
            }

            $message->setBody($html, 'text/html');

            Swift_Mailer::newInstance($this->getTransport($random_sender))->send($message);
        } catch (Swift_TransportException $e) {
            $this->logger->error($e->getMessage());
        }
    }
    /**
     * Get mail transport instance
     *
     * @access public
     * @param  string  $transport
     * @return \Swift_Transport
     */
    public function getTransport($random_sender)
    {
        // Smtp транспорт
        $transport = Swift_SmtpTransport::newInstance($random_sender['hostname'], $random_sender['port']);
        $transport->setUsername($random_sender['adress']);
        $transport->setPassword($random_sender['app_password']);
        if (!is_null(MAIL_SMTP_HELO_NAME)) {
                $transport->setLocalDomain(MAIL_SMTP_HELO_NAME);
        }
        $transport->setEncryption($random_sender['encription']);
    
        if (HTTP_VERIFY_SSL_CERTIFICATE === false) {
            $transport->setStreamOptions(array(
                'ssl' => array(
                    'allow_self_signed' => true,
                    'verify_peer'       => false,
                    'verify_peer_name'  => false,
                )
            ));
        }

        $this->logger->debug(__METHOD__.' Email transport is ' . $this->helper->mail->getMailTransport() . '. Random senders set: '.$random_sender['adress']);
        return $transport;
    }

}
