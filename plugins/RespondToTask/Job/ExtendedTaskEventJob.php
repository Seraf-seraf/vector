<?php

namespace Kanboard\Plugin\RespondToTask\Job;

use Kanboard\Event\TaskEvent;
use Kanboard\EventBuilder\TaskEventBuilder;
use Kanboard\Job\BaseJob;
use Kanboard\Model\TaskModel;

class ExtendedTaskEventJob extends BaseJob {
    public function withParams($taskId, array $eventNames, array $changes = array(), array $values = array(), array $task = array())
    {
        $this->jobParams = array($taskId, $eventNames, $changes, $values, $task);
        return $this;
    }

    public function execute($taskId, array $eventNames, array $changes = array(), array $values = array(), array $task = array())
    {
        $event = TaskEventBuilder::getInstance($this->container)
            ->withTaskId($taskId)
            ->withChanges($changes)
            ->withValues($values)
            ->withTask($task)
            ->buildEvent();

        if ($event !== null) {
            foreach ($eventNames as $eventName) {
                $this->fireEvent($eventName, $event, $changes);
            }
        }
    }

    protected function fireEvent($eventName, TaskEvent $event, $changes)
    {
        $comment = false;
        $task = $this->taskFinderModel->getById($event->getTaskId());

        $this->logger->debug(__METHOD__.' Event fired: '.$eventName);
        $this->dispatcher->dispatch($event, $eventName);

        if ($eventName === TaskModel::EVENT_CREATE) {
            $userMentionJob = $this->userMentionJob->withParams($event['task']['description'], TaskModel::EVENT_USER_MENTION, $event);
            $this->queueManager->push($userMentionJob);
        } else if ($eventName === TaskModel::EVENT_MOVE_COLUMN) {
            if ($changes['src_column_id'] == 5 && $changes['dst_column_id'] == '1') {
                $comment = $this->prepareComment("Задача отправлена на доработку", $task['id']);
                $this->vectorNotifyModel->sendNotifications($task['id'], $task['controller_id'], "Задача отправлена на доработку", '');
            }

            switch ($task['column_id']) {
                case 2:
                    $comment = $this->prepareComment("Задача принята", $task['id']);
                    $this->vectorNotifyModel->sendNotifications($task['id'], $task['controller_id'], "Задача принята", '');
                    break;
                case 5:
                    $comment = $this->prepareComment("Задача отправлена на проверку", $task['id']);
                    $this->vectorNotifyModel->sendNotifications($task['id'], $task['controller_id'], "Задача отправлена на проверку", '');
                    break;
            }
        }

        if ($comment != false) {
            $this->commentModel->create($comment);
        }
    }

    protected function prepareComment($message, $taskId) {
        $comment = array();
        $comment['user_id'] = $this->userSession->getId();
        $comment['task_id'] = $taskId;
        $comment['comment'] = $message;
        return $comment;
    }
}