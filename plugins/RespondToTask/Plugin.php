<?php

namespace Kanboard\Plugin\RespondToTask;

use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Translator;
use Kanboard\Plugin\RespondToTask\Model\TaskReplyingModel;
use Kanboard\Plugin\RespondToTask\ServiceProvider\ExtendedClassProvider;
use Kanboard\Plugin\RespondToTask\ServiceProvider\ExtendedJobProvider;

class Plugin extends Base
{
    public function initialize()
    {
        //$this->container->register(new ExtendedJobProvider());
        $this->eventManager->register(TaskReplyingModel::EVENT_REVISION_COLUMN, 'Task revision');
    }

    public function onStartup()
    {
        Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getClasses() {
        return [
            'Plugin\RespondToTask\Model' => [
                'TaskReplyingModel',
            ],
            'Plugin\VectorNotify\Model' => [
                'VectorNotifyModel',
            ],
        ];
    }

    public function getPluginName()
    {
        return 'RespondToTask';
    }

    public function getPluginDescription()
    {
        return t('accept or reject the incoming task');
    }

    public function getPluginAuthor()
    {
        return 'Rustam Urazov';
    }

    public function getPluginVersion()
    {
        return '1.0.0';
    }

    public function getPluginHomepage()
    {
        return 'https://github.com/kanboard/plugin-myplugin';
    }
}

