<?php

namespace Kanboard\Plugin\RespondToTask\ServiceProvider;

use Kanboard\Job\CommentEventJob;
use Kanboard\Job\NotificationJob;
use Kanboard\Job\ProjectFileEventJob;
use Kanboard\Job\ProjectMetricJob;
use Kanboard\Job\SubtaskEventJob;
use Kanboard\Job\TaskFileEventJob;
use Kanboard\Job\TaskLinkEventJob;
use Kanboard\Job\UserMentionJob;
use Kanboard\Plugin\RespondToTask\Job\ExtendedTaskEventJob;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ExtendedJobProvider implements ServiceProviderInterface {
    public function register(Container $container)
    {
        $container['commentEventJob'] = $container->factory(function ($c) {
            return new CommentEventJob($c);
        });

        $container['subtaskEventJob'] = $container->factory(function ($c) {
            return new SubtaskEventJob($c);
        });

        $container['taskEventJob'] = $container->factory(function ($c) {
            return new ExtendedTaskEventJob($c);
        });

        $container['taskFileEventJob'] = $container->factory(function ($c) {
            return new TaskFileEventJob($c);
        });

        $container['taskLinkEventJob'] = $container->factory(function ($c) {
            return new TaskLinkEventJob($c);
        });

        $container['projectFileEventJob'] = $container->factory(function ($c) {
            return new ProjectFileEventJob($c);
        });

        $container['notificationJob'] = $container->factory(function ($c) {
            return new NotificationJob($c);
        });

        $container['projectMetricJob'] = $container->factory(function ($c) {
            return new ProjectMetricJob($c);
        });

        $container['userMentionJob'] = $container->factory(function ($c) {
            return new UserMentionJob($c);
        });

        return $container;
    }
}