<div class="page-header">
    <h2><?= 'Принять задачу' ?></h2>
</div>

<div class="confirm">
    <p class="alert alert-info">
        <?= t('Вы действительно хотите принять задачу "%s"?', $task['title']) ?>
    </p>

    <?= $this->modal->confirmButtons(
        'TaskReplyingController',
        'accept',
        array('task_id' => $task['id'], 'confirmation' => 'yes', 'plugin' => 'RespondToTask')
    ) ?>
</div>
