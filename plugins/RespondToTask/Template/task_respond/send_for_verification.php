<div class="page-header">
    <h2><?= 'Отправить задачу на проверку' ?></h2>
</div>

<div class="confirm">
    <p class="alert alert-info">
        <?= t('Вы действительно хотите отправить задачу "%s" на проверку?', $task['title']) ?>
    </p>

    <?= $this->modal->confirmButtons(
        'TaskReplyingController',
        'sendForVerification',
        array('task_id' => $task['id'], 'confirmation' => 'yes', 'plugin' => 'RespondToTask')
    ) ?>
</div>
