<div class="page-header">
    <h2><?= 'Отправить задачу на доработку' ?></h2>
</div>

<div class="confirm">
    <p class="alert alert-info">
        <?= t('Вы действительно хотите отправить задачу "%s" на доработку?', $task['title']) ?>
    </p>

    <?= $this->modal->confirmButtons(
        'TaskReplyingController',
        'sendForRevision',
        array('task_id' => $task['id'], 'confirmation' => 'yes', 'plugin' => 'RespondToTask')
    ) ?>
</div>
