<div class="page-header">
    <h2><?= 'Отклонить задачу' ?></h2>
</div>

<div class="confirm">
    <p class="alert alert-info">
        <?= t('Вы действительно хотите отклонить задачу "%s"?', $task['title']) ?>
    </p>

    <?= $this->modal->confirmButtons(
        'TaskReplyingController',
        'reject',
        array('task_id' => $task['id'], 'confirmation' => 'yes', 'plugin' => 'RespondToTask')
    ) ?>
</div>
