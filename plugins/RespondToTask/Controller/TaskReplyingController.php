<?php

namespace Kanboard\Plugin\RespondToTask\Controller;

use Kanboard\Controller\BaseController;

class TaskReplyingController extends BaseController {
    public function accept() {
        $this->changeStatus('accept', 'RespondToTask:task_respond/accept', 'Задача принята.', t('Unable to update your task.'));
    }

    public function reject() {
        $this->changeStatus('reject', 'RespondToTask:task_respond/reject', 'Задача отклонена.', t('Unable to update your task.'));
    }

    public function sendForVerification() {
        $this->changeStatus('sendForVerification', 'RespondToTask:task_respond/send_for_verification', 'Задача отправлена на проверку', t('Unable to update your task.'));
    }

    public function sendForRevision() {
        $this->changeStatus('sendForRevision', 'RespondToTask:task_respond/send_for_revision', 'Задача отправлена на доработку', t('Unable to update your task.'));
    }

    private function changeStatus($method, $template, $success_message, $failure_message) {
        $task = $this->getTask();
        if ($this->request->getStringParam('confirmation') === 'yes') {
            $this->checkCSRFParam();

            if ($this->taskReplyingModel->$method($task['id'])) {
                $this->vectorNotifyModel->sendNotifications($task['id'], $task['controller_id'], $success_message, '');
                $this->flash->success($success_message);
            } else {
                $this->flash->failure($failure_message);
            }

            $this->response->redirect($this->helper->url->to('TaskViewController', 'show', array('task_id' => $task['id'])), true);
        } else {
            $this->response->html($this->template->render($template, array(
                'task' => $task,
            )));
        }
    }
}