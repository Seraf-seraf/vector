<?php

namespace Kanboard\Plugin\RespondToTask\Model;

use Kanboard\Core\Base;
use Kanboard\Model\TaskModel;

class TaskReplyingModel extends Base {
    public const EVENT_REVISION_COLUMN = 'task.move.revision';

    public function accept($taskId) {
        $task = $this->taskFinderModel->getById($taskId);
        $values = $this->prepare(2, $task['owner_id']);

        $comment = $this->prepareComment("Задача принята", $taskId);

        $result = $this->db->table(TaskModel::TABLE)->eq('id', $taskId)->update($values);

        if ($result) {
            $this->commentModel->create($comment);
        }

        return $result;
    }

    public function reject($taskId) {
        $task = $this->taskFinderModel->getById($taskId);
        $values = $this->prepare(1, $task['creator_id']);

        $comment = $this->prepareComment("Задача отклонена", $taskId);

        $result = $this->db->table(TaskModel::TABLE)->eq('id', $taskId)->update($values);

        if ($result) {
            $this->commentModel->create($comment);
        }

        return $result;
    }

    public function sendForVerification($taskId) {
        $task = $this->taskFinderModel->getById($taskId);
        $values = $this->prepare(5, $task['owner_id']);

        $comment = $this->prepareComment("Задача отправлена на проверку", $taskId);
        
        $result = $this->db->table(TaskModel::TABLE)->eq('id', $taskId)->update($values);

        if ($result) {
            $this->commentModel->create($comment);
        }

        return $result;
    }

    public function sendForRevision($taskId) {
        $task = $this->taskFinderModel->getById($taskId);
        $values = $this->prepare(1, $task['owner_id']);

        $comment = $this->prepareComment("Задача отправлена на доработку", $taskId);

        $result = $this->db->table(TaskModel::TABLE)->eq('id', $taskId)->update($values);

            
        if ($result) {
            $this->commentModel->create($comment);
        }

        return $result;
    }

    protected function prepare($columnId, $ownerId) {
        $values = array();
        $values['column_id'] = $columnId;
        $values['owner_id'] = $ownerId;
        return $values;
    }

    protected function prepareComment($message, $taskId) {
        $comment = array();
        $comment['user_id'] = $this->userSession->getId();
        $comment['task_id'] = $taskId;
        $comment['comment'] = $message;
        return $comment;
    }
}