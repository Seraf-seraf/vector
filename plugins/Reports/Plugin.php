<?php

namespace Kanboard\Plugin\Reports;

use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Translator;

class Plugin extends Base
{
    public function initialize()
    {
        $this->hook->on('template:layout:css', array('template' => 'plugins/Reports/Assets/style.css'));
        $this->hook->on('template:layout:js', array('template' => 'plugins/Reports/Assets/main.js'));
        $this->template->hook->attach('template:dashboard:sidebar', 'Reports:dashboard/sidebar');
        $this->helper->register('customLayoutHelper', '\Kanboard\Plugin\Reports\Helper\ExtendedLayoutHelper');
        $this->helper->register('dateHelper', '\Kanboard\Plugin\Reports\Helper\DateHelper');
    }

    public function onStartup()
    {
        Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getClasses() {
        return [
            'Plugin\Reports\Model' => [
                'CongestionModel',
                'RepeatTasksModel'
            ],
        ];
    }

    public function getPluginName()
    {
        return 'Reports';
    }

    public function getPluginDescription()
    {
        return t('My plugin is awesome');
    }

    public function getPluginAuthor()
    {
        return 'Rustam Urazov';
    }

    public function getPluginVersion()
    {
        return '1.0.0';
    }

    public function getPluginHomepage()
    {
        return 'https://github.com/kanboard/plugin-myplugin';
    }
}

