<?php

namespace Kanboard\Plugin\Reports\Model;

use Kanboard\Core\Base;


class RepeatTasksModel extends Base
{

    // Принимает массив с повторяющими задачами: [['Название задачи'] => [1], [2], ..]
    public function createOnCommunicationTable(array $data)
    {
        $rows = [];

        foreach ($data as $recurrenceId => $tasks) {
            // Столбцы
            $rows[$recurrenceId] = [];
            $rows[$recurrenceId] += ['title' => $tasks[0]['task_title']]; // Навзание;
            $rows[$recurrenceId] += ['count' => count($tasks)]; // Количесвто коммуникаций
            $rows[$recurrenceId] += ['withoutProofs' => 0]; // Задачи, закрытые без доказательств
            $rows[$recurrenceId] += ['notCompleted' => 0]; // Невыполненные задачи
            $rows[$recurrenceId] += ['waitingTasks' => 0]; // Ожидающие
            $rows[$recurrenceId] += ['inProgressTasks' => 0]; // В процессе
            $rows[$recurrenceId] += ['agreedTasks' => 0]; // Согласованные
            $rows[$recurrenceId] += ['completedTasks' => 0]; // Выполнено
            $rows[$recurrenceId] += ['percentCompletedTasks' => 0]; // Процент выполненных задач

            foreach ($tasks as $task) {
                if ($this->checkTaskWithoutProofs($task['task_id'])) {
                    $rows[$recurrenceId]['withoutProofs'] += 1;
                }

                if ($task['date_completed'] == null && $task['column_id'] != 4) {
                    $rows[$recurrenceId]['notCompleted'] += 1;
                }

                switch ($task['column_id']) {
                    case 1:
                        $rows[$recurrenceId]['waitingTasks'] += 1;
                        break;
                    case 2:
                        $rows[$recurrenceId]['agreedTasks'] += 1;
                        break;
                    case 3:
                        $rows[$recurrenceId]['inProgressTasks'] += 1;
                        break;
                    case 4:
                        $rows[$recurrenceId]['completedTasks'] += 1;
                        break;
                }

                $rows[$recurrenceId]['percentCompletedTasks'] = number_format($rows[$recurrenceId]['completedTasks'] / $rows[$recurrenceId]['count'] * 100, 2, '.', '');
            }
        }

        return $rows;
    }

    private function checkTaskWithoutProofs($taskId)
    {
        return !empty($this->db->table('tasks')
            ->join('task_has_proof', 'task_id', 'id')
            ->eq('tasks.id', $taskId)
            ->notNull('tasks.date_completed')
            ->findAll()
        );
    }
}