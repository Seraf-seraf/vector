<?php

namespace Kanboard\Plugin\Reports\Model;

use Kanboard\Core\Base;

class CongestionModel extends Base {
    public function getDeadlineCongestions($userId, $time, $due = PHP_INT_MAX)
    {
        $now = time();

        $data = $this->taskFinderModel->getExtendedQuery()
            ->isNull('date_completed')
            ->neq('columns.position', 4)
            ->lt('date_due', $now)
            ->gte('date_due', $time)
            ->lte('date_due', $due);

        if ($userId != -1) {
            $data->eq('tasks.owner_id', $userId);
        }

        return $data->orderBy('tasks.owner_id', 'ASC')->findAll();
    }

    public function getNotAcceptedCongestions($userId, $time, $due = PHP_INT_MAX)
    {
        $now = time();
        $data = $this->taskFinderModel->getExtendedQuery()
            ->eq('columns.position', 1)
            ->gte('date_creation', $time)
            ->lte('date_creation', $due)
            ->lte('date_creation + 84600', $now);

        if ($userId != -1) {
            $data->eq('tasks.owner_id', $userId);
        }
        return $data->orderBy('tasks.owner_id', 'ASC')->findAll();
    }

    public function getAllCommunications($userId, $time, $due = PHP_INT_MAX)
    {
        $data = $this->taskFinderModel
            ->getExtendedQuery()
            ->gte('date_creation', $time)
            ->lte('date_creation', $due);

        if ($userId != -1) {
            $data->eq('tasks.owner_id', $userId);
        }

        return $data->orderBy('tasks.owner_id', 'ASC')->orderBy('tasks.date_creation', 'ASC')->findAll();
    }

    public function getAllActivities($userId, $time, $due = PHP_INT_MAX)
    {
        $activities = $this->helper->projectActivity->getProjectEvents(1, PHP_INT_MAX);

        if (!empty($activities)) {
                $mergedEvents = array();

                foreach ($activities as $key => $value) {
                    $author_username = $value['author'];

                    $date_creation = $value['date_creation'];

                    $author_id = $value['creator_id'];
                    $task_id = $value['task']['id'];
                    
                    if ($userId != -1 && $userId != $author_id) {
                        continue;
                    }
                    
                    if (($date_creation < $time) || ($date_creation >= $due)) {
                        continue;
                    }
    
                    $mergedEvents[$author_id]['author_username'] = $author_username;
                    $mergedEvents[$author_id]['task_id'] = $task_id;
                    $mergedEvents[$author_id]['events'][] = [$value['event_title'], $date_creation];
    
                }
                
                return $mergedEvents;
            }

        return null;
    }

    public function getOnCommunicationList($userId, $time, $due = PHP_INT_MAX)
    {
        $duplicatedTask = [];

        $data = $this->taskFinderModel->getExtendedQuery()
            ->gte('date_creation', $time)
            ->lte('date_creation', $due);

        if ($userId != -1) {
            $data->eq('tasks.owner_id', $userId);
        }

        $data = $data->orderBy('tasks.owner_id', 'ASC')->orderBy('tasks.date_creation', 'ASC')->findAll();

        foreach ($data as $task) {
            $title = $task["title"];

            // Проверяем, содержит ли название копии подстроку '[КОПИЯ]'
            if (str_contains($title, t('[DUPLICATE]'))) {
                // Название содержит '[КОПИЯ]', обрезаем его для группировки
                $title = substr($title, strpos($title, ']') + 2);
            }

            if (isset($duplicatedTask[$title])) {
                $duplicatedTask[$title][] = $task;
            } else {
                $duplicatedTask[$title] = [$task];
            }
        }

        return $duplicatedTask;
    }

    public function getRepeatTasks($userId = -1, $time = 0, $due = PHP_INT_MAX)
    {
        $query = "SELECT *, tasks.id AS task_id, tasks.title AS task_title, tasks.column_id AS column_id FROM TASKS
              INNER JOIN recurrence_tasks ON tasks.recurrence_id = recurrence_tasks.id
              INNER JOIN columns ON columns.id = tasks.column_id
              WHERE tasks.date_creation >= $time AND tasks.date_creation <= $due";

        if ($userId > 0) {
            $query .= " AND tasks.owner_id = $userId";
        }

        $duplicatedTask = $this->db->execute($query)->fetchAll();

        return array_reduce($duplicatedTask, function($result, $task) {
            $result[$task['recurrence_id']][] = $task;
            return $result;
        }, array());
    }

}