<?php

namespace Kanboard\Plugin\Reports\Helper;

class DateHelper
{
    // получение начала текущего дня
    public static function getTodayAsInt()
    {
        $now = date("Y-m-d");
        return strtotime($now);
    }

    public static function getYesterdayAsInt() {
        // 86400 - количество секунд в сутках
        return self::getTodayAsInt() - 86400;
    }

    public static function getLastWeekAsInt() {
        $this_week = strtotime('monday this week');
        return $this_week;
    }

    public static function getLastMonthAsInt() {
        $this_month = date("Y-m");
        return strtotime($this_month);
    }

    public static function getTimestamp($time) {
        return strtotime($time);
    }

    public static function setDatesPeriod($firstDate, $secondDate)
    {
        $firstDate = $firstDate == '' ? self::getTimestamp(1970) : self::getTimestamp($firstDate);
        // ДО включительно
        $secondDate = $secondDate == '' ? self::getTimestamp(9999) : self::getTimestamp($secondDate) + 86400;

        return [$firstDate, $secondDate];
    }
}