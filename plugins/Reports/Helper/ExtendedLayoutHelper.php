<?php

namespace Kanboard\Plugin\Reports\Helper;

use Kanboard\Helper\LayoutHelper;

class ExtendedLayoutHelper extends LayoutHelper {
    public function report($template, array $params = array()) {
        $content = $this->template->render($template, $params);

        $params['content_for_report'] = $content;

        return $this->dashboard('Reports:report/layout', $params);
    }
}