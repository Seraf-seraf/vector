<?php

namespace Kanboard\Plugin\Reports\Controller;

use Kanboard\Controller\BaseController;
use Kanboard\Plugin\Reports\Helper\DateHelper;

class ReportsController extends BaseController
{
    public function show($template = '', array $params = array()) {
        $user = $this->getUser();
        $params['plugin'] = 'Reports';
        $params['title'] = t('Dashboard for %s', $this->helper->user->getFullname($user));
        $params['user'] = $user;
        $params['controllerName'] = $this->router->getController();
        $params['actionName'] = $this->router->getAction();
        $params['employees'] = $this->getListEmployees();

        if ($template == '') {
            $this->communicationsList();
        } else {
            $this->response->html($this->helper->customLayoutHelper->report($template, $params));
        }
    }

    public function onCommunications() {
        $values = $this->request->getValues();
        $selectedDate = $this->request->getStringParam('date', 'all');
        $selectedDatePeriod_1 = $values['period_1'] ?? '';
        $selectedDatePeriod_2 = $values['period_2'] ?? '';
        $selectedEmployee = $this->getSelectedEmployee($values, $this->getListEmployees());

        $repeatTasks = array();

        if (empty($selectedDatePeriod_1) && empty($selectedDatePeriod_2)) {
            switch ($selectedDate) {
                case 'all':
                    $repeatTasks = $this->congestionModel->getRepeatTasks($selectedEmployee, 0);
                    break;
                case 'today':
                    $repeatTasks = $this->congestionModel->getRepeatTasks($selectedEmployee, DateHelper::getTodayAsInt());
                    break;
                case 'yesterday':
                    $repeatTasks = $this->congestionModel->getRepeatTasks($selectedEmployee, DateHelper::getYesterdayAsInt(), DateHelper::getTodayAsInt());
                    break;
                case 'week':
                    $repeatTasks = $this->congestionModel->getRepeatTasks($selectedEmployee, DateHelper::getLastWeekAsInt());
                    break;
                case 'month':
                    $repeatTasks = $this->congestionModel->getRepeatTasks($selectedEmployee, DateHelper::getLastMonthAsInt());
                    break;
            }
        } else {
            $repeatTasks =  $this->congestionModel->getRepeatTasks(
                $selectedEmployee,
                ...DateHelper::setDatesPeriod($selectedDatePeriod_1, $selectedDatePeriod_2),
            );
        }

        $repeatTasks = $this->repeatTasksModel->createOnCommunicationTable($repeatTasks);

        $this->show('Reports:report/communications', array(
            'filters' => array(
                'date' => $selectedDate,
                'period_1' => $selectedDatePeriod_1,
                'period_2' => $selectedDatePeriod_2,
                'selectedEmployee' => $selectedEmployee
            ),
            'repeatTasks' => $repeatTasks
        ));
    }

    public function onCommunicators() {
        $values = $this->request->getValues();
        $selectedDate = $this->request->getStringParam('date', 'all');
        $selectedDatePeriod_1 = $values['period_1'] ?? '';
        $selectedDatePeriod_2 = $values['period_2'] ?? '';
        $selectedEmployee = $this->getSelectedEmployee($values, $this->getListEmployees());

        $tasksWithCommunicators = array();

        if (empty($selectedDatePeriod_1) && empty($selectedDatePeriod_2)) {
            switch ($selectedDate) {
                case 'all':
                    $tasksWithCommunicators = $this->congestionModel->getAllActivities($selectedEmployee, 0);
                    break;
                case 'today':
                    $tasksWithCommunicators = $this->congestionModel->getAllActivities($selectedEmployee, DateHelper::getTodayAsInt());
                    break;
                case 'yesterday':
                    $tasksWithCommunicators = $this->congestionModel->getAllActivities($selectedEmployee, DateHelper::getYesterdayAsInt(), DateHelper::getTodayAsInt());
                    break;
                case 'week':
                    $tasksWithCommunicators = $this->congestionModel->getAllActivities($selectedEmployee, DateHelper::getLastWeekAsInt());
                    break;
                case 'month':
                    $tasksWithCommunicators = $this->congestionModel->getAllActivities($selectedEmployee, DateHelper::getLastMonthAsInt());
                    break;
            }
        } else {
            $tasksWithCommunicators = $this->congestionModel->getAllActivities(
                $selectedEmployee,
                ...DateHelper::setDatesPeriod($selectedDatePeriod_1, $selectedDatePeriod_2),
            );
        }

        $this->show(
        'Reports:report/communicators',
        array(
            'tasksWithCommunicators' => $tasksWithCommunicators,
            'filters' => array(
                'date' => $selectedDate,
                'period_1' => $selectedDatePeriod_1,
                'period_2' => $selectedDatePeriod_2,
                'selectedEmployee' => $selectedEmployee
            )
        )
    );
    }

    public function onCongestions() {
        $values = $this->request->getValues();
        $selectedDate = $this->request->getStringParam('date', 'all');
        $selectedDatePeriod_1 = $values['period_1'] ?? '';
        $selectedDatePeriod_2 = $values['period_2'] ?? '';
        $selectedEmployee = $this->getSelectedEmployee($values, $this->getListEmployees());

        $congestions = $deadlineCongestions = $notAcceptedCongestions = array();

        if (empty($selectedDatePeriod_1) && empty($selectedDatePeriod_2)) {
            switch ($selectedDate) {
                case 'all':
                    $deadlineCongestions = $this->congestionModel->getDeadlineCongestions($selectedEmployee, 0);
                    $notAcceptedCongestions = $this->congestionModel->getNotAcceptedCongestions($selectedEmployee, 0);
                    break;
                case 'today':
                    $deadlineCongestions = $this->congestionModel->getDeadlineCongestions($selectedEmployee, DateHelper::getTodayAsInt());
                    $notAcceptedCongestions = $this->congestionModel->getNotAcceptedCongestions($selectedEmployee, DateHelper::getTodayAsInt());
                    break;
                case 'yesterday':
                    $deadlineCongestions = $this->congestionModel->getDeadlineCongestions($selectedEmployee, DateHelper::getYesterdayAsInt(), DateHelper::getTodayAsInt());
                    $notAcceptedCongestions = $this->congestionModel->getNotAcceptedCongestions($selectedEmployee, DateHelper::getYesterdayAsInt(), DateHelper::getTodayAsInt());
                    break;
                case 'week':
                    $deadlineCongestions = $this->congestionModel->getDeadlineCongestions($selectedEmployee, DateHelper::getLastWeekAsInt());
                    $notAcceptedCongestions = $this->congestionModel->getNotAcceptedCongestions($selectedEmployee, DateHelper::getLastWeekAsInt());
                    break;
                case 'month':
                    $deadlineCongestions = $this->congestionModel->getDeadlineCongestions($selectedEmployee, DateHelper::getLastMonthAsInt());
                    $notAcceptedCongestions = $this->congestionModel->getNotAcceptedCongestions($selectedEmployee, DateHelper::getLastMonthAsInt());
                    break;
                }
        } else {
            $deadlineCongestions = $this->congestionModel->getDeadlineCongestions($selectedEmployee, ...DateHelper::setDatesPeriod($selectedDatePeriod_1, $selectedDatePeriod_2));
            $notAcceptedCongestions = $this->congestionModel->getNotAcceptedCongestions($selectedEmployee, ...DateHelper::setDatesPeriod($selectedDatePeriod_1, $selectedDatePeriod_2));
        }

        foreach ($deadlineCongestions as $congestion) {
            $congestions[$congestion['assignee_name']]['deadline'][] = $congestion;
        }

        foreach ($notAcceptedCongestions as $congestion) {
            $congestions[$congestion['assignee_name']]['not_accepted'][] = $congestion;
        }

        $this->show(
            'Reports:report/congestions',
            array(
                'congestions' => $congestions,
                'filters' => array(
                    'date' => $selectedDate,
                    'period_1' => $selectedDatePeriod_1,
                    'period_2' => $selectedDatePeriod_2,
                    'selectedEmployee' => $selectedEmployee
                )
            )
        );
    }

    public function communicationsList()
    {
        $values = $this->request->getValues();
        $selectedDate = $this->request->getStringParam('date', 'all');
        $selectedDatePeriod_1 = $values['period_1'] ?? '';
        $selectedDatePeriod_2 = $values['period_2'] ?? '';
        $selectedEmployee = $this->getSelectedEmployee($values, $this->getListEmployees());

        $communications = array();
        if (empty($selectedDatePeriod_1) && empty($selectedDatePeriod_2)) {
            switch ($selectedDate) {
                case 'all':
                    $communications = $this->congestionModel->getAllCommunications($selectedEmployee, 0);
                    break;
                case 'today':
                    $communications = $this->congestionModel->getAllCommunications($selectedEmployee, DateHelper::getTodayAsInt());
                    break;
                case 'yesterday':
                    $communications = $this->congestionModel->getAllCommunications($selectedEmployee, DateHelper::getYesterdayAsInt(), DateHelper::getTodayAsInt());
                    break;
                case 'week':
                    $communications = $this->congestionModel->getAllCommunications($selectedEmployee, DateHelper::getLastWeekAsInt());
                    break;
                case 'month':
                    $communications = $this->congestionModel->getAllCommunications($selectedEmployee, DateHelper::getLastMonthAsInt());
                    break;
            }
        } else {
            $communications = $this->congestionModel->getAllCommunications(
                $selectedEmployee,
                ...DateHelper::setDatesPeriod($selectedDatePeriod_1, $selectedDatePeriod_2)
            );
        }

        $this->show(
            'Reports:report/communicationsList',
            array(
                'communications' => $communications,
                'filters' => array(
                    'date' => $selectedDate,
                    'period_1' => $selectedDatePeriod_1,
                    'period_2' => $selectedDatePeriod_2,
                    'selectedEmployee' => $selectedEmployee
                )
            )
        );
    }

    protected function getListEmployees()
    {
        $employees = $this->userModel->getActiveUsersList(false);
        $employees[-1] = 'Не выбран';
        asort($employees, SORT_STRING );

        return $employees;
    }

    protected function getSelectedEmployee($values, $employees) {
        return isset($values['selectedEmployee']) && in_array($values['selectedEmployee'], array_keys($employees))
            ? $values['selectedEmployee']
            : -1;
    }
}