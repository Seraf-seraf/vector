<section class="activities">
    <ul class="reports">
        <?php if (!empty($tasksWithCommunicators)): ?>
            <?php foreach ($tasksWithCommunicators as $userID => $data): ?>
            <?php if (!empty($data['events'])): ?>
                <p class="user user--close"><?= $this->text->e($data['author_username']) ?></p>
                <li class="report-item">
                    <ul class="activities-list">
                        <?php foreach ($data['events'] as $event): ?>
                            <li class="activities-item activity">
                                <p class="activity-date">
                                    <?= date('d.m.Y', $event[1]) ?>
                                </p>
                                <p class="activity-event">
                                    <?= $this->url->link($this->text->e($this->text->e($event[0])), 'VectorTaskViewController', 'show', array('plugin' => 'VectorTasks', 'task_id' => $data['task_id'])); ?>
                                </p>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php endif ?>
    </ul>
</section>
