<section class="topbar-container">
    <div class="topbar">
        <ul>
            <li <?= $this->app->checkMenuSelection('ReportsController', 'onCommunications') ?>>
                <?= $this->modal->replaceLink(
                    'По коммуникациям',
                    'ReportsController',
                    'onCommunications',
                    array('plugin' => 'Reports')
                ) ?>
            </li>
            <li <?= $this->app->checkMenuSelection('ReportsController', 'onCommunicators') ?>>
                <?= $this->modal->replaceLink(
                    'По коммуникаторам',
                    'ReportsController',
                    'onCommunicators',
                    array('plugin' => 'Reports')
                ) ?>
            </li>
            <li <?= $this->app->checkMenuSelection('ReportsController', 'onCongestions') ?>>
                <?= $this->modal->replaceLink(
                    'По заторам',
                    'ReportsController',
                    'onCongestions',
                    array('plugin' => 'Reports')
                ) ?>
            </li>
            <li <?= $this->app->checkMenuSelection('ReportsController', 'communicationsList') ?>>
                <?= $this->modal->replaceLink(
                    'Список коммуникаций',
                    'ReportsController',
                    'communicationsList',
                    array('plugin' => 'Reports')
                ) ?>
            </li>
        </ul>
    </div>
    <form id="reportsFilters" method="post"
          action="<?= $this->url->to($controllerName, $actionName, array('plugin' => 'Reports')) ?>">
        <?= $this->form->csrf() ?>
        <div class="form-field">
            <span>Сотрудник </span>
            <select name="selectedEmployee" id="employee" class="tag-autocomplete scrollable" tabindex="3">
                <?php foreach ($employees as $id => $option): ?>
                    <?php
                    $selected = in_array($id, $filters) ? 'selected' : '';
                    $html = sprintf(
                        '<option value="%s" %s>%s</option>',
                        $this->helper->text->e($id),
                        $selected,
                        $this->helper->text->e($option)
                    );
                    echo $html;
                    ?>
                <?php endforeach; ?>
            </select>
        </div>

        <div class="date-select" id="dateSelect">
            <span style="padding: 4px;">Дата </span>
            <input type="radio" name="date" id="all" <?php
            if ($filters['date'] == 'all'): ?>checked<?php
            endif; ?> style="display: none">
            <label for="all" class="date-label"><?= $this->url->link(
                    'Все время',
                    $controllerName,
                    $actionName,
                    array('plugin' => 'Reports', 'date' => 'all')
                ); ?></label>
            <input type="radio" name="date" id="today" <?php
            if ($filters['date'] == 'today'): ?>checked<?php
            endif; ?> style="display: none">
            <label for="today" class="date-label"><?= $this->url->link(
                    'Сегодня',
                    $controllerName,
                    $actionName,
                    array('plugin' => 'Reports', 'date' => 'today')
                ); ?></label>
            <input type="radio" name="date" id="yesterday" <?php
            if ($filters['date'] == 'yesterday'): ?>checked<?php
            endif; ?> style="display: none">
            <label for="yesterday" class="date-label"><?= $this->url->link(
                    'Вчера',
                    $controllerName,
                    $actionName,
                    array('plugin' => 'Reports', 'date' => 'yesterday')
                ); ?></label>
            <input type="radio" name="date" id="week" <?php
            if ($filters['date'] == 'week'): ?>checked<?php
            endif; ?> style="display: none">
            <label for="week" class="date-label"><?= $this->url->link(
                    'Неделя',
                    $controllerName,
                    $actionName,
                    array('plugin' => 'Reports', 'date' => 'week')
                ); ?></label>
            <input type="radio" name="date" id="month" <?php
            if ($filters['date'] == 'month'): ?>checked<?php
            endif; ?> style="display: none">
            <label for="month" class="date-label"><?= $this->url->link(
                    'Месяц',
                    $controllerName,
                    $actionName,
                    array('plugin' => 'Reports', 'date' => 'month')
                ); ?></label>

            <?= $this->form->date('C: ', 'period_1', $filters); ?>
            <?= $this->form->date('До: ', 'period_2', $filters); ?>
        </div>
        <input class="button-filters" type="submit" value="Найти">
    </form>
    <div class="topbar-content">
        <?= $content_for_report ?>
    </div>
</section>