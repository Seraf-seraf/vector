<section class="reports">
    <?php foreach ($congestions as $name => $congestion): ?>
        <div>
            <div class="user user--close">
                <p class="user-name">
                    <?= $name ?>
                </p>
                <p>
                    <?= 'Заторов: '.$this->text->e(count($congestion['deadline'] ?? []) + count($congestion['not_accepted'] ?? [])) ?>
                </p>
            </div>
            <div class="reports-items">
                <?php if (!empty($congestion['deadline'])): ?>
                    <p class="report-title">Просрочен крайний срок</p>
                    <?php foreach ($congestion['deadline'] as $task): ?>
                        <div class="report-item">
                            <h3>
                                <?= $this->url->link($this->text->e($task['title']), 'VectorTaskViewController', 'show', array('plugin' => 'VectorTasks', 'task_id' => $task['id'])); ?>
                            </h3>
                            <p class="report-status">Дата завершения задачи: <?= date('d.m.Y', $task['date_due']); ?></p>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>

                <?php if (!empty($congestion['not_accepted'])): ?>
                    <div class="report-title">Не приняты</div>
                    <?php foreach ($congestion['not_accepted'] as $task): ?>
                        <div class="report-item">
                            <h3>
                                <?= $this->url->link($this->text->e($task['title']), 'VectorTaskViewController', 'show', array('plugin' => 'VectorTasks', 'task_id' => $task['id'])); ?>
                            </h3>
                            <p class="report-status">Не принята после: <?= date('Y-m-d', $this->text->e($task['date_creation']) + 86400); ?></p>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    <?php endforeach; ?>
</section>