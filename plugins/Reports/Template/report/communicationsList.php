<section class="reports">
    <ul class="communications">
        <?php foreach ($communications as $task): ?>
            <li class="table-list-row communication">
                <div class="table-list-details">
                    <h3 class="table-list-title">
                        <?= $this->url->link($this->text->e($task['title']), 'VectorTaskViewController', 'show', array('plugin' => 'VectorTasks', 'task_id' => $task['id'])); ?>
                    </h3>
                    <label class="communication-status">
                        <?= $this->text->e($task['column_name']) ?>
                    </label>
                    <p>
                        Создана: <?= $this->dt->date($task['date_creation']) ?>
                    </p>
                    <p>Дата завершения:
                        <?= date('d.m.Y', $task['date_due']) ?>
                    </p>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</section>