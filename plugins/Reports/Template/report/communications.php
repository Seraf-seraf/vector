<section class="onCommunications">
    <table class="iksweb">
        <tbody>
            <tr>
                <th>Повторяющаяся задача</th>
                <th>Всего коммуникаций</th>
                <th>Не выполнено</th>
                <th>Ожидающие</th>
                <th>Согласованные</th>
                <th>В процессе</th>
                <th>Выполнено</th>
                <th>Процент выполненных задач</th>
                <th>Закрыто без доказательств</th>
            </tr>
            <?php foreach ($repeatTasks as $recurrenceId => $cell): ?>
                <tr>
                    <td><?= $this->text->e($cell['title']); ?></td>
                    <td><?= $this->text->e($cell['count']); ?></td>
                    <td><?= $this->text->e($cell['notCompleted']); ?></td>
                    <td><?= $this->text->e($cell['waitingTasks']); ?></td>
                    <td><?= $this->text->e($cell['agreedTasks']); ?></td>
                    <td><?= $this->text->e($cell['inProgressTasks']); ?></td>
                    <td><?= $this->text->e($cell['completedTasks']); ?></td>
                    <td><?= $this->text->e($cell['percentCompletedTasks']); ?></td>
                    <td><?= $this->text->e($cell['withoutProofs']); ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</section>