const topbarContent = document.querySelector('.topbar-content');

if (topbarContent) {
    topbarContent.addEventListener('click', (event) => {
        let target = event.target;
    
        if (target.matches('.user')) {
            target.classList.toggle('user--close');
        }
    })
}
