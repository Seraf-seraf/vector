<?php

namespace Kanboard\Plugin\Employee;

use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Translator;
use Kanboard\Plugin\Employee\ServiceProvider\ExtendedRouteProvider;

class Plugin extends Base
{
    public function initialize()
    {
        $this->container->register(new ExtendedRouteProvider());
        $this->template->setTemplateOverride('user_list/dropdown', 'Employee:user_list/dropdown');
        $this->template->setTemplateOverride('header/title', 'Employee:header/title');
        $this->template->setTemplateOverride('dashboard/sidebar', 'Employee:dashboard/sidebar');
        $this->template->setTemplateOverride('dashboard/overview', 'Employee:dashboard/overview');
        $this->template->setTemplateOverride('user_creation/show', 'Employee:user_creation/show');
        $this->hook->on('template:layout:css', array('template' => 'plugins/Employee/Assets/css/plug.css'));
    }

    public function onStartup()
    {
        Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getClasses()
    {
        return [
            'Plugin\Employee\Model' => [
                'UserDataModel',
                'CreateUserModel',
            ],
        ];
    }

    public function getPluginAuthor()
    {
        return "Rustam Urazov";
    }

    public function getPluginName()
    {
        return "Employee";
    }

    public function getPluginDescription()
    {
        return "Count of active, came in today and created today employee";
    }

    public function getPluginVersion()
    {
        return "1.0.0";
    }
}

