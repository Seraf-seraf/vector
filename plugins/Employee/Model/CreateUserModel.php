<?php

namespace Kanboard\Plugin\Employee\Model;

use DateTime;
use Kanboard\Core\Base;
use Kanboard\Model\UserModel;

class CreateUserModel extends Base {
    public function prepare(array &$values)
    {
        if (isset($values['password'])) {
            if (! empty($values['password'])) {
                $values['password'] = \password_hash($values['password'], PASSWORD_BCRYPT);
            } else {
                unset($values['password']);
            }
        }

        if (isset($values['username'])) {
            $values['username'] = trim($values['username']);
        }

        $date = new DateTime();
        $now = $date->format('Y-m-d H:i:s');
        $values['created_at'] = strtotime($now);

        $this->helper->model->removeFields($values, array('confirmation', 'current_password'));
        $this->helper->model->resetFields($values, array('is_ldap_user', 'disable_login_form'));
        $this->helper->model->convertNullFields($values, array('gitlab_id'));
        $this->helper->model->convertIntegerFields($values, array('gitlab_id'));
    }
    
    public function create(array $values)
    {
        $this->prepare($values);
        return $this->db->table(UserModel::TABLE)->persist($values);
    }
}