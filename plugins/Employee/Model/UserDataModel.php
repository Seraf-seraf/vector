<?php

namespace Kanboard\Plugin\Employee\Model;

use Kanboard\Core\Base;
use Kanboard\Model\LastLoginModel;
use Kanboard\Model\UserModel;
use Kanboard\Plugin\RelatedGroups\Model\GroupRelatedModel;

/**
 * Employee UserData Model
 *
 * @package  Kanboard\Plugin\Employee
 * @author   Rustam Urazov
 */
class UserDataModel extends Base
{

    public function getCameInToday()
    {

        $users = $this->db->table(LastLoginModel::TABLE)->gte('date_creation', $this->getTodayAsInt())->columns('user_id')->groupBy('user_id')->findAll();

        return $users;
    }

    public function getCreatedToday()
    {
        $users = $this->db->table(UserModel::TABLE)->gte('created_at', $this->getTodayAsInt())->columns('id')->groupBy('id')->findAll();
        return $users;
    }

    public function getAdministrators()
    {
        $users = $this->db->table(GroupRelatedModel::TABLE)->columns('user_id')->groupBy('user_id')->findAll();

        return $users;
    }

    private function getTodayAsInt()
    {
        $now = date("Y-m-d");
        return strtotime($now);
    }
}
