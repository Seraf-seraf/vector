employee
==============================

Count of active, came in today and created today employee

Author
------

- Rustam Urazov
- License MIT

Requirements
------------

- Kanboard >= 1.0.35

Installation
------------

You have the choice between 3 methods:

1. Install the plugin from the Kanboard plugin manager in one click
2. Download the zip file and decompress everything under the directory `plugins/Employee`
3. Clone this repository into the folder `plugins/Employee`

Note: Plugin folder is case-sensitive.

Documentation
-------------

TODO.
