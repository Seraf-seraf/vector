<?php

namespace Kanboard\Plugin\Employee\Controller;

use Kanboard\Core\Security\Role;
use Kanboard\Controller\UserCreationController;
use Kanboard\Notification\MailNotification;

class CreateUserController extends UserCreationController {
    /**
     * Validate and save a new user
     *
     * @access public
     */
    public function save()
    {
        $values = $this->request->getValues();
        $values['username'] = $values['email'];
        list($valid, $errors) = $this->userValidator->validateCreation($values);

        if ($valid) {
            $this->createUser($values);
        } else {
            $this->show($values, $errors);
        }
    }

    protected function createUser(array $values)
    {
        $project_id = empty($values['project_id']) ? 1 : $values['project_id'];
        unset($values['project_id']);

        $user_id = $this->createUserModel->create($values);

        if ($user_id !== false) {
            if ($project_id !== 0) {
                $this->projectUserRoleModel->addUser($project_id, $user_id, Role::PROJECT_MEMBER);
            }

            if (! empty($values['notifications_enabled'])) {
                $this->userNotificationTypeModel->saveSelectedTypes($user_id, array(MailNotification::TYPE));
            }

            $this->flash->success(t('User created successfully.'));
            $this->response->redirect($this->helper->url->to('UserViewingController', 'show', array('user_id' => $user_id, 'plugin' => 'UserProfileEdit')));
        } else {
            $this->flash->failure(t('Unable to create your user.'));
            $this->response->redirect($this->helper->url->to('UserListController', 'show'));
        }
    }
}