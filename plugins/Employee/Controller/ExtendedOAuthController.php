<?php

namespace Kanboard\Plugin\Employee\Controller;

use Kanboard\Controller\OAuthController;

class ExtendedOAuthController extends OAuthController {
    protected function redirectAfterLogin()
    {
        if (session_exists('redirectAfterLogin') && ! filter_var(session_get('redirectAfterLogin'), FILTER_VALIDATE_URL)) {
            $redirect = session_get('redirectAfterLogin');
            session_remove('redirectAfterLogin');
            $this->response->redirect($redirect);
        } else {
            $this->response->redirect($this->helper->url->to('EmployerController', 'show', array('plugin' => 'Employee')));
        }
    }
}