<?php

namespace Kanboard\Plugin\Employee\Controller;

use Kanboard\Controller\DashboardController;

class EmployerController extends DashboardController {
    public function show()
    {
        $user = $this->getUser();
        $activeUsers = count($this->userModel->getActiveUsersList());
        $cameInTodayUsers = count($this->userDataModel->getCameInToday());
        $createdTodayUsers = count($this->userDataModel->getCreatedToday());
        $administrators = count($this->userDataModel->getAdministrators());

        $this->response->html($this->helper->layout->dashboard('dashboard/overview', array(
            'title'              => t('Dashboard for %s', $this->helper->user->getFullname($user)),
            'user'               => $user,
            'active_users'       => $activeUsers,
            'came_in_today_users'=> $cameInTodayUsers,
            'created_today_users'=> $createdTodayUsers,
            'administrators'     => $administrators,
            'overview_paginator' => $this->dashboardPagination->getOverview($user['id']),
            'project_paginator'  => $this->projectPagination->getDashboardPaginator($user['id'], 'show', 10),
        )));
    }

}