<?php

namespace Kanboard\Plugin\Employee\Controller;

use Kanboard\Controller\AuthController;

class ExtendedAuthController extends AuthController {
    public function login(array $values = array(), array $errors = array())
    {
        if ($this->userSession->isLogged()) {
            $this->response->redirect($this->helper->url->to('EmployerController', 'show', array('plugin' => 'Employee')));
        } else {
            $this->response->html($this->helper->layout->app('auth/index', array(
                'captcha' => ! empty($values['username']) && $this->userLockingModel->hasCaptcha($values['username']),
                'errors' => $errors,
                'values' => $values,
                'no_layout' => true,
                'title' => t('Login')
            )));
        }
    }

    public function logout()
    {
        if (! DISABLE_LOGOUT) {
            $this->sessionManager->close();
            $this->response->redirect($this->helper->url->to('ExtendedAuthController', 'login', array('plugin' => 'Employee')));
        } else {
            $this->response->redirect($this->helper->url->to('EmployerController', 'show', array('plugin' => 'Employee')));
        }
    }

    protected function redirectAfterLogin()
    {
        if (session_exists('redirectAfterLogin') && ! filter_var(session_get('redirectAfterLogin'), FILTER_VALIDATE_URL)) {
            $redirect = session_get('redirectAfterLogin');
            session_remove('redirectAfterLogin');
            $this->response->redirect($redirect);
        } else {
            $this->response->redirect($this->helper->url->to('EmployerController', 'show', array('plugin' => 'Employee')));
        }
    }
}