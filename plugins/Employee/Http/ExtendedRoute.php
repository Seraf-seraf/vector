<?php

namespace Kanboard\Plugin\Employee\Http;

use Kanboard\Core\Http\Route;

class ExtendedRoute extends Route {
    private $activated = false;

    /**
     * Store routes for path lookup
     *
     * @access private
     * @var array
     */
    private $paths = array();

    /**
     * Store routes for url lookup
     *
     * @access private
     * @var array
     */
    private $urls = array();
    
    public function findRoute($path)
    {
        $items = explode('/', ltrim($path, '/'));
        $count = count($items);

        foreach ($this->paths as $route) {
            if ($count === $route['count']) {
                $params = array();

                for ($i = 0; $i < $count; $i++) {
                    if ($route['items'][$i][0] === ':') {
                        $params[substr($route['items'][$i], 1)] = urldecode($items[$i]);
                    } elseif ($route['items'][$i] !== $items[$i]) {
                        break;
                    }
                }

                if ($i === $count) {
                    $this->request->setParams($params);
                    return array(
                        'controller' => $route['controller'],
                        'action' => $route['action'],
                        'plugin' => $route['plugin'],
                    );
                }
            }
        }

        return array(
            'controller' => 'EmployerController',
            'action' => 'show',
            'plugin' => 'Employee',
        );
    }
}