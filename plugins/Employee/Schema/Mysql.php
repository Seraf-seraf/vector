<?php

namespace Kanboard\Plugin\Employee\Schema;

use PDO;

const VERSION = 2;

function version_2(PDO $pdo) {
    $pdo->exec("ALTER TABLE `users` ADD COLUMN `position` VARCHAR DEFAULT ' '");
}

function version_1(PDO $pdo)
{
    $pdo->exec("ALTER TABLE `users` ADD COLUMN `created_at` INT DEFAULT '0'");
}
