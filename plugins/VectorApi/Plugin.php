<?php

namespace Kanboard\Plugin\VectorApi;

use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Translator;
use Kanboard\Plugin\VectorApi\ServiceProvider\ExtendedApiProvider;

class Plugin extends Base
{
    public function initialize()
    {
        $this->container->register(new ExtendedApiProvider());
    }

    public function onStartup()
    {
        Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getClasses() {
        return [
            'Plugin\VectorApi\Formatter' => [
                'ProofApiFormatter',
            ],
            'Plugin\VectorApi\Model' => [
                'TaskProofModel',
                'TaskProofFileModel',
                'RecurrenceTaskFileModel',
            ],
        ];
    }

    public function getPluginName()
    {
        return 'VectorApi';
    }

    public function getPluginDescription()
    {
        return t('api procedures');
    }

    public function getPluginAuthor()
    {
        return 'Rustam Urazov';
    }

    public function getPluginVersion()
    {
        return '1.0.0';
    }

    public function getPluginHomepage()
    {
        return 'https://github.com/kanboard/plugin-myplugin';
    }
}

