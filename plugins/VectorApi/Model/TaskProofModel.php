<?php

namespace Kanboard\Plugin\VectorApi\Model;

use Kanboard\Core\Base;

class TaskProofModel extends Base {
    public function getProofByTaskId($taskId) {
        return $this->db->table('recurrence_task_has_proof')
            ->eq('task_id', $taskId)
            ->findOne();
    }

    public function getProofByRecurrenceId($recurrenceId) {
        return $this->db->table('recurrence_task_has_proof')
            ->eq('recurrence_id', $recurrenceId)
            ->findOne();
    }

    public function getTextProof($proofId) {
        return $this->db->table('task_has_proof')
            ->eq('proof_id', $proofId)
            ->findOne();
    }

    public function createTextProof($taskId, $proofId, $text) {
        return $this->db->table('task_has_proof')
            ->persist(array('proof_id' => $proofId, 'task_id' => $taskId, 'proof' => $text));
    }

    public function updateTextProof($id, $text) {
        return $this->db->table('task_has_proof')
            ->eq('id', $id)
            ->update(['proof' => $text]);
    }
}