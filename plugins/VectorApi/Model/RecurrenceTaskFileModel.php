<?php

namespace Kanboard\Plugin\VectorApi\Model;

use Kanboard\Model\FileModel;
use Kanboard\Model\TaskModel;
use Kanboard\Plugin\VectorTasks\Model\RecurrenceTaskModel;

class RecurrenceTaskFileModel extends FileModel {
    const TABLE = 'recurrence_task_has_files';

    protected function getTable() {
        return self::TABLE;
    }

    protected function getForeignKey() {
        return 'recurrence_id';
    }

    protected function getPathPrefix() {
        return 'recurrencetasks';
    }

    public function getProjectId($file_id) {
        return $this->db->table(self::TABLE)
            ->eq(self::TABLE.'.id', $file_id)
            ->join(RecurrenceTaskModel::TABLE, 'id', 'recurrence_id')
            ->join(TaskModel::TABLE, 'recurrence_id', 'id')
            ->findOneColumn(TaskModel::TABLE.'.project_id') ?: 0;
    }

    protected function fireCreationEvent($file_id) {
        $this->queueManager->push($this->taskFileEventJob->withParams($file_id, 'task.file.create'));
    }

    protected function fireDestructionEvent($file_id) {
        $this->queueManager->push($this->taskFileEventJob->withParams($file_id, 'task.file.destroy'));
    }
}