<?php

namespace Kanboard\Plugin\VectorApi\Model;

use Kanboard\Core\ObjectStorage\ObjectStorageException;
use Kanboard\Model\TaskFileModel;
use Kanboard\Model\UserModel;

class TaskProofFileModel extends TaskFileModel {
    public function create($foreign_key_id, $name, $path, $size) {
        $values = array(
            $this->getForeignKey() => $foreign_key_id,
            'name' => substr($name, 0, 255),
            'path' => $path,
            'is_image' => $this->isImage($name) ? 1 : 0,
            'size' => $size,
            'user_id' => $this->userSession->getId() ?: 0,
            'date' => time(),
            'proof' => true,
        );

        $task_has_file = $this->db->table($this->getTable())->persist($values);

        if ($task_has_file) {
            $file_id = (int) $this->db->getLastId();
            $this->fireCreationEvent($file_id);
            return $file_id;
        }

        return false;
    }

    public function getAllByRecurrenceId($recurrence_id) {
        return $this->db->table('recurrence_task_has_files')
            ->eq('recurrence_id', $recurrence_id)
            ->findAll();
    }

    public function uploadСontent($id, $originalFilename, $data, $isEncoded = true) {
        try {
            if ($isEncoded) {
                $data = base64_decode($data);
            }

            if (empty($data)) {
                $this->logger->error(__METHOD__.': Content upload with no data');
                return false;
            }

            $destinationFilename = $this->generatePath($id, $originalFilename);
            $this->objectStorage->put($destinationFilename, $data);

            if ($this->isImage($originalFilename)) {
                $this->generateThumbnailFromData($destinationFilename, $data);
            }

            return $this->create(
                $id,
                $originalFilename,
                $destinationFilename,
                strlen($data)
            );
        } catch (ObjectStorageException $e) {
            $this->logger->error($e->getMessage());
            return false;
        }
    }

    protected function getQuery()
    {
        return $this->db
            ->table($this->getTable())
            ->columns(
                $this->getTable().'.id',
                $this->getTable().'.name',
                $this->getTable().'.path',
                $this->getTable().'.is_image',
                $this->getTable().'.'.$this->getForeignKey(),
                $this->getTable().'.date',
                $this->getTable().'.user_id',
                $this->getTable().'.size',
                $this->getTable().'.proof',
                UserModel::TABLE.'.username',
                UserModel::TABLE.'.name as user_name'
            )
            ->join(UserModel::TABLE, 'id', 'user_id')
            ->asc($this->getTable().'.name');
    }
}