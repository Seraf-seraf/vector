<?php

namespace Kanboard\Plugin\VectorApi\Api\Procedure;

use Kanboard\Api\Procedure\TaskProcedure;
use Kanboard\Api\Authorization\ProjectAuthorization;

class ExtendedTaskProcedure extends TaskProcedure {
    public function addTask($title, $project_id, $owner_id, $controller_id, $color_id = '', $column_id = 1, $creator_id = 0,
                               $date_due = '', $description = '', $category_id = 0, $score = 0, $swimlane_id = null, $priority = 0,
                               $recurrence_status = 0, $recurrence_trigger = 0, $recurrence_factor = 0, $recurrence_timeframe = 0,
                               $recurrence_basedate = 0, $reference = '', array $tags = array(), $date_started = '',
                               $time_spent = null, $time_estimated = null, $proof_type = 'not', $proof_desc = '', $proof_count = 1)
    {
        ProjectAuthorization::getInstance($this->container)->check($this->getClassName(), 'createTask', $project_id);

        if ($owner_id !== 0 && ! $this->projectPermissionModel->isAssignable($project_id, $owner_id)) {
            return false;
        }

        if ($this->userSession->isLogged()) {
            $creator_id = $this->userSession->getId();
        }

        $values = array(
            'title' => $title,
            'project_id' => $project_id,
            'owner_id' => $owner_id,
            'controller_id' => $controller_id,
            'color_id' => $color_id,
            'column_id' => $column_id,
            'creator_id' => $creator_id,
            'date_due' => $date_due,
            'description' => $description,
            'category_id' => $category_id,
            'score' => $score,
            'swimlane_id' => $swimlane_id,
            'recurrence_status' => $recurrence_status,
            'recurrence_trigger' => $recurrence_trigger,
            'recurrence_factor' => $recurrence_factor,
            'recurrence_timeframe' => $recurrence_timeframe,
            'recurrence_basedate' => $recurrence_basedate,
            'reference' => $reference,
            'priority' => $priority,
            'tags' => $tags,
            'date_started' => $date_started,
            'time_spent' => $time_spent,
            'time_estimated' => $time_estimated,
        );

        $task_id = $this->taskCreationModel->create($values);
        if ($task_id > 0) {
            if ($proof_type !== 'not') {
                return $this->taskCreationModel->set_task_proof(array('task_id' => $task_id, 'proof_type' => $proof_type, 'proof_description' => $proof_desc, 'proof_count' => $proof_count));
            }
            return true;
        }

        return  false;
    }
}