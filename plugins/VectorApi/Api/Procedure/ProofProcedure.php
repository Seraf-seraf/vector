<?php

namespace Kanboard\Plugin\VectorApi\Api\Procedure;

use Kanboard\Api\Procedure\BaseProcedure;
use Kanboard\Core\ObjectStorage\ObjectStorageException;

class ProofProcedure extends BaseProcedure {
    public function getTaskProofByTaskId($task_id) {
        $task_proof = $this->taskProofModel->getProofByTaskId($task_id);
        return $this->proofApiFormatter->withProof($task_proof)->format();
    }

    public function getTaskProofByRecurrenceId($recurrence_id) {
        $task_proof = $this->taskProofModel->getProofByRecurrenceId($recurrence_id);
        return $this->proofApiFormatter->withProof($task_proof)->format();
    }

    public function getTextProof($proof_id) {
        $text_proof = $this->taskProofModel->getTextProof($proof_id);
        return $this->proofApiFormatter->withProof($text_proof)->format();
    }

    public function getProofFilesByTaskId($task_id) {
        return $this->taskProofFileModel->getAll($task_id);
    }

    public function createFileProofForTask($task_id, $filename, $blob) {
        try {
            return $this->taskProofFileModel->uploadContent($task_id, $filename, $blob);
        } catch (ObjectStorageException $e) {
            $this->logger->error(__METHOD__.': '.$e->getMessage());
            return false;
        }
    }

    public function removeFileProof($file_id) {
        return $this->taskFileModel->remove($file_id);
    }

    public function createTextProof($task_id, $proof_id, $text) {
        return $this->taskProofModel->createTextProof($task_id, $proof_id, $text);
    }

    public function updateTextProof($id, $text) {
        return $this->taskProofModel->updateTextProof($id, $text);
    }
}