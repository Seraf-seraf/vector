<?php

namespace Kanboard\Plugin\VectorApi\Api\Procedure;

use Kanboard\Api\Procedure\BaseProcedure;
use DateTime;

class RecurrenceTaskProcedure extends BaseProcedure {
    public function getAdminLevels() {
        $admin_levels = $this->groupRelatedModel->getAdminLevels();
        return $this->recurrenceApiFormatter->withRecurrence($admin_levels)->format();
    }

    public function getExecTypes() {
        $exec_types = $this->groupRelatedModel->getExecTypes();
        return $this->recurrenceApiFormatter->withRecurrence($exec_types)->format();
    }

    public function getGroups() {
        $groups = $this->groupRelatedModel->getAll();
        return $this->recurrenceApiFormatter->withRecurrence($groups)->format();
    }

    public function addRecurrenceTask($title, $select, $ignore, $controller, $description = '', $result = '', $recurrence_trigger = 4, $recurence_factor = 1,
                                      $recurrence_timeframe = 0, $recurrence_basedate = 0, $recurrence_date = '00:00', $time_due = '00:00', $proof_select = 'not',
                                      $proof_count = 1, $proof_desc = ''
    ) {
        $columns = array('all', 'admins', 'admin_level', 'admin_function', 'exec_type', 'exec_function');
        $filterData = $this->filterGroupsAndUsersByRecurrenceTask($columns, $select, $ignore);
        $ignore_list = $filterData['ignore_list'];
        $user_list = $filterData['user_list'];
        $add_list = $filterData['add_list'];
        $group_list = $filterData['group_list'];
        $add_list = array_unique($add_list);
        $ignore_list = array_unique($ignore_list);
        sort($add_list);
        sort($ignore_list);
        $result_list = array_diff($add_list, $ignore_list);
        sort($result_list);
        $task_data = array();
        $task_data['title'] = $title;
        $task_data['description'] = $description;
        $task_data['result'] = $result;
        $task_data['recurrence_trigger'] = $recurrence_trigger;
        $task_data['recurrence_factor'] = $recurence_factor;
        $task_data['recurrence_timeframe'] = $recurrence_timeframe;
        $task_data['recurrence_basedate'] = $recurrence_basedate;
        $recurrence_date = new DateTime($recurrence_date);
        $time_due = new DateTime($time_due);
        $time_now = new DateTime();
        $recurring_time = $time_now->modify('today ' . $recurrence_date->format('H:i'));
        $time_now_2 = clone $time_now;
        $isToday = $recurring_time->getTimestamp() > time();
        $time_due = $time_now_2->modify('+' . $time_due->format('H') . ' hours +' . $time_due->format('i') . ' minutes');
        if ($isToday) {
            $task_data['date_due'] = $time_due->getTimestamp();
            $task_data['recurring_time'] = $recurring_time->getTimestamp();
        } else {
            $time_due->modify('+1 day');
            $recurring_time->modify('+1 day');
            $task_data['date_due'] = $time_due->getTimestamp();
            $task_data['recurring_time'] = $recurring_time->getTimestamp();
        }
        $task_data['color_id'] = 'red';
        $task_data['project_id'] = 1;
        $task_data['column_id'] = 1;
        $task_data['recurrence_status'] = 1;

        $groups = $this->group_select($group_list);
        $r_task_id = $this->recurrenceTaskModel->create(array('recurrence_factor' => $recurence_factor, 'recurrence_timeframe' => $recurrence_timeframe, 'time_due' => $time_due->getTimestamp(), 'recurring_time' => $recurring_time->getTimestamp()));

        if ($proof_select !== 'not') {
            $this->recurrenceTaskModel->set_recurrence_task_proof(array('recurrence_id' => $r_task_id, 'proof_type' => $proof_select, 'proof_count' => $proof_count, 'proof_description' => $proof_desc));
        }

        if (isset($groups['ids'])) {
            foreach ($groups['ids'] as $group) {
                $this->recurrenceTaskModel->set_group_recurrence_task(array('group_id' => $group, 'recurrence_id' => $r_task_id, 'ignore_admin' => $groups[$group]['ignore_admin'], 'ignore_exec' => $groups[$group]['ignore_exec']));
            }
        }

        $task_data['recurrence_id'] = $r_meta['recurrence_id'] = $recurrence_id = $r_task_id;
        $r_meta['title'] = $title;
        $r_meta['description'] = $description;
        $r_meta['result'] = $result;

        $this->recurrenceTaskModel->add_meta_info($r_meta);
        $this->recurrenceTaskModel->add_select_options_api($select, $ignore, $recurrence_id);
        foreach ($result_list as $user_id) {
            $group_id = $this->groupRelatedModel->groupContainUser($groups['ids'], $user_id)['group_id'];
            if ($controller == 'group_controller') {
                $controller_id = $this->recurrenceTaskModel->getRecurrenceController($group_id);
            }

            if (isset($controller_id[0]['id'])) {
                $task_data['controller_id'] = $controller_id[0]['id'];
                $this->recurrenceTaskModel->set_recurrence_user_task($user_id, $r_task_id, $controller_id[0]['id']);
            } else {
                $task_data['controller_id'] = $this->userSession->getId();
                $this->recurrenceTaskModel->set_recurrence_user_task($user_id, $r_task_id, $this->userSession->getId());
            }
        }
    }

    private function filterGroupsAndUsersByRecurrenceTask(array $data, $select, $ignore)
    {
        $user_list = $group_list = $add_list = $ignore_list = array();
        foreach ($data as $key) {
            switch ($key) {
                case "all":
                    if ($select[$key]) {
                        $user_list = $this->userModel->getActiveUsersList();
                        $group_list['add']['all'] = array_column($this->groupRelatedModel->getAll(), 'id');
                        foreach ($user_list as $user => $name) {
                            $add_list[] = $user;
                        }
                    }
                    break;
                case "admins":
                    if ($ignore[$key] || $select[$key]) {
                        if ($select[$key]) {
                            $group_list['add']['admins'] = array_column($this->groupRelatedModel->getAll(), 'id');
                        } else {
                            $group_list['ignore']['admins'] = array_column($this->groupRelatedModel->getAll(), 'id');
                        }

                        $unique_admins = array_column($this->groupRelatedModel->getAdminsUnique(), 'id');
                        foreach ($unique_admins as $admin) {
                            if ($ignore[$key]) {
                                $ignore_list[] = $admin;
                            } else {
                                $add_list[] = $admin;
                            }

                        }
                    }
                    break;
                case "admin_level":
                    if (count($select[$key]) > 0) {
                        $group_list['ignore']['admin_level'] = array_column($this->groupRelatedModel->getGroupsByAdminLevel($select[$key]), 'id');
                        foreach ($select[$key] as $val) {
                            $arr = array_column($this->groupRelatedModel->getAdminsByLevel($val), 'id');
                            foreach ($arr as $col) {
                                $add_list[] = $col;
                            }
                        }
                    }
                    if (count($ignore[$key]) > 0) {
                        $group_list['ignore']['admin_level'] = array_column($this->groupRelatedModel->getGroupsByAdminLevel($ignore[$key]), 'id');
                        foreach ($ignore[$key] as $val) {
                            $arr = array_column($this->groupRelatedModel->getAdminsByLevel($val), 'id');
                            foreach ($arr as $col) {
                                $ignore_list[] = $col;
                            }
                        }
                    }
                    // $ignore_list=array_merge($ignore_list,$admin_level_list);
                    break;
                case "exec_type":
                    if (count($select[$key]) > 0) {
                        $group_list['add']['exec_type'] = array_column($this->groupRelatedModel->getGroupsByExecType($select[$key]), 'group_id');
                        foreach ($select[$key] as $val) {
                            $executors_by_exec_type = array_column($this->groupRelatedModel->getExecutorsByType($val), 'id');
                            foreach ($executors_by_exec_type as $executor) {
                                $add_list[] = $executor;
                            }
                        }
                    }
                    if (count($ignore[$key]) > 0) {
                        $group_list['ignore']['exec_type'] = array_column($this->groupRelatedModel->getGroupsByExecType($ignore[$key]), 'group_id');
                        foreach ($ignore[$key] as $val) {
                            $executors_by_exec_type = array_column($this->groupRelatedModel->getExecutorsByType($val), 'id');
                            foreach ($executors_by_exec_type as $executor) {
                                $ignore_list[] = $executor;
                            }
                        }
                    }
                    break;
                case "admin_function":
                    //     var_dump($allowTasks['ignore']);
                    // return;
                    if (count($select[$key]) > 0) {
                        $group_list['add']['admin_function'] = array_column($this->groupRelatedModel->getGroupsById($select[$key]), 'id');
                        foreach ($select[$key] as $val) {
                            $admins_by_group = array_column($this->groupRelatedModel->getAdminsByGroup($val), 'id');
                            foreach ($admins_by_group as $admin) {
                                $add_list[] = $admin;
                            }
                        }
                    }
                    if (count($ignore[$key]) > 0) {
                        $group_list['ignore']['admin_function'] = array_column($this->groupRelatedModel->getGroupsById($ignore[$key]), 'id');
                        foreach ($ignore[$key] as $val) {
                            $admins_by_group = array_column($this->groupRelatedModel->getAdminsByGroup($val), 'id');
                            foreach ($admins_by_group as $admin) {
                                $ignore_list[] = $admin;
                            }
                        }
                    }
                    // var_dump($add_list);
                    // return;
                    break;
                case "exec_function":
                    if (count($select[$key]) > 0) {
                        $group_list['add']['exec_function'] = array_column($this->groupRelatedModel->getGroupsById($select[$key]), 'id');
                        $executors_by_group = array_column($this->groupRelatedModel->getExecutorsByGroup($select[$key]), 'id');
                        foreach ($executors_by_group as $executor) {
                            $add_list[] = $executor;
                        }
                    }
                    if (count($ignore[$key]) > 0) {
                        $group_list['ignore']['exec_function'] = array_column($this->groupRelatedModel->getGroupsById($ignore[$key]), 'id');
                        $executors_by_group = array_column($this->groupRelatedModel->getExecutorsByGroup($ignore[$key]), 'id');
                        foreach ($executors_by_group as $executor) {
                            $ignore_list[] = $executor;
                        }
                    }
            }
        }
        return array('add_list' => $add_list, 'ignore_list' => $ignore_list, 'user_list' => $user_list, 'group_list' => $group_list);
    }

    protected function group_select($group_list)
    {
        $merge_arr = array();
        $merge_exec_arr = array();
        $merge_admin_arr = array();
        if (!isset($group_list['ignore'])) {
            $group_list['ignore'] = array();
        }

        $keys_ignore = array_keys($group_list['ignore']);
        if (!isset($group_list['add'])) {
            $group_list['add'] = array();
        }

        $all = array_key_exists('all', $group_list['add']);
        foreach ($group_list['add'] as $group => $arr) {
            if (!$all) {
                if ($group == 'exec_type' || $group == 'exec_function') {
                    $merge_exec_arr = array_merge($merge_exec_arr, $arr);
                }
                if ($group == 'all_admins' || $group == 'admin_level' || $group == 'admin_function') {
                    $merge_admin_arr = array_merge($merge_admin_arr, $arr);
                }
            }

            $merge_arr = array_merge($merge_arr, $arr);
        }
        $merge_exec_arr = array_unique($merge_exec_arr);
        $merge_admin_arr = array_unique($merge_admin_arr);
        $merge_arr = array_unique($merge_arr);
        $result_arr = array();
        foreach ($merge_arr as $el) {
            if (!$all) {
                if (in_array($el, $merge_admin_arr)) {
                    $ignore_admin = false;
                } else {
                    $ignore_admin = true;
                }

                if (in_array($el, $merge_exec_arr)) {
                    $ignore_exec = false;
                } else {
                    $ignore_exec = true;
                }
            } else {
                $ignore_exec = false;
            }

            $ignore_admin = false;

            foreach ($keys_ignore as $key) {
                foreach ($group_list['ignore'][$key] as $group) {
                    if (($el == $group) && ($key == 'all_admins' || $key == 'admin_level' || $key == 'admin_function') && ($ignore_admin == false)) {
                        $ignore_admin = true;
                        continue;
                    }
                    if (($el == $group) && ($key == 'exec_type' || $key == 'exec_function') && ($ignore_exec == false)) {
                        $ignore_exec = true;
                        continue;
                    }
                }
            }
            if ($ignore_admin && $ignore_exec) {
                continue;
            } else {
                $result_arr['ids'][] = $el;
                $result_arr[$el]['ignore_admin'] = $ignore_admin;
                $result_arr[$el]['ignore_exec'] = $ignore_exec;
            }

        }
        return $result_arr;
    }
}