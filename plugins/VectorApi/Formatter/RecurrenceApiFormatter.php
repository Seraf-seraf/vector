<?php

namespace Kanboard\Plugin\VectorApi\Formatter;

use Kanboard\Core\Filter\FormatterInterface;
use Kanboard\Formatter\BaseFormatter;

class RecurrenceApiFormatter extends BaseFormatter implements FormatterInterface {
    protected $recurrence = null;

    public function withRecurrence($recurrence) {
        $this->recurrence = $recurrence;
        return $this;
    }

    public function format() {
        return $this->recurrence;
    }
}