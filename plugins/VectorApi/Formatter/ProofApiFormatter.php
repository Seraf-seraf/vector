<?php

namespace Kanboard\Plugin\VectorApi\Formatter;

use Kanboard\Core\Filter\FormatterInterface;
use Kanboard\Formatter\BaseFormatter;

class ProofApiFormatter extends BaseFormatter implements FormatterInterface {
    protected $proof = null;

    public function withProof($proof) {
        $this->proof = $proof;
        return $this;
    }

    public function format() {
        return $this->proof;
    }
}