<?php

namespace Kanboard\Plugin\UIEdit;

use Kanboard\Core\Plugin\Base;

class Plugin extends Base
{
    public function initialize()
    {
        $this->hook->on('template:layout:css', array('template' => 'plugins/UIEdit/Assets/style.css'));
    }

    public function getPluginName()
    {
        return 'UIEdit';
    }
    public function getPluginDescription()
    {
        return t('Change UI');
    }
    public function getPluginAuthor()
    {
        return 'Seraf-seraf';
    }
    public function getPluginVersion()
    {
        return '0.0.1';
    }
}