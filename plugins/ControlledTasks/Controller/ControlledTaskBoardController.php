<?php

namespace Kanboard\Plugin\ControlledTasks\Controller;

use Kanboard\Controller\BaseController;

class ControlledTaskBoardController extends BaseController {
    public function show() {
        $project = $this->getProject();

        $this->response->html($this->helper->layout->app('board/view_private', array(
            'project' => $project,
            'title' => $project['name'],
            'my_tasks' => $this->request->getStringParam('task'),
            'description' => $this->helper->projectHeader->getDescription($project),
            'board_private_refresh_interval' => $this->configModel->get('board_private_refresh_interval'),
            'board_highlight_period' => $this->configModel->get('board_highlight_period'),
            'swimlanes' => $this->taskLexer
                ->build('')
                ->format($this->controlledTaskBoardFormatter->withProjectId($project['id'])->withUserId($this->getUser()['id']))
        )));
    }
}