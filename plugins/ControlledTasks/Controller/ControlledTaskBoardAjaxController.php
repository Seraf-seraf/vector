<?php

namespace Kanboard\Plugin\ControlledTasks\Controller;

use Exception;
use Kanboard\Core\Controller\AccessForbiddenException;
use Kanboard\Plugin\VectorTasks\Controller\VectorBoardAjaxController;

class ControlledTaskBoardAjaxController extends VectorBoardAjaxController
{
    public function save()
    {
        $this->checkReusableGETCSRFParam();
        $project_id = $this->request->getIntegerParam('project_id');


        if (! $project_id || ! $this->request->isAjax()) {
            throw new AccessForbiddenException();
        }

        $values = $this->request->getJson();

        if (! $this->helper->projectRole->canMoveTask($project_id, $values['src_column_id'], $values['dst_column_id'])) {
            throw new AccessForbiddenException(e("You don't have the permission to move this task"));
        }

        try {
            $result =$this->taskPositionModel->movePosition(
                $project_id,
                $values['task_id'],
                $values['dst_column_id'],
                $values['position'],
                $values['swimlane_id']
            );

            if (! $result) {
                $this->response->status(400);
            } else {
                $user=$this->getUser()['id'];
                $this->response->html($this->renderBoard($project_id,$user, 201));
            }
        } catch (Exception $e) {
            $this->response->html('<div class="alert alert-error">'.$e->getMessage().'</div>');
        }
    }

    protected function renderBoard($project_id)
    {
        return $this->template->render('ControlledTasks:board/table_container', array(
            'project' => $this->projectModel->getById($project_id),
            'board_private_refresh_interval' => $this->configModel->get('board_private_refresh_interval'),
            'board_highlight_period' => $this->configModel->get('board_highlight_period'),
            'swimlanes' => $this->taskLexer
                ->build($this->userSession->getFilters($project_id))
                ->format($this->controlledTaskBoardFormatter->withProjectId($project_id)->withUserId($this->getUser()['id']))
        ));
    }
}
