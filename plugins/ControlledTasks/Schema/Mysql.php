<?php

namespace Kanboard\Plugin\ControlledTasks\Schema;

use PDO;

const VERSION = 1;

function version_1(PDO $pdo)
{
    $pdo->exec("ALTER TABLE `tasks` ADD COLUMN `controller_id` INT DEFAULT '0'");
}
