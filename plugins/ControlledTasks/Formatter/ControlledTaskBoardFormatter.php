<?php

namespace Kanboard\Plugin\ControlledTasks\Formatter;

use Kanboard\Model\SwimlaneModel;
use Kanboard\Model\TaskModel;
use Kanboard\Plugin\VectorTasks\Formatter\VectorBoardFormatter;

class ControlledTaskBoardFormatter extends VectorBoardFormatter {
    protected function getTasks() {
        return $this->query
            ->eq(TaskModel::TABLE.'.project_id', $this->projectId)
            ->eq(TaskModel::TABLE.'.controller_id', $this->userId)
            ->findAll();
    }
}