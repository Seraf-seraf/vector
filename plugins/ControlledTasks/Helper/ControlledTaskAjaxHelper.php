<?php

namespace Kanboard\Plugin\ControlledTasks\Helper;

use Kanboard\Plugin\VectorTasks\Helper\AjaxHelper;

class ControlledTaskAjaxHelper extends AjaxHelper
{
    public function isDraggable(array &$task)
    {
        if ($task['is_active'] == 1 && $this->helper->user->hasProjectAccess('ControlledTaskBoardAjaxController', 'save', $task['project_id'])) {
            return $this->isSortableColumn($task['project_id'], $task['column_id'], $task['owner_id']);
        }

        return false;
    }
}