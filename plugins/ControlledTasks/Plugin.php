<?php

namespace Kanboard\Plugin\ControlledTasks;

use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Translator;

class Plugin extends Base
{
    public function initialize()
    {
        $this->helper->register('participationTaskAjaxHelper', '\Kanboard\Plugin\ControlledTasks\Helper\ControlledTaskAjaxHelper');
        $this->template->setTemplateOverride('board/table_container', 'ControlledTasks:board/table_container');
    }

    public function onStartup()
    {
        Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function getClasses()
    {
        return [
            'Plugin\ControlledTasks\Formatter' => [
                'ControlledTaskBoardFormatter',
            ],
        ];
    }

    public function getPluginName()
    {
        return 'ControlledTasks';
    }

    public function getPluginDescription()
    {
        return t('Showing tasks which controlled by the user');
    }

    public function getPluginAuthor()
    {
        return 'Rustam Urazov';
    }

    public function getPluginVersion()
    {
        return '1.0.0';
    }

    public function getPluginHomepage()
    {
        return 'https://github.com/kanboard/plugin-myplugin';
    }
}

