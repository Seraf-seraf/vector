<?php

namespace Kanboard\Plugin\AccessPlugin;

use Kanboard\Core\Plugin\Base;

class Plugin extends Base
{

    public function initialize()
    {
        $this->applicationAccessMap->add('TaskListController', array('show'), \Kanboard\Core\Security\Role::APP_ADMIN);
    }

    public function getPluginName()
    {
        return 'AccessPlugin';
    }

    public function getPluginDescription()
    {
        return t('Setting up access to pages');
    }

    public function getPluginAuthor()
    {
        return 'Seraf-seraf';
    }

    public function getPluginVersion()
    {
        return '0.0.1';
    }
}